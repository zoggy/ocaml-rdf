(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Reading and writing Turtle. *)

type error = Parse_error of Loc.loc * string | Unknown_namespace of string
exception Error of error
val string_of_error : error -> string

(** Input graph from string. Default base is the graph name. *)
val from_string : Graph.graph -> ?base:Iri.t -> string -> unit

(** Same as {!from_string} but read from the given file. *)
val from_file : Graph.graph -> ?base:Iri.t -> string -> unit

val string_of_term : Term.term -> string

val string_of_triple :
  sub:Term.term -> pred:Iri.t -> obj:Term.term -> string

val to_ : ?compact: bool -> ?namespaces: (Iri.t * string) list ->
  (string -> unit) -> Graph.graph -> unit
val to_string : ?compact: bool -> ?namespaces: (Iri.t * string) list ->
  Graph.graph -> string
val to_file : ?compact: bool -> ?namespaces: (Iri.t * string) list ->
  Graph.graph -> string -> unit
