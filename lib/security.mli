(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Elements of [https://w3id.org/security#] *)

(** [https://w3id.org/security#] *)
val security : Iri.t
val security_ : string -> Iri.t

(** <div>A Linked Data signature is used for digital signatures on RDF Datasets. The default canonicalization mechanism is specified in the RDF Dataset Normalization specification, which deterministically names all unnamed nodes. Importantly, a `BbsBlsSignature` digests each of the statements produced by the normalization process individually to enable selective disclosure. The signature mechanism uses Blake2B as the digest for each statement and produces a single output digital signature.</div> *)
val c_BbsBlsSignature2020 : Iri.t

(** <div>A Linked Data signature is used for digital signatures on RDF Datasets. The default canonicalization mechanism is specified in the RDF Dataset Normalization specification, which deterministically names all unnamed nodes. Importantly, a `BbsBlsSignatureProof2020` is in fact a proof of knowledge of an unrevealed BbsBlsSignature2020 enabling the ability to selectively reveal information from the set that was originally signed. Each of the statements produced by the normalizing process for a JSON-LD document featuring a <a href="#BbsBlsSignatureProof2020">`BbsBlsSignatureProof2020`</a> represent statements that were originally signed in producing the `BbsBlsSignature2020` and represent the denomination under which information can be selectively disclosed. The signature mechanism uses Blake2B as the digest for each statement and produces a single output digital signature.</div> *)
val c_BbsBlsSignatureProof2020 : Iri.t

(** <div>This class represents a data integrity signature key.</div> *)
val c_Bls12381G1Key2020 : Iri.t

(** <div>This class represents a data integrity signature key.</div> *)
val c_Bls12381G2Key2020 : Iri.t

(** <div>This class represents a data integrity proof used to encode a variety of cryptographic suite proof encodings.</div> *)
val c_DataIntegrityProof : Iri.t

(** <div>This class represents a message digest that may be used for data integrity verification. The digest algorithm used will determine the cryptographic properties of the digest.</div> *)
val c_Digest : Iri.t

(** <div>This class represents a data integrity verification method.</div> *)
val c_EcdsaSecp256k1RecoveryMethod2020 : Iri.t

(** <div>This class represents a data integrity signature.</div> *)
val c_EcdsaSecp256k1RecoverySignature2020 : Iri.t

(** <div>This class represents a data integrity signature suite.</div> *)
val c_EcdsaSecp256k1Signature2019 : Iri.t

(** <div>This class represents a data integrity signature suite.</div> *)
val c_EcdsaSecp256k1Signature2020 : Iri.t

(** <div>This class represents a data integrity verification method.</div> *)
val c_EcdsaSecp256k1VerificationKey2019 : Iri.t

(** <div>T.B.D.</div> *)
val c_Ed25519Signature2020 : Iri.t

(** <div>This class represents a data integrity verification method.</div> *)
val c_Ed25519VerificationKey2018 : Iri.t

(** <div>A linked data proof suite verification method type used with <a href="#Ed25519Signature2020">`Ed25519Signature2020`</a>.</div> *)
val c_Ed25519VerificationKey2020 : Iri.t

(** <div>A class of messages that are obfuscated in some cryptographic manner. These messages are incredibly difficult to decrypt without the proper decryption key.</div> *)
val c_EncryptedMessage : Iri.t

(** <div>A graph signature is used for digital signatures on RDF graphs. The default canonicalization mechanism is specified in the RDF Graph normalization specification, which effectively deterministically names all unnamed nodes. The default signature mechanism uses a SHA-256 digest and RSA to perform the digital signature.</div> *)
val c_GraphSignature2012 : Iri.t

(** <div>A linked data proof suite verification method type used with <a href="#JsonWebSignature2020">`JsonWebSignature2020`</a></div> *)
val c_JsonWebKey2020 : Iri.t

(** <div>A Linked Data signature is used for digital signatures on RDF Datasets. The default canonicalization mechanism is specified in the RDF Dataset Normalization specification, which deterministically names all unnamed nodes. The default signature mechanism uses a SHA-256 digest and JWS to perform the digital signature.</div> *)
val c_JsonWebSignature2020 : Iri.t

(** <div>This class represents a cryptographic key that may be used for encryption, decryption, or digitally signing data.</div> *)
val c_Key : Iri.t

(** <div>A Linked Data signature is used for digital signatures on RDF Datasets. The default canonicalization mechanism is specified in the RDF Dataset Normalization specification, which effectively deterministically names all unnamed nodes. The default signature mechanism uses a SHA-256 digest and RSA to perform the digital signature.</div> *)
val c_LinkedDataSignature2015 : Iri.t

(** <div>A Linked Data signature is used for digital signatures on RDF Datasets. The default canonicalization mechanism is specified in the RDF Dataset Normalization specification, which effectively deterministically names all unnamed nodes. The default signature mechanism uses a SHA-256 digest and RSA to perform the digital signature.</div> *)
val c_LinkedDataSignature2016 : Iri.t

(** <div>Linked Data signature is used for digital signatures on RDF Datasets. The default canonicalization mechanism is specified in the RDF Dataset Normalization specification, which effectively deterministically names all unnamed nodes. The default signature mechanism uses a SHA-256 digest and ECDSA to perform the digital signature.</div> *)
val c_MerkleProof2019 : Iri.t

(** <div>Verification method to be used with, for example, data integrity proof cryptographic suites, such as the eddsa-2022 cryptographic suite. See the <a href="https://www.w3.org/TR/vc-di-eddsa/#multikey">EdDSA Cryptosuite v2022 specification</a> for further details.</div> *)
val c_Multikey : Iri.t

(** <div>This class represents a digital proof on serialized data.</div> *)
val c_Proof : Iri.t

(** <div>Instances of this class are RDF Graphs, where each of these graphs must include exactly one Proof.</div> *)
val c_ProofGraph : Iri.t

(** <div>This class represents a data integrity signature suite.</div> *)
val c_RsaSignature2018 : Iri.t

(** <div>This class represents a data integrity verification method.</div> *)
val c_RsaVerificationKey2018 : Iri.t

(** <div>This class represents a data integrity signature suite.</div> *)
val c_SchnorrSecp256k1Signature2019 : Iri.t

(** <div>This class represents a data integrity verification method.</div> *)
val c_SchnorrSecp256k1VerificationKey2019 : Iri.t

(** <div>T.B.D.</div> *)
val c_ServiceEndpointProxyService : Iri.t

(** <div>This class represents a digital signature on serialized data. It is an abstract class and should not be used other than for Semantic Web reasoning purposes, such as by a reasoning agent. This class MUST NOT be used directly, but only through its subclasses.</div> *)
val c_Signature : Iri.t

(** <div>Instances of this class are RDF Graphs, where each of these graphs must include exactly one Signature.</div> *)
val c_SignatureGraph : Iri.t

(** <div>A Verification Method class can express different verification methods, such as cryptographic public keys, which can be used to authenticate or authorize interaction with the `controller` or associated parties. Verification methods might take many parameters.</div> *)
val c_VerificationMethod : Iri.t

(** <div>This class represents a verification key.</div> *)
val c_X25519KeyAgreementKey2019 : Iri.t

(** <div>An action that the controller of a capability may take when invoking the capability.</div> *)
val allowedAction : Iri.t

(** <div>A `blockchainAccountId` property is used to specify a blockchain account identifier, as per the <a href="https://github.com/ChainAgnostic/CAIPs/blob/master/CAIPs/caip-10.md">CAIP-10Account ID Specification</a>.</div> *)
val blockchainAccountId : Iri.t

(** <div>The canonicalization algorithm is used to transform the input data into a form that can be passed to a cryptographic digest method. The digest is then digitally signed using a digital signature algorithm. Canonicalization ensures that a piece of software that is generating a digital signature is able to do so on the same set of information in a deterministic manner.</div> *)
val canonicalizationAlgorithm : Iri.t

(** <div>An action that can be taken given a capability.</div> *)
val capabilityAction : Iri.t

(** <div>An list of delegated capabilities from a delegator to a delegatee.</div> *)
val capabilityChain : Iri.t

(** <div>A restriction on the way the capability may be used.</div> *)
val caveat : Iri.t

(** <div>The challenge property is used to associate a challenge with a proof, for use with a `proofPurpose` such as `authentication`. This string value SHOULD be included in a proof if a `domain` is specified.</div> *)
val challenge : Iri.t

(** <div>The cipher algorithm describes the mechanism used to encrypt a message. It is typically a string expressing the cipher suite, the strength of the cipher, and a block cipher mode.</div> *)
val cipherAlgorithm : Iri.t

(** <div>Cipher data is an opaque blob of information that is used to specify an encrypted message.</div> *)
val cipherData : Iri.t

(** <div>A cipher key is a symmetric key that is used to encrypt or decrypt a piece of information. The key itself may be expressed in clear text or encrypted.</div> *)
val cipherKey : Iri.t

(** <div>A controller is an entity that claims control over a particular resource. Note that control is best validated as a two-way relationship, where the controller claims control over a particular resource, and the resource clearly identifies its controller.</div> *)
val controller : Iri.t

(** <div>A text-based identifier for a specific cryptographic suite.</div> *)
val cryptosuite : Iri.t

(** <div>An entity that delegates a capability to a delegatee.</div> *)
val delegator : Iri.t

(** <div>The digest algorithm is used to specify the cryptographic function to use when generating the data to be digitally signed. Typically, data that is to be signed goes through three steps: 1) canonicalization, 2) digest, and 3) signature. This property is used to specify the algorithm that should be used for step 2. A signature class typically specifies a default digest method, so this property is typically used to specify information for a signature algorithm.</div> *)
val digestAlgorithm : Iri.t

(** <div>The digest value is used to express the output of the digest algorithm expressed in Base-16 (hexadecimal) format.</div> *)
val digestValue : Iri.t

(** <div>The `domain` property is used to associate a domain with a proof, for use with a `proofPurpose` such as `authentication` and indicating its intended usage.</div> *)
val domain : Iri.t

(** <div>An `ethereumAddress` property is used to specify the Ethereum address. As per the Ethereum Yellow Paper "Ethereum: a secure decentralised generalised transaction ledger" in consists of a prefix "0x", a common identifier for hexadecimal, concatenated with the rightmost 20 bytes of the Keccak-256 hash (big endian) of the ECDSA public key (the curve used is the so-called secp256k1). In hexadecimal, 2 digits represent a byte, meaning addresses contain 40 hexadecimal digits. The Ethereum address should also contain a checksum as per EIP-55.</div> *)
val ethereumAddress : Iri.t

(** <div>The `expirationDate` property is used to associate an expiration date with a proof.</div> *)
val expirationDate : Iri.t

(** <div>The expiration time is typically associated with a <a href="#Key">`Key`</a> and specifies when the validity of the key will expire.</div> *)
val expires : Iri.t

(** <div>The initialization vector (IV) is a byte stream that is typically used to initialize certain block cipher encryption schemes. For a receiving application to be able to decrypt a message, it must know the decryption key and the initialization vector. The value is typically base-64 encoded.</div> *)
val initializationVector : Iri.t

(** <div>An invocation target identifies where a capability may be invoked, and identifies the target object for which the root capability expresses authority.</div> *)
val invocationTarget : Iri.t

(** <div>An identifier to cryptographic material that can invoke a capability.</div> *)
val invoker : Iri.t

(** <div>The jws property is used to associate a detached Json Web Signature with a proof.</div> *)
val jws : Iri.t

(** <div>This property is used in conjunction with the input to the signature hashing function in order to protect against replay attacks. Typically, receivers need to track all nonce values used within a certain time period in order to ensure that an attacker cannot merely re-send a compromised packet in order to execute a privileged request.</div> *)
val nonce : Iri.t

(** <div>An owner is an entity that claims control over a particular resource. Note that ownership is best validated as a two-way relationship where the owner claims ownership over a particular resource, and the resource clearly identifies its owner.</div> *)
val owner : Iri.t

(** <div>A secret that is used to generate a key that can be used to encrypt or decrypt message. It is typically a string value.</div> *)
val password : Iri.t

(** <div>A private key PEM property is used to specify the PEM-encoded version of the private key. This encoding is compatible with almost every Secure Sockets Layer library implementation and typically plugs directly into functions intializing private keys.</div> *)
val privateKeyPem : Iri.t

(** <div>The` proofPurpose` property is used to associate a purpose, such as `assertionMethod` or `authentication` with a proof. The proof purpose acts as a safeguard to prevent the proof from being misused by being applied to a purpose other than the one that was intended.</div> *)
val proofPurpose : Iri.t

(** <div>A string value that contains the data necessary to verify the digital proof using the `verificationMethod` specified</div> *)
val proofValue : Iri.t

(** <div>A public key property is used to specify a URL that contains information about a public key.</div> *)
val publicKey : Iri.t

(** <div>A public key Base58 property is used to specify the base58-encoded version of the public key.</div> *)
val publicKeyBase58 : Iri.t

(** <div>A `publicKeyHex` property is used to specify the hex-encoded version of the public key, based on section 8 of rfc4648. Hex encoding is also known as Base16 encoding.</div> *)
val publicKeyHex : Iri.t

(** <div>See the JOSE suite.</div> *)
val publicKeyJwk : Iri.t

(** <div><p>The public key multibase property is used to specify the multibase-encoded version of a public key. The contents of the property are defined by specifications such as ED25519-2020 and listed in the Linked Data Cryptosuite Registry. Most public key type definitions are expected to:</p>
<ul>
<li>Specify only a single encoding base per public key type as it reduces implementation burden and increases the chances of reaching broad interoperability.
<li>Specify a multicodec header on the encoded public key to aid encoding and decoding applications in confirming that they are serializing and deserializing an expected public key type.
<li>Use compressed binary formats to ensure efficient key sizes.
</ul></div> *)
val publicKeyMultibase : Iri.t

(** <div>A public key PEM property is used to specify the PEM-encoded version of the public key. This encoding is compatible with almost every Secure Sockets Layer library implementation and typically plugs directly into functions initializing public keys.</div> *)
val publicKeyPem : Iri.t

(** <div>The publicKeyService property is used to express the REST URL that provides public key management services.</div> *)
val publicKeyService : Iri.t

(** <div>The revocation time is typically associated with a <a href="#Key">`Key`</a> that has been marked as invalid as of the date and time associated with the property. Key revocations are often used when a key is compromised, such as the theft of the private key, or during the course of best-practice key rotation schedules.</div> *)
val revoked : Iri.t

(** <div>Examples of specific services include discovery services, social networks, file storage services, and verifiable claim repository services.</div> *)
val service : Iri.t

(** <div>A network address at which a service operates on behalf of a controller. Examples of specific services include discovery services, social networks, file storage services, and verifiable claim repository services. Service endpoints might also be provided by a generalized data interchange protocol, such as extensible data interchange.</div> *)
val serviceEndpoint : Iri.t

(** <div>The property is used to associate a proof with a graph of information. The proof property is typically not included in the canonicalized graph that is then digested, and digitally signed.</div> *)
val signature : Iri.t

(** <div>The signature algorithm is used to specify the cryptographic signature function to use when digitally signing the digest data. Typically, text to be signed goes through three steps: 1) canonicalization, 2) digest, and 3) signature. This property is used to specify the algorithm that should be used for step #3. A signature class typically specifies a default signature algorithm, so this property rarely needs to be used in practice when specifying digital signatures.</div> *)
val signatureAlgorithm : Iri.t

(** <div>The signature value is used to express the output of the signature algorithm expressed in base-64 format.</div> *)
val signatureValue : Iri.t

(** <div>The x509CertificateChain property is used to associate a chain of X.509 Certificates with a proof. The value of this property is an ordered list where each value in the list is an X.509 Certificate expressed as a DER PKIX format, that is encoded with multibase using the base64pad variant. The certificate directly associated to the verification method used to verify the proof MUST be the first element in the list. Subsequent certificates in the list MAY be included where each one MUST certify the previous one.</div> *)
val x509CertificateChain : Iri.t

(** <div>The x509CertificateFingerprint property is used to associate an X.509 Certificate with a proof via its fingerprint. The value is a multihash encoded then multibase encoded value using the base64pad variant. It is RECOMMENDED that the fingerprint value be the SHA-256 hash of the X.509 Certificate.</div> *)
val x509CertificateFingerprint : Iri.t


module Open : sig
  (** <div>A Linked Data signature is used for digital signatures on RDF Datasets. The default canonicalization mechanism is specified in the RDF Dataset Normalization specification, which deterministically names all unnamed nodes. Importantly, a `BbsBlsSignature` digests each of the statements produced by the normalization process individually to enable selective disclosure. The signature mechanism uses Blake2B as the digest for each statement and produces a single output digital signature.</div> *)
  val security_c_BbsBlsSignature2020 : Iri.t

  (** <div>A Linked Data signature is used for digital signatures on RDF Datasets. The default canonicalization mechanism is specified in the RDF Dataset Normalization specification, which deterministically names all unnamed nodes. Importantly, a `BbsBlsSignatureProof2020` is in fact a proof of knowledge of an unrevealed BbsBlsSignature2020 enabling the ability to selectively reveal information from the set that was originally signed. Each of the statements produced by the normalizing process for a JSON-LD document featuring a <a href="#BbsBlsSignatureProof2020">`BbsBlsSignatureProof2020`</a> represent statements that were originally signed in producing the `BbsBlsSignature2020` and represent the denomination under which information can be selectively disclosed. The signature mechanism uses Blake2B as the digest for each statement and produces a single output digital signature.</div> *)
  val security_c_BbsBlsSignatureProof2020 : Iri.t

  (** <div>This class represents a data integrity signature key.</div> *)
  val security_c_Bls12381G1Key2020 : Iri.t

  (** <div>This class represents a data integrity signature key.</div> *)
  val security_c_Bls12381G2Key2020 : Iri.t

  (** <div>This class represents a data integrity proof used to encode a variety of cryptographic suite proof encodings.</div> *)
  val security_c_DataIntegrityProof : Iri.t

  (** <div>This class represents a message digest that may be used for data integrity verification. The digest algorithm used will determine the cryptographic properties of the digest.</div> *)
  val security_c_Digest : Iri.t

  (** <div>This class represents a data integrity verification method.</div> *)
  val security_c_EcdsaSecp256k1RecoveryMethod2020 : Iri.t

  (** <div>This class represents a data integrity signature.</div> *)
  val security_c_EcdsaSecp256k1RecoverySignature2020 : Iri.t

  (** <div>This class represents a data integrity signature suite.</div> *)
  val security_c_EcdsaSecp256k1Signature2019 : Iri.t

  (** <div>This class represents a data integrity signature suite.</div> *)
  val security_c_EcdsaSecp256k1Signature2020 : Iri.t

  (** <div>This class represents a data integrity verification method.</div> *)
  val security_c_EcdsaSecp256k1VerificationKey2019 : Iri.t

  (** <div>T.B.D.</div> *)
  val security_c_Ed25519Signature2020 : Iri.t

  (** <div>This class represents a data integrity verification method.</div> *)
  val security_c_Ed25519VerificationKey2018 : Iri.t

  (** <div>A linked data proof suite verification method type used with <a href="#Ed25519Signature2020">`Ed25519Signature2020`</a>.</div> *)
  val security_c_Ed25519VerificationKey2020 : Iri.t

  (** <div>A class of messages that are obfuscated in some cryptographic manner. These messages are incredibly difficult to decrypt without the proper decryption key.</div> *)
  val security_c_EncryptedMessage : Iri.t

  (** <div>A graph signature is used for digital signatures on RDF graphs. The default canonicalization mechanism is specified in the RDF Graph normalization specification, which effectively deterministically names all unnamed nodes. The default signature mechanism uses a SHA-256 digest and RSA to perform the digital signature.</div> *)
  val security_c_GraphSignature2012 : Iri.t

  (** <div>A linked data proof suite verification method type used with <a href="#JsonWebSignature2020">`JsonWebSignature2020`</a></div> *)
  val security_c_JsonWebKey2020 : Iri.t

  (** <div>A Linked Data signature is used for digital signatures on RDF Datasets. The default canonicalization mechanism is specified in the RDF Dataset Normalization specification, which deterministically names all unnamed nodes. The default signature mechanism uses a SHA-256 digest and JWS to perform the digital signature.</div> *)
  val security_c_JsonWebSignature2020 : Iri.t

  (** <div>This class represents a cryptographic key that may be used for encryption, decryption, or digitally signing data.</div> *)
  val security_c_Key : Iri.t

  (** <div>A Linked Data signature is used for digital signatures on RDF Datasets. The default canonicalization mechanism is specified in the RDF Dataset Normalization specification, which effectively deterministically names all unnamed nodes. The default signature mechanism uses a SHA-256 digest and RSA to perform the digital signature.</div> *)
  val security_c_LinkedDataSignature2015 : Iri.t

  (** <div>A Linked Data signature is used for digital signatures on RDF Datasets. The default canonicalization mechanism is specified in the RDF Dataset Normalization specification, which effectively deterministically names all unnamed nodes. The default signature mechanism uses a SHA-256 digest and RSA to perform the digital signature.</div> *)
  val security_c_LinkedDataSignature2016 : Iri.t

  (** <div>Linked Data signature is used for digital signatures on RDF Datasets. The default canonicalization mechanism is specified in the RDF Dataset Normalization specification, which effectively deterministically names all unnamed nodes. The default signature mechanism uses a SHA-256 digest and ECDSA to perform the digital signature.</div> *)
  val security_c_MerkleProof2019 : Iri.t

  (** <div>Verification method to be used with, for example, data integrity proof cryptographic suites, such as the eddsa-2022 cryptographic suite. See the <a href="https://www.w3.org/TR/vc-di-eddsa/#multikey">EdDSA Cryptosuite v2022 specification</a> for further details.</div> *)
  val security_c_Multikey : Iri.t

  (** <div>This class represents a digital proof on serialized data.</div> *)
  val security_c_Proof : Iri.t

  (** <div>Instances of this class are RDF Graphs, where each of these graphs must include exactly one Proof.</div> *)
  val security_c_ProofGraph : Iri.t

  (** <div>This class represents a data integrity signature suite.</div> *)
  val security_c_RsaSignature2018 : Iri.t

  (** <div>This class represents a data integrity verification method.</div> *)
  val security_c_RsaVerificationKey2018 : Iri.t

  (** <div>This class represents a data integrity signature suite.</div> *)
  val security_c_SchnorrSecp256k1Signature2019 : Iri.t

  (** <div>This class represents a data integrity verification method.</div> *)
  val security_c_SchnorrSecp256k1VerificationKey2019 : Iri.t

  (** <div>T.B.D.</div> *)
  val security_c_ServiceEndpointProxyService : Iri.t

  (** <div>This class represents a digital signature on serialized data. It is an abstract class and should not be used other than for Semantic Web reasoning purposes, such as by a reasoning agent. This class MUST NOT be used directly, but only through its subclasses.</div> *)
  val security_c_Signature : Iri.t

  (** <div>Instances of this class are RDF Graphs, where each of these graphs must include exactly one Signature.</div> *)
  val security_c_SignatureGraph : Iri.t

  (** <div>A Verification Method class can express different verification methods, such as cryptographic public keys, which can be used to authenticate or authorize interaction with the `controller` or associated parties. Verification methods might take many parameters.</div> *)
  val security_c_VerificationMethod : Iri.t

  (** <div>This class represents a verification key.</div> *)
  val security_c_X25519KeyAgreementKey2019 : Iri.t

  (** <div>An action that the controller of a capability may take when invoking the capability.</div> *)
  val security_allowedAction : Iri.t

  (** <div>A `blockchainAccountId` property is used to specify a blockchain account identifier, as per the <a href="https://github.com/ChainAgnostic/CAIPs/blob/master/CAIPs/caip-10.md">CAIP-10Account ID Specification</a>.</div> *)
  val security_blockchainAccountId : Iri.t

  (** <div>The canonicalization algorithm is used to transform the input data into a form that can be passed to a cryptographic digest method. The digest is then digitally signed using a digital signature algorithm. Canonicalization ensures that a piece of software that is generating a digital signature is able to do so on the same set of information in a deterministic manner.</div> *)
  val security_canonicalizationAlgorithm : Iri.t

  (** <div>An action that can be taken given a capability.</div> *)
  val security_capabilityAction : Iri.t

  (** <div>An list of delegated capabilities from a delegator to a delegatee.</div> *)
  val security_capabilityChain : Iri.t

  (** <div>A restriction on the way the capability may be used.</div> *)
  val security_caveat : Iri.t

  (** <div>The challenge property is used to associate a challenge with a proof, for use with a `proofPurpose` such as `authentication`. This string value SHOULD be included in a proof if a `domain` is specified.</div> *)
  val security_challenge : Iri.t

  (** <div>The cipher algorithm describes the mechanism used to encrypt a message. It is typically a string expressing the cipher suite, the strength of the cipher, and a block cipher mode.</div> *)
  val security_cipherAlgorithm : Iri.t

  (** <div>Cipher data is an opaque blob of information that is used to specify an encrypted message.</div> *)
  val security_cipherData : Iri.t

  (** <div>A cipher key is a symmetric key that is used to encrypt or decrypt a piece of information. The key itself may be expressed in clear text or encrypted.</div> *)
  val security_cipherKey : Iri.t

  (** <div>A controller is an entity that claims control over a particular resource. Note that control is best validated as a two-way relationship, where the controller claims control over a particular resource, and the resource clearly identifies its controller.</div> *)
  val security_controller : Iri.t

  (** <div>A text-based identifier for a specific cryptographic suite.</div> *)
  val security_cryptosuite : Iri.t

  (** <div>An entity that delegates a capability to a delegatee.</div> *)
  val security_delegator : Iri.t

  (** <div>The digest algorithm is used to specify the cryptographic function to use when generating the data to be digitally signed. Typically, data that is to be signed goes through three steps: 1) canonicalization, 2) digest, and 3) signature. This property is used to specify the algorithm that should be used for step 2. A signature class typically specifies a default digest method, so this property is typically used to specify information for a signature algorithm.</div> *)
  val security_digestAlgorithm : Iri.t

  (** <div>The digest value is used to express the output of the digest algorithm expressed in Base-16 (hexadecimal) format.</div> *)
  val security_digestValue : Iri.t

  (** <div>The `domain` property is used to associate a domain with a proof, for use with a `proofPurpose` such as `authentication` and indicating its intended usage.</div> *)
  val security_domain : Iri.t

  (** <div>An `ethereumAddress` property is used to specify the Ethereum address. As per the Ethereum Yellow Paper "Ethereum: a secure decentralised generalised transaction ledger" in consists of a prefix "0x", a common identifier for hexadecimal, concatenated with the rightmost 20 bytes of the Keccak-256 hash (big endian) of the ECDSA public key (the curve used is the so-called secp256k1). In hexadecimal, 2 digits represent a byte, meaning addresses contain 40 hexadecimal digits. The Ethereum address should also contain a checksum as per EIP-55.</div> *)
  val security_ethereumAddress : Iri.t

  (** <div>The `expirationDate` property is used to associate an expiration date with a proof.</div> *)
  val security_expirationDate : Iri.t

  (** <div>The expiration time is typically associated with a <a href="#Key">`Key`</a> and specifies when the validity of the key will expire.</div> *)
  val security_expires : Iri.t

  (** <div>The initialization vector (IV) is a byte stream that is typically used to initialize certain block cipher encryption schemes. For a receiving application to be able to decrypt a message, it must know the decryption key and the initialization vector. The value is typically base-64 encoded.</div> *)
  val security_initializationVector : Iri.t

  (** <div>An invocation target identifies where a capability may be invoked, and identifies the target object for which the root capability expresses authority.</div> *)
  val security_invocationTarget : Iri.t

  (** <div>An identifier to cryptographic material that can invoke a capability.</div> *)
  val security_invoker : Iri.t

  (** <div>The jws property is used to associate a detached Json Web Signature with a proof.</div> *)
  val security_jws : Iri.t

  (** <div>This property is used in conjunction with the input to the signature hashing function in order to protect against replay attacks. Typically, receivers need to track all nonce values used within a certain time period in order to ensure that an attacker cannot merely re-send a compromised packet in order to execute a privileged request.</div> *)
  val security_nonce : Iri.t

  (** <div>An owner is an entity that claims control over a particular resource. Note that ownership is best validated as a two-way relationship where the owner claims ownership over a particular resource, and the resource clearly identifies its owner.</div> *)
  val security_owner : Iri.t

  (** <div>A secret that is used to generate a key that can be used to encrypt or decrypt message. It is typically a string value.</div> *)
  val security_password : Iri.t

  (** <div>A private key PEM property is used to specify the PEM-encoded version of the private key. This encoding is compatible with almost every Secure Sockets Layer library implementation and typically plugs directly into functions intializing private keys.</div> *)
  val security_privateKeyPem : Iri.t

  (** <div>The` proofPurpose` property is used to associate a purpose, such as `assertionMethod` or `authentication` with a proof. The proof purpose acts as a safeguard to prevent the proof from being misused by being applied to a purpose other than the one that was intended.</div> *)
  val security_proofPurpose : Iri.t

  (** <div>A string value that contains the data necessary to verify the digital proof using the `verificationMethod` specified</div> *)
  val security_proofValue : Iri.t

  (** <div>A public key property is used to specify a URL that contains information about a public key.</div> *)
  val security_publicKey : Iri.t

  (** <div>A public key Base58 property is used to specify the base58-encoded version of the public key.</div> *)
  val security_publicKeyBase58 : Iri.t

  (** <div>A `publicKeyHex` property is used to specify the hex-encoded version of the public key, based on section 8 of rfc4648. Hex encoding is also known as Base16 encoding.</div> *)
  val security_publicKeyHex : Iri.t

  (** <div>See the JOSE suite.</div> *)
  val security_publicKeyJwk : Iri.t

  (** <div><p>The public key multibase property is used to specify the multibase-encoded version of a public key. The contents of the property are defined by specifications such as ED25519-2020 and listed in the Linked Data Cryptosuite Registry. Most public key type definitions are expected to:</p>
<ul>
<li>Specify only a single encoding base per public key type as it reduces implementation burden and increases the chances of reaching broad interoperability.
<li>Specify a multicodec header on the encoded public key to aid encoding and decoding applications in confirming that they are serializing and deserializing an expected public key type.
<li>Use compressed binary formats to ensure efficient key sizes.
</ul></div> *)
  val security_publicKeyMultibase : Iri.t

  (** <div>A public key PEM property is used to specify the PEM-encoded version of the public key. This encoding is compatible with almost every Secure Sockets Layer library implementation and typically plugs directly into functions initializing public keys.</div> *)
  val security_publicKeyPem : Iri.t

  (** <div>The publicKeyService property is used to express the REST URL that provides public key management services.</div> *)
  val security_publicKeyService : Iri.t

  (** <div>The revocation time is typically associated with a <a href="#Key">`Key`</a> that has been marked as invalid as of the date and time associated with the property. Key revocations are often used when a key is compromised, such as the theft of the private key, or during the course of best-practice key rotation schedules.</div> *)
  val security_revoked : Iri.t

  (** <div>Examples of specific services include discovery services, social networks, file storage services, and verifiable claim repository services.</div> *)
  val security_service : Iri.t

  (** <div>A network address at which a service operates on behalf of a controller. Examples of specific services include discovery services, social networks, file storage services, and verifiable claim repository services. Service endpoints might also be provided by a generalized data interchange protocol, such as extensible data interchange.</div> *)
  val security_serviceEndpoint : Iri.t

  (** <div>The property is used to associate a proof with a graph of information. The proof property is typically not included in the canonicalized graph that is then digested, and digitally signed.</div> *)
  val security_signature : Iri.t

  (** <div>The signature algorithm is used to specify the cryptographic signature function to use when digitally signing the digest data. Typically, text to be signed goes through three steps: 1) canonicalization, 2) digest, and 3) signature. This property is used to specify the algorithm that should be used for step #3. A signature class typically specifies a default signature algorithm, so this property rarely needs to be used in practice when specifying digital signatures.</div> *)
  val security_signatureAlgorithm : Iri.t

  (** <div>The signature value is used to express the output of the signature algorithm expressed in base-64 format.</div> *)
  val security_signatureValue : Iri.t

  (** <div>The x509CertificateChain property is used to associate a chain of X.509 Certificates with a proof. The value of this property is an ordered list where each value in the list is an X.509 Certificate expressed as a DER PKIX format, that is encoded with multibase using the base64pad variant. The certificate directly associated to the verification method used to verify the proof MUST be the first element in the list. Subsequent certificates in the list MAY be included where each one MUST certify the previous one.</div> *)
  val security_x509CertificateChain : Iri.t

  (** <div>The x509CertificateFingerprint property is used to associate an X.509 Certificate with a proof via its fingerprint. The value is a multihash encoded then multibase encoded value using the base64pad variant. It is RECOMMENDED that the fingerprint value be the SHA-256 hash of the X.509 Certificate.</div> *)
  val security_x509CertificateFingerprint : Iri.t

end

class from : ?sub: Term.term -> Graph.graph ->
  object
    method allowedAction : Term.term list
    method allowedAction_opt : Term.term option
    method allowedAction_iris : Iri.t list
    method allowedAction_opt_iri : Iri.t option
    method blockchainAccountId : Term.literal list
    method blockchainAccountId_opt : Term.literal option
    method canonicalizationAlgorithm : Term.term list
    method canonicalizationAlgorithm_opt : Term.term option
    method canonicalizationAlgorithm_iris : Iri.t list
    method canonicalizationAlgorithm_opt_iri : Iri.t option
    method capabilityAction : Term.term list
    method capabilityAction_opt : Term.term option
    method capabilityAction_iris : Iri.t list
    method capabilityAction_opt_iri : Iri.t option
    method capabilityChain : Term.term list
    method capabilityChain_opt : Term.term option
    method capabilityChain_iris : Iri.t list
    method capabilityChain_opt_iri : Iri.t option
    method caveat : Term.term list
    method caveat_opt : Term.term option
    method caveat_iris : Iri.t list
    method caveat_opt_iri : Iri.t option
    method challenge : Term.literal list
    method challenge_opt : Term.literal option
    method cipherAlgorithm : Term.literal list
    method cipherAlgorithm_opt : Term.literal option
    method cipherData : Term.literal list
    method cipherData_opt : Term.literal option
    method cipherKey : Term.literal list
    method cipherKey_opt : Term.literal option
    method controller : Term.term list
    method controller_opt : Term.term option
    method controller_iris : Iri.t list
    method controller_opt_iri : Iri.t option
    method cryptosuite : Term.literal list
    method cryptosuite_opt : Term.literal option
    method delegator : Term.term list
    method delegator_opt : Term.term option
    method delegator_iris : Iri.t list
    method delegator_opt_iri : Iri.t option
    method digestAlgorithm : Term.literal list
    method digestAlgorithm_opt : Term.literal option
    method digestValue : Term.literal list
    method digestValue_opt : Term.literal option
    method domain : Term.literal list
    method domain_opt : Term.literal option
    method ethereumAddress : Term.literal list
    method ethereumAddress_opt : Term.literal option
    method expirationDate : Term.term list
    method expirationDate_opt : Term.term option
    method expirationDate_iris : Iri.t list
    method expirationDate_opt_iri : Iri.t option
    method expires : Term.term list
    method expires_opt : Term.term option
    method expires_iris : Iri.t list
    method expires_opt_iri : Iri.t option
    method initializationVector : Term.literal list
    method initializationVector_opt : Term.literal option
    method invocationTarget : Term.term list
    method invocationTarget_opt : Term.term option
    method invocationTarget_iris : Iri.t list
    method invocationTarget_opt_iri : Iri.t option
    method invoker : Term.term list
    method invoker_opt : Term.term option
    method invoker_iris : Iri.t list
    method invoker_opt_iri : Iri.t option
    method jws : Term.term list
    method jws_opt : Term.term option
    method jws_iris : Iri.t list
    method jws_opt_iri : Iri.t option
    method nonce : Term.literal list
    method nonce_opt : Term.literal option
    method owner : Term.term list
    method owner_opt : Term.term option
    method owner_iris : Iri.t list
    method owner_opt_iri : Iri.t option
    method password : Term.literal list
    method password_opt : Term.literal option
    method privateKeyPem : Term.literal list
    method privateKeyPem_opt : Term.literal option
    method proofPurpose : Term.literal list
    method proofPurpose_opt : Term.literal option
    method proofValue : Term.literal list
    method proofValue_opt : Term.literal option
    method publicKey : Term.term list
    method publicKey_opt : Term.term option
    method publicKey_iris : Iri.t list
    method publicKey_opt_iri : Iri.t option
    method publicKeyBase58 : Term.literal list
    method publicKeyBase58_opt : Term.literal option
    method publicKeyHex : Term.literal list
    method publicKeyHex_opt : Term.literal option
    method publicKeyJwk : Term.literal list
    method publicKeyJwk_opt : Term.literal option
    method publicKeyMultibase : Term.literal list
    method publicKeyMultibase_opt : Term.literal option
    method publicKeyPem : Term.literal list
    method publicKeyPem_opt : Term.literal option
    method publicKeyService : Term.term list
    method publicKeyService_opt : Term.term option
    method publicKeyService_iris : Iri.t list
    method publicKeyService_opt_iri : Iri.t option
    method revoked : Term.term list
    method revoked_opt : Term.term option
    method revoked_iris : Iri.t list
    method revoked_opt_iri : Iri.t option
    method service : Term.term list
    method service_opt : Term.term option
    method service_iris : Iri.t list
    method service_opt_iri : Iri.t option
    method serviceEndpoint : Term.term list
    method serviceEndpoint_opt : Term.term option
    method serviceEndpoint_iris : Iri.t list
    method serviceEndpoint_opt_iri : Iri.t option
    method signature : Term.term list
    method signature_opt : Term.term option
    method signature_iris : Iri.t list
    method signature_opt_iri : Iri.t option
    method signatureAlgorithm : Term.term list
    method signatureAlgorithm_opt : Term.term option
    method signatureAlgorithm_iris : Iri.t list
    method signatureAlgorithm_opt_iri : Iri.t option
    method signatureValue : Term.literal list
    method signatureValue_opt : Term.literal option
    method x509CertificateChain : Term.term list
    method x509CertificateChain_opt : Term.term option
    method x509CertificateChain_iris : Iri.t list
    method x509CertificateChain_opt_iri : Iri.t option
    method x509CertificateFingerprint : Term.term list
    method x509CertificateFingerprint_opt : Term.term option
    method x509CertificateFingerprint_iris : Iri.t list
    method x509CertificateFingerprint_opt_iri : Iri.t option
  end
