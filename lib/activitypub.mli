(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Elements of Activitypub extension of [https://www.w3.org/ns/activitystreams#] *)

include (module type of Activitystreams)

val endpoints : Iri.t
val following : Iri.t
val followers : Iri.t
val inbox : Iri.t
val liked : Iri.t
val shares : Iri.t
val likes : Iri.t
val oauthAuthorizationEndpoint : Iri.t
val oauthTokenEndpoint : Iri.t
val outbox : Iri.t
val preferredUsername : Iri.t
val provideClientKey : Iri.t
val proxyUrl : Iri.t
val sharedInbox : Iri.t
val signClientKey : Iri.t
val source : Iri.t
val streams : Iri.t
val uploadMedia : Iri.t


module Open : sig
  include (module type of Activitystreams.Open)
   val activitypub_endpoints : Iri.t
    val activitypub_following : Iri.t
    val activitypub_followers : Iri.t
    val activitypub_inbox : Iri.t
    val activitypub_liked : Iri.t
    val activitypub_shares : Iri.t
    val activitypub_likes : Iri.t
    val activitypub_oauthAuthorizationEndpoint : Iri.t
    val activitypub_oauthTokenEndpoint : Iri.t
    val activitypub_outbox : Iri.t
    val activitypub_preferredUsername : Iri.t
    val activitypub_provideClientKey : Iri.t
    val activitypub_proxyUrl : Iri.t
    val activitypub_sharedInbox : Iri.t
    val activitypub_signClientKey : Iri.t
    val activitypub_source : Iri.t
    val activitypub_streams : Iri.t
    val activitypub_uploadMedia : Iri.t
end

class from : ?sub: Term.term -> Graph.graph ->
  object
    inherit Activitystreams.from
    method endpoints : Term.term list
    method endpoints_iris : Iri.t list
    method endpoints_opt : Term.term option
    method followers : Term.term list
    method followers_iris : Iri.t list
    method followers_opt : Term.term option
    method following : Term.term list
    method following_iris : Iri.t list
    method following_opt : Term.term option
    method liked : Term.term list
    method liked_iris : Iri.t list
    method liked_opt : Term.term option
    method likes : Term.term list
    method likes_iris : Iri.t list
    method likes_opt : Term.term option
    method oauthAuthorizationEndpoint : Term.term list
    method oauthAuthorizationEndpoint_iris : Iri.t list
    method oauthAuthorizationEndpoint_opt : Term.term option
    method oauthTokenEndpoint : Term.term list
    method oauthTokenEndpoint_iris : Iri.t list
    method oauthTokenEndpoint_opt : Term.term option
    method outbox : Term.term list
    method outbox_iris : Iri.t list
    method outbox_opt : Term.term option
    method preferredUsername : Term.term list
    method preferredUsername_opt : Term.term option
    method provideClientKey : Term.term list
    method provideClientKey_iris : Iri.t list
    method provideClientKey_opt : Term.term option
    method proxyUrl : Term.term list
    method proxyUrl_iris : Iri.t list
    method proxyUrl_opt : Term.term option
    method sharedInbox : Term.term list
    method sharedInbox_iris : Iri.t list
    method sharedInbox_opt : Term.term option
    method shares : Term.term list
    method shares_iris : Iri.t list
    method shares_opt : Term.term option
    method signClientKey : Term.term list
    method signClientKey_iris : Iri.t list
    method signClientKey_opt : Term.term option
    method source : Term.term list
    method source_opt : Term.term option
    method streams : Term.term list
    method streams_iris : Iri.t list
    method streams_opt : Term.term option
    method uploadMedia : Term.term list
    method uploadMedia_iris : Iri.t list
    method uploadMedia_opt : Term.term option
  end
