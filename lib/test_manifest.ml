(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

let test_manifest_str = "http://www.w3.org/2001/sw/DataAccess/tests/test-manifest#";;
let test_manifest = Iri.of_string test_manifest_str ;;
let test_manifest_ s = Iri.of_string (test_manifest_str ^ s);;

let c_Manifest = test_manifest_ "Manifest" ;;
let c_ManifestEntry = test_manifest_ "ManifestEntry" ;;
let c_NegativeSyntaxTest = test_manifest_ "NegativeSyntaxTest" ;;
let c_NegativeSyntaxTest11 = test_manifest_ "NegativeSyntaxTest11" ;;
let c_NegativeUpdateSyntaxTest11 = test_manifest_ "NegativeUpdateSyntaxTest11" ;;
let c_Notable = test_manifest_ "Notable" ;;
let c_PositiveSyntaxTest = test_manifest_ "PositiveSyntaxTest" ;;
let c_PositiveSyntaxTest11 = test_manifest_ "PositiveSyntaxTest11" ;;
let c_PositiveUpdateSyntaxTest11 = test_manifest_ "PositiveUpdateSyntaxTest11" ;;
let c_QueryEvaluationTest = test_manifest_ "QueryEvaluationTest" ;;
let c_Requirement = test_manifest_ "Requirement" ;;
let c_ResultCardinality = test_manifest_ "ResultCardinality" ;;
let c_TestStatus = test_manifest_ "TestStatus" ;;
let c_UpdateEvaluationTest = test_manifest_ "UpdateEvaluationTest" ;;
let action = test_manifest_ "action" ;;
let entries = test_manifest_ "entries" ;;
let include_ = test_manifest_ "include" ;;
let name = test_manifest_ "name" ;;
let notable = test_manifest_ "notable" ;;
let requires = test_manifest_ "requires" ;;
let result = test_manifest_ "result" ;;
let resultCardinality = test_manifest_ "resultCardinality" ;;
let status = test_manifest_ "status" ;;

module Open = struct
  let test_manifest_c_Manifest = c_Manifest
  let test_manifest_c_ManifestEntry = c_ManifestEntry
  let test_manifest_c_NegativeSyntaxTest = c_NegativeSyntaxTest
  let test_manifest_c_NegativeSyntaxTest11 = c_NegativeSyntaxTest11
  let test_manifest_c_NegativeUpdateSyntaxTest11 = c_NegativeUpdateSyntaxTest11
  let test_manifest_c_Notable = c_Notable
  let test_manifest_c_PositiveSyntaxTest = c_PositiveSyntaxTest
  let test_manifest_c_PositiveSyntaxTest11 = c_PositiveSyntaxTest11
  let test_manifest_c_PositiveUpdateSyntaxTest11 = c_PositiveUpdateSyntaxTest11
  let test_manifest_c_QueryEvaluationTest = c_QueryEvaluationTest
  let test_manifest_c_Requirement = c_Requirement
  let test_manifest_c_ResultCardinality = c_ResultCardinality
  let test_manifest_c_TestStatus = c_TestStatus
  let test_manifest_c_UpdateEvaluationTest = c_UpdateEvaluationTest
  let test_manifest_action = action
  let test_manifest_entries = entries
  let test_manifest_include = include_
  let test_manifest_name = name
  let test_manifest_notable = notable
  let test_manifest_requires = requires
  let test_manifest_result = result
  let test_manifest_resultCardinality = resultCardinality
  let test_manifest_status = status
end

class from ?sub g =
  let sub = match sub with None -> Term.Iri (g.Graph.name()) | Some t -> t in
  object(self)
  method action = g.Graph.objects_of ~sub ~pred: action
  method action_opt = match self#action with [] -> None | x::_ -> Some x
  method action_iris = Graph.only_iris (self#action)
  method action_opt_iri = match self#action_iris with [] -> None | x::_ -> Some x
  method entries = g.Graph.objects_of ~sub ~pred: entries
  method entries_opt = match self#entries with [] -> None | x::_ -> Some x
  method entries_iris = Graph.only_iris (self#entries)
  method entries_opt_iri = match self#entries_iris with [] -> None | x::_ -> Some x
  method include_ = g.Graph.objects_of ~sub ~pred: include_
  method include__opt = match self#include_ with [] -> None | x::_ -> Some x
  method include__iris = Graph.only_iris (self#include_)
  method include__opt_iri = match self#include__iris with [] -> None | x::_ -> Some x
  method name = Graph.literal_objects_of g ~sub ~pred: name
  method name_opt = match self#name with [] -> None | x::_ -> Some x
  method notable = g.Graph.objects_of ~sub ~pred: notable
  method notable_opt = match self#notable with [] -> None | x::_ -> Some x
  method notable_iris = Graph.only_iris (self#notable)
  method notable_opt_iri = match self#notable_iris with [] -> None | x::_ -> Some x
  method requires = g.Graph.objects_of ~sub ~pred: requires
  method requires_opt = match self#requires with [] -> None | x::_ -> Some x
  method requires_iris = Graph.only_iris (self#requires)
  method requires_opt_iri = match self#requires_iris with [] -> None | x::_ -> Some x
  method result = g.Graph.objects_of ~sub ~pred: result
  method result_opt = match self#result with [] -> None | x::_ -> Some x
  method result_iris = Graph.only_iris (self#result)
  method result_opt_iri = match self#result_iris with [] -> None | x::_ -> Some x
  method resultCardinality = g.Graph.objects_of ~sub ~pred: resultCardinality
  method resultCardinality_opt = match self#resultCardinality with [] -> None | x::_ -> Some x
  method resultCardinality_iris = Graph.only_iris (self#resultCardinality)
  method resultCardinality_opt_iri = match self#resultCardinality_iris with [] -> None | x::_ -> Some x
  method status = g.Graph.objects_of ~sub ~pred: status
  method status_opt = match self#status with [] -> None | x::_ -> Some x
  method status_iris = Graph.only_iris (self#status)
  method status_opt_iri = match self#status_iris with [] -> None | x::_ -> Some x
  end
