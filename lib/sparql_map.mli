(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

module T = Sparql_types
val map_opt : ('a -> 'b) -> 'a option -> 'b option
type ('acc, 't) map_fun = 'acc mapper -> 'acc -> 't -> 't
and 'a mapper = {
  var : ('a, Sparql_types.var) map_fun;
  iriref : ('a, Sparql_types.iriref) map_fun;
  prefixed_name : ('a, Sparql_types.prefixed_name) map_fun;
  iriloc : ('a, Sparql_types.iriloc) map_fun;
  iri : ('a, Sparql_types.iri) map_fun;
  rdf_literal : ('a, Sparql_types.rdf_literal) map_fun;
  data_block_value : ('a, Sparql_types.data_block_value) map_fun;
  data_full_block_value :
    ('a, Sparql_types.data_full_block_value) map_fun;
  inline_data_one_var : ('a, Sparql_types.inline_data_one_var) map_fun;
  inline_data_full : ('a, Sparql_types.inline_data_full) map_fun;
  datablock : ('a, Sparql_types.datablock) map_fun;
  values_clause : ('a, Sparql_types.values_clause) map_fun;
  var_or_iri : ('a, Sparql_types.var_or_iri) map_fun;
  blank_node : ('a, Sparql_types.blank_node) map_fun;
  select_var : ('a, Sparql_types.select_var) map_fun;
  select_vars : ('a, Sparql_types.select_vars) map_fun;
  select_clause : ('a, Sparql_types.select_clause) map_fun;
  dataset_clause : ('a, Sparql_types.dataset_clause) map_fun;
  arg_list : ('a, Sparql_types.arg_list) map_fun;
  function_call : ('a, Sparql_types.function_call) map_fun;
  binary_op : ('a, Sparql_types.binary_op) map_fun;
  expr : ('a, Sparql_types.expr) map_fun;
  expression : ('a, Sparql_types.expression) map_fun;
  built_in_call : ('a, Sparql_types.built_in_call) map_fun;
  aggregate : ('a, Sparql_types.aggregate) map_fun;
  group_var : ('a, Sparql_types.group_var) map_fun;
  group_condition : ('a, Sparql_types.group_condition) map_fun;
  constraint_ : ('a, Sparql_types.constraint_) map_fun;
  order_condition : ('a, Sparql_types.order_condition) map_fun;
  limit_offset_clause : ('a, Sparql_types.limit_offset_clause) map_fun;
  solution_modifier : ('a, Sparql_types.solution_modifier) map_fun;
  bind : ('a, Sparql_types.bind) map_fun;
  service_graph_pattern :
    ('a, Sparql_types.service_graph_pattern) map_fun;
  graph_graph_pattern : ('a, Sparql_types.graph_graph_pattern) map_fun;
  graph_pattern_elt : ('a, Sparql_types.graph_pattern_elt) map_fun;
  graph_term : ('a, Sparql_types.graph_term) map_fun;
  var_or_term : ('a, Sparql_types.var_or_term) map_fun;
  path_one_in_prop_set : ('a, Sparql_types.path_one_in_prop_set) map_fun;
  path_primary : ('a, Sparql_types.path_primary) map_fun;
  path_elt : ('a, Sparql_types.path_elt) map_fun;
  path_elt_or_inverse : ('a, Sparql_types.path_elt_or_inverse) map_fun;
  path_sequence : ('a, Sparql_types.path_sequence) map_fun;
  path : ('a, Sparql_types.path) map_fun;
  verb : ('a, Sparql_types.verb) map_fun;
  triples_node : ('a, Sparql_types.triples_node) map_fun;
  graph_node : ('a, Sparql_types.graph_node) map_fun;
  prop_object_list : ('a, Sparql_types.prop_object_list) map_fun;
  triples_block : ('a, Sparql_types.triples_block) map_fun;
  triples_same_subject : ('a, Sparql_types.triples_same_subject) map_fun;
  ggp_sub : ('a, Sparql_types.ggp_sub) map_fun;
  group_graph_pattern : ('a, Sparql_types.group_graph_pattern) map_fun;
  sub_select : ('a, Sparql_types.sub_select) map_fun;
}
val var : 'a -> 'b -> 'c -> 'c
val iriref : 'a -> 'b -> 'c -> 'c
val prefixed_name : 'a -> 'b -> 'c -> 'c
val iriloc : 'a -> 'b -> 'c -> 'c
val iri : 'a mapper -> 'a -> Sparql_types.iri -> Sparql_types.iri
val rdf_literal : 'a -> 'b -> 'c -> 'c
val data_block_value :
  'a mapper ->
  'a ->
  Sparql_types.data_block_value -> Sparql_types.data_block_value
val data_full_block_value :
  'a mapper ->
  'a ->
  Sparql_types.data_full_block_value ->
  Sparql_types.data_full_block_value
val inline_data_one_var :
  'a mapper ->
  'a ->
  Sparql_types.inline_data_one_var ->
  Sparql_types.inline_data_one_var
val inline_data_full :
  'a mapper ->
  'a ->
  Sparql_types.inline_data_full -> Sparql_types.inline_data_full
val datablock :
  'a mapper -> 'a -> Sparql_types.datablock -> Sparql_types.datablock
val values_clause :
  'a mapper ->
  'a ->
  Sparql_types.datablock option -> Sparql_types.datablock option
val var_or_iri :
  'a mapper ->
  'a -> Sparql_types.var_or_iri -> Sparql_types.var_or_iri
val blank_node : 'a -> 'b -> 'c -> 'c
val select_var :
  'a mapper ->
  'a -> Sparql_types.select_var -> Sparql_types.select_var
val select_vars :
  'a mapper ->
  'a -> Sparql_types.select_vars -> Sparql_types.select_vars
val select_clause :
  'a mapper ->
  'a -> Sparql_types.select_clause -> Sparql_types.select_clause
val dataset_clause :
  'a mapper ->
  'a -> Sparql_types.dataset_clause -> Sparql_types.dataset_clause
val arg_list :
  'a mapper -> 'a -> Sparql_types.arg_list -> Sparql_types.arg_list
val function_call :
  'a mapper -> 'a -> T.function_call -> Sparql_types.function_call
val binary_op : 'a -> 'b -> 'c -> 'c
val expr : 'a mapper -> 'a -> Sparql_types.expr -> Sparql_types.expr
val expression : 'a mapper -> 'a -> T.expression -> T.expression
val built_in_call :
  'a mapper ->
  'a -> Sparql_types.built_in_call -> Sparql_types.built_in_call
val aggregate :
  'a mapper -> 'a -> Sparql_types.aggregate -> Sparql_types.aggregate
val group_var :
  'a mapper -> 'a -> Sparql_types.group_var -> Sparql_types.group_var
val group_condition :
  'a mapper ->
  'a -> Sparql_types.group_condition -> Sparql_types.group_condition
val constraint_ :
  'a mapper ->
  'a -> Sparql_types.constraint_ -> Sparql_types.constraint_
val order_condition :
  'a mapper ->
  'a -> Sparql_types.order_condition -> Sparql_types.order_condition
val limit_offset_clause : 'a -> 'b -> 'c -> 'c
val solution_modifier :
  'a mapper ->
  'a ->
  Sparql_types.solution_modifier -> Sparql_types.solution_modifier
val bind : 'a mapper -> 'a -> Sparql_types.bind -> Sparql_types.bind
val service_graph_pattern :
  'a mapper ->
  'a ->
  Sparql_types.service_graph_pattern ->
  Sparql_types.service_graph_pattern
val graph_graph_pattern :
  'a mapper ->
  'a ->
  Sparql_types.graph_graph_pattern ->
  Sparql_types.graph_graph_pattern
val graph_pattern_elt :
  'a mapper ->
  'a ->
  Sparql_types.graph_pattern_elt -> Sparql_types.graph_pattern_elt
val graph_term :
  'a mapper ->
  'a -> Sparql_types.graph_term -> Sparql_types.graph_term
val var_or_term :
  'a mapper ->
  'a -> Sparql_types.var_or_term -> Sparql_types.var_or_term
val path_one_in_prop_set :
  'a mapper ->
  'a ->
  Sparql_types.path_one_in_prop_set ->
  Sparql_types.path_one_in_prop_set
val path_primary :
  'a mapper ->
  'a -> Sparql_types.path_primary -> Sparql_types.path_primary
val path_elt :
  'a mapper -> 'a -> Sparql_types.path_elt -> Sparql_types.path_elt
val path_elt_or_inverse :
  'a mapper ->
  'a ->
  Sparql_types.path_elt_or_inverse ->
  Sparql_types.path_elt_or_inverse
val path_sequence :
  'a mapper ->
  'a ->
  Sparql_types.path_elt_or_inverse list ->
  Sparql_types.path_elt_or_inverse list
val path :
  'a mapper ->
  'a ->
  Sparql_types.path_sequence list -> Sparql_types.path_sequence list
val verb : 'a mapper -> 'a -> Sparql_types.verb -> Sparql_types.verb
val triples_node :
  'a mapper ->
  'a -> Sparql_types.triples_node -> Sparql_types.triples_node
val graph_node :
  'a mapper ->
  'a -> Sparql_types.graph_node -> Sparql_types.graph_node
val prop_object_list :
  'a mapper ->
  'a ->
  Sparql_types.prop_object_list -> Sparql_types.prop_object_list
val triples_block :
  'a mapper ->
  'a -> Sparql_types.triples_block -> Sparql_types.triples_block
val triples_same_subject :
  'a mapper ->
  'a ->
  Sparql_types.triples_same_subject ->
  Sparql_types.triples_same_subject
val ggp_sub :
  'a mapper -> 'a -> Sparql_types.ggp_sub -> Sparql_types.ggp_sub
val group_graph_pattern :
  'a mapper ->
  'a ->
  Sparql_types.group_graph_pattern ->
  Sparql_types.group_graph_pattern
val sub_select :
  'a mapper ->
  'a -> Sparql_types.sub_select -> Sparql_types.sub_select
val default : 'a mapper
