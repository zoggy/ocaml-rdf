(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

let ldp_str = "http://www.w3.org/ns/ldp#";;
let ldp = Iri.of_string ldp_str ;;
let ldp_ s = Iri.of_string (ldp_str ^ s);;

let c_BasicContainer = ldp_ "BasicContainer" ;;
let c_Container = ldp_ "Container" ;;
let c_DirectContainer = ldp_ "DirectContainer" ;;
let c_IndirectContainer = ldp_ "IndirectContainer" ;;
let c_NonRDFSource = ldp_ "NonRDFSource" ;;
let c_Page = ldp_ "Page" ;;
let c_PageSortCriterion = ldp_ "PageSortCriterion" ;;
let c_RDFSource = ldp_ "RDFSource" ;;
let c_Resource = ldp_ "Resource" ;;
let constrainedBy = ldp_ "constrainedBy" ;;
let contains = ldp_ "contains" ;;
let hasMemberRelation = ldp_ "hasMemberRelation" ;;
let inbox = ldp_ "inbox" ;;
let insertedContentRelation = ldp_ "insertedContentRelation" ;;
let isMemberOfRelation = ldp_ "isMemberOfRelation" ;;
let member = ldp_ "member" ;;
let membershipResource = ldp_ "membershipResource" ;;
let pageSequence = ldp_ "pageSequence" ;;
let pageSortCollation = ldp_ "pageSortCollation" ;;
let pageSortCriteria = ldp_ "pageSortCriteria" ;;
let pageSortOrder = ldp_ "pageSortOrder" ;;
let pageSortPredicate = ldp_ "pageSortPredicate" ;;

module Open = struct
  let ldp_c_BasicContainer = c_BasicContainer
  let ldp_c_Container = c_Container
  let ldp_c_DirectContainer = c_DirectContainer
  let ldp_c_IndirectContainer = c_IndirectContainer
  let ldp_c_NonRDFSource = c_NonRDFSource
  let ldp_c_Page = c_Page
  let ldp_c_PageSortCriterion = c_PageSortCriterion
  let ldp_c_RDFSource = c_RDFSource
  let ldp_c_Resource = c_Resource
  let ldp_constrainedBy = constrainedBy
  let ldp_contains = contains
  let ldp_hasMemberRelation = hasMemberRelation
  let ldp_inbox = inbox
  let ldp_insertedContentRelation = insertedContentRelation
  let ldp_isMemberOfRelation = isMemberOfRelation
  let ldp_member = member
  let ldp_membershipResource = membershipResource
  let ldp_pageSequence = pageSequence
  let ldp_pageSortCollation = pageSortCollation
  let ldp_pageSortCriteria = pageSortCriteria
  let ldp_pageSortOrder = pageSortOrder
  let ldp_pageSortPredicate = pageSortPredicate
end

class from ?sub g =
  let sub = match sub with None -> Term.Iri (g.Graph.name()) | Some t -> t in
  object(self)
  method constrainedBy = g.Graph.objects_of ~sub ~pred: constrainedBy
  method constrainedBy_opt = match self#constrainedBy with [] -> None | x::_ -> Some x
  method constrainedBy_iris = Graph.only_iris (self#constrainedBy)
  method constrainedBy_opt_iri = match self#constrainedBy_iris with [] -> None | x::_ -> Some x
  method contains = g.Graph.objects_of ~sub ~pred: contains
  method contains_opt = match self#contains with [] -> None | x::_ -> Some x
  method contains_iris = Graph.only_iris (self#contains)
  method contains_opt_iri = match self#contains_iris with [] -> None | x::_ -> Some x
  method hasMemberRelation = g.Graph.objects_of ~sub ~pred: hasMemberRelation
  method hasMemberRelation_opt = match self#hasMemberRelation with [] -> None | x::_ -> Some x
  method hasMemberRelation_iris = Graph.only_iris (self#hasMemberRelation)
  method hasMemberRelation_opt_iri = match self#hasMemberRelation_iris with [] -> None | x::_ -> Some x
  method inbox = g.Graph.objects_of ~sub ~pred: inbox
  method inbox_opt = match self#inbox with [] -> None | x::_ -> Some x
  method inbox_iris = Graph.only_iris (self#inbox)
  method inbox_opt_iri = match self#inbox_iris with [] -> None | x::_ -> Some x
  method insertedContentRelation = g.Graph.objects_of ~sub ~pred: insertedContentRelation
  method insertedContentRelation_opt = match self#insertedContentRelation with [] -> None | x::_ -> Some x
  method insertedContentRelation_iris = Graph.only_iris (self#insertedContentRelation)
  method insertedContentRelation_opt_iri = match self#insertedContentRelation_iris with [] -> None | x::_ -> Some x
  method isMemberOfRelation = g.Graph.objects_of ~sub ~pred: isMemberOfRelation
  method isMemberOfRelation_opt = match self#isMemberOfRelation with [] -> None | x::_ -> Some x
  method isMemberOfRelation_iris = Graph.only_iris (self#isMemberOfRelation)
  method isMemberOfRelation_opt_iri = match self#isMemberOfRelation_iris with [] -> None | x::_ -> Some x
  method member = g.Graph.objects_of ~sub ~pred: member
  method member_opt = match self#member with [] -> None | x::_ -> Some x
  method member_iris = Graph.only_iris (self#member)
  method member_opt_iri = match self#member_iris with [] -> None | x::_ -> Some x
  method membershipResource = g.Graph.objects_of ~sub ~pred: membershipResource
  method membershipResource_opt = match self#membershipResource with [] -> None | x::_ -> Some x
  method membershipResource_iris = Graph.only_iris (self#membershipResource)
  method membershipResource_opt_iri = match self#membershipResource_iris with [] -> None | x::_ -> Some x
  method pageSequence = g.Graph.objects_of ~sub ~pred: pageSequence
  method pageSequence_opt = match self#pageSequence with [] -> None | x::_ -> Some x
  method pageSequence_iris = Graph.only_iris (self#pageSequence)
  method pageSequence_opt_iri = match self#pageSequence_iris with [] -> None | x::_ -> Some x
  method pageSortCollation = g.Graph.objects_of ~sub ~pred: pageSortCollation
  method pageSortCollation_opt = match self#pageSortCollation with [] -> None | x::_ -> Some x
  method pageSortCollation_iris = Graph.only_iris (self#pageSortCollation)
  method pageSortCollation_opt_iri = match self#pageSortCollation_iris with [] -> None | x::_ -> Some x
  method pageSortCriteria = g.Graph.objects_of ~sub ~pred: pageSortCriteria
  method pageSortCriteria_opt = match self#pageSortCriteria with [] -> None | x::_ -> Some x
  method pageSortCriteria_iris = Graph.only_iris (self#pageSortCriteria)
  method pageSortCriteria_opt_iri = match self#pageSortCriteria_iris with [] -> None | x::_ -> Some x
  method pageSortOrder = g.Graph.objects_of ~sub ~pred: pageSortOrder
  method pageSortOrder_opt = match self#pageSortOrder with [] -> None | x::_ -> Some x
  method pageSortOrder_iris = Graph.only_iris (self#pageSortOrder)
  method pageSortOrder_opt_iri = match self#pageSortOrder_iris with [] -> None | x::_ -> Some x
  method pageSortPredicate = g.Graph.objects_of ~sub ~pred: pageSortPredicate
  method pageSortPredicate_opt = match self#pageSortPredicate with [] -> None | x::_ -> Some x
  method pageSortPredicate_iris = Graph.only_iris (self#pageSortPredicate)
  method pageSortPredicate_opt_iri = match self#pageSortPredicate_iris with [] -> None | x::_ -> Some x
  end
