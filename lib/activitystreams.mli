(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Elements of [https://www.w3.org/ns/activitystreams#] *)

(** [https://www.w3.org/ns/activitystreams#] *)
val activitystreams : Iri.t
val activitystreams_ : string -> Iri.t

(** Actor accepts the Object *)
val c_Accept : Iri.t

(** An Object representing some form of Action that has been taken *)
val c_Activity : Iri.t

(** To Add an Object or Link to Something *)
val c_Add : Iri.t

(** Actor announces the object to the target *)
val c_Announce : Iri.t

(** Represents a software application of any sort *)
val c_Application : Iri.t

(** To Arrive Somewhere (can be used, for instance, to indicate that a particular entity is currently located somewhere, e.g. a "check-in") *)
val c_Arrive : Iri.t

(** A written work. Typically several paragraphs long. For example, a blog post or a news article. *)
val c_Article : Iri.t

(** An audio file *)
val c_Audio : Iri.t

val c_Block : Iri.t

(** An ordered or unordered collection of Objects or Links *)
val c_Collection : Iri.t

(** A subset of items from a Collection *)
val c_CollectionPage : Iri.t

(** To Create Something *)
val c_Create : Iri.t

(** To Delete Something *)
val c_Delete : Iri.t

(** The actor dislikes the object *)
val c_Dislike : Iri.t

(** Represents a digital document/file of any sort *)
val c_Document : Iri.t

(** An Event of any kind *)
val c_Event : Iri.t

(** To flag something (e.g. flag as inappropriate, flag as spam, etc) *)
val c_Flag : Iri.t

(** To Express Interest in Something *)
val c_Follow : Iri.t

(** A Group of any kind. *)
val c_Group : Iri.t

(** Actor is ignoring the Object *)
val c_Ignore : Iri.t

(** An Image file *)
val c_Image : Iri.t

(** An Activity that has no direct object *)
val c_IntransitiveActivity : Iri.t

(** To invite someone or something to something *)
val c_Invite : Iri.t

(** To Join Something *)
val c_Join : Iri.t

(** To Leave Something *)
val c_Leave : Iri.t

(** To Like Something *)
val c_Like : Iri.t

(** Represents a qualified reference to another resource. Patterned after the RFC5988 Web Linking Model *)
val c_Link : Iri.t

(** The actor listened to the object *)
val c_Listen : Iri.t

(** A specialized Link that represents an \@mention *)
val c_Mention : Iri.t

(** The actor is moving the object. The target specifies where the object is moving to. The origin specifies where the object is moving from. *)
val c_Move : Iri.t

(** A Short note, typically less than a single paragraph. A "tweet" is an example, or a "status update" *)
val c_Note : Iri.t

val c_Object : Iri.t

(** To Offer something to someone or something *)
val c_Offer : Iri.t

(** A variation of Collection in which items are strictly ordered *)
val c_OrderedCollection : Iri.t

(** An ordered subset of items from an OrderedCollection *)
val c_OrderedCollectionPage : Iri.t

(** A rdf:List variant for Objects and Links *)
val c_OrderedItems : Iri.t

(** An Organization *)
val c_Organization : Iri.t

(** A Web Page *)
val c_Page : Iri.t

(** A Person *)
val c_Person : Iri.t

(** A physical or logical location *)
val c_Place : Iri.t

(** A Profile Document *)
val c_Profile : Iri.t

(** "Public" audience from activitypub. *)
val c_Public : Iri.t

(** A question of any sort. *)
val c_Question : Iri.t

(** The actor read the object *)
val c_Read : Iri.t

(** Actor rejects the Object *)
val c_Reject : Iri.t

(** Represents a Social Graph relationship between two Individuals (indicated by the 'a' and 'b' properties) *)
val c_Relationship : Iri.t

(** To Remove Something *)
val c_Remove : Iri.t

(** A service provided by some entity *)
val c_Service : Iri.t

(** Actor tentatively accepts the Object *)
val c_TentativeAccept : Iri.t

(** Actor tentatively rejects the object *)
val c_TentativeReject : Iri.t

(** A placeholder for a deleted object *)
val c_Tombstone : Iri.t

(** The actor is traveling to the target. The origin specifies where the actor is traveling from. *)
val c_Travel : Iri.t

(** To Undo Something. This would typically be used to indicate that a previous Activity has been undone. *)
val c_Undo : Iri.t

(** To Update/Modify Something *)
val c_Update : Iri.t

(** A Video document of any kind. *)
val c_Video : Iri.t

(** The actor viewed the object *)
val c_View : Iri.t

(** Specifies the accuracy around the point established by the longitude and latitude *)
val accuracy : Iri.t

(** Subproperty of as:attributedTo that identifies the primary actor *)
val actor : Iri.t

(** The altitude of a place *)
val altitude : Iri.t

(** Describes a possible inclusive answer or option for a question. *)
val anyOf : Iri.t

val attachment : Iri.t

val attachments : Iri.t

(** Identifies an entity to which an object is attributed *)
val attributedTo : Iri.t

val audience : Iri.t

(** Identifies the author of an object. Deprecated. Use as:attributedTo instead *)
val author : Iri.t

val bcc : Iri.t

val bto : Iri.t

val cc : Iri.t

(** The content of the object. *)
val content : Iri.t

(** Specifies the context within which an object exists or an activity was performed *)
val context : Iri.t

val current : Iri.t

(** Specifies the date and time the object was deleted *)
val deleted : Iri.t

(** On a Profile object, describes the object described by the profile *)
val describes : Iri.t

val downstreamDuplicates : Iri.t

(** The duration of the object *)
val duration : Iri.t

(** The ending time of the object *)
val endTime : Iri.t

val first : Iri.t

(** On a Tombstone object, describes the former type of the deleted object *)
val formerType : Iri.t

val generator : Iri.t

(** The display height expressed as device independent pixels *)
val height : Iri.t

(** The target URI of the Link *)
val href : Iri.t

(** A hint about the language of the referenced resource *)
val hreflang : Iri.t

val icon : Iri.t

val id : Iri.t

val image : Iri.t

val inReplyTo : Iri.t

(** Indentifies an object used (or to be used) to complete an activity *)
val instrument : Iri.t

val items : Iri.t

val last : Iri.t

(** The latitude *)
val latitude : Iri.t

val location : Iri.t

(** The longitude *)
val longitude : Iri.t

(** The MIME Media Type *)
val mediaType : Iri.t

val name : Iri.t

val next : Iri.t

val object_ : Iri.t

val objectType : Iri.t

(** Describes a possible exclusive answer or option for a question. *)
val oneOf : Iri.t

(** For certain activities, specifies the entity from which the action is directed. *)
val origin : Iri.t

val partOf : Iri.t

val prev : Iri.t

val preview : Iri.t

val provider : Iri.t

(** Specifies the date and time the object was published *)
val published : Iri.t

(** Specifies a radius around the point established by the longitude and latitude *)
val radius : Iri.t

(** A numeric rating (>= 0.0, <= 5.0) for the object *)
val rating : Iri.t

(** The RFC 5988 or HTML5 Link Relation associated with the Link *)
val rel : Iri.t

(** On a Relationship object, describes the type of relationship *)
val relationship : Iri.t

val replies : Iri.t

val result : Iri.t

(** In a strictly ordered logical collection, specifies the index position of the first item in the items list *)
val startIndex : Iri.t

(** The starting time of the object *)
val startTime : Iri.t

(** On a Relationship object, identifies the subject. e.g. when saying "John is connected to Sally", 'subject' refers to 'John' *)
val subject : Iri.t

(** A short summary of the object *)
val summary : Iri.t

val tag : Iri.t

val tags : Iri.t

val target : Iri.t

val to_ : Iri.t

(** The total number of items in a logical collection *)
val totalItems : Iri.t

(** Identifies the unit of measurement used by the radius, altitude and accuracy properties. The value can be expressed either as one of a set of predefined units or as a well-known common URI that identifies units. *)
val units : Iri.t

(** Specifies when the object was last updated *)
val updated : Iri.t

val upstreamDuplicates : Iri.t

(** Specifies a link to a specific representation of the Object *)
val url : Iri.t

val verb : Iri.t

(** Specifies the preferred display width of the content, expressed in terms of device independent pixels. *)
val width : Iri.t


module Open : sig
  (** Actor accepts the Object *)
  val activitystreams_c_Accept : Iri.t

  (** An Object representing some form of Action that has been taken *)
  val activitystreams_c_Activity : Iri.t

  (** To Add an Object or Link to Something *)
  val activitystreams_c_Add : Iri.t

  (** Actor announces the object to the target *)
  val activitystreams_c_Announce : Iri.t

  (** Represents a software application of any sort *)
  val activitystreams_c_Application : Iri.t

  (** To Arrive Somewhere (can be used, for instance, to indicate that a particular entity is currently located somewhere, e.g. a "check-in") *)
  val activitystreams_c_Arrive : Iri.t

  (** A written work. Typically several paragraphs long. For example, a blog post or a news article. *)
  val activitystreams_c_Article : Iri.t

  (** An audio file *)
  val activitystreams_c_Audio : Iri.t

  val activitystreams_c_Block : Iri.t

  (** An ordered or unordered collection of Objects or Links *)
  val activitystreams_c_Collection : Iri.t

  (** A subset of items from a Collection *)
  val activitystreams_c_CollectionPage : Iri.t

  (** To Create Something *)
  val activitystreams_c_Create : Iri.t

  (** To Delete Something *)
  val activitystreams_c_Delete : Iri.t

  (** The actor dislikes the object *)
  val activitystreams_c_Dislike : Iri.t

  (** Represents a digital document/file of any sort *)
  val activitystreams_c_Document : Iri.t

  (** An Event of any kind *)
  val activitystreams_c_Event : Iri.t

  (** To flag something (e.g. flag as inappropriate, flag as spam, etc) *)
  val activitystreams_c_Flag : Iri.t

  (** To Express Interest in Something *)
  val activitystreams_c_Follow : Iri.t

  (** A Group of any kind. *)
  val activitystreams_c_Group : Iri.t

  (** Actor is ignoring the Object *)
  val activitystreams_c_Ignore : Iri.t

  (** An Image file *)
  val activitystreams_c_Image : Iri.t

  (** An Activity that has no direct object *)
  val activitystreams_c_IntransitiveActivity : Iri.t

  (** To invite someone or something to something *)
  val activitystreams_c_Invite : Iri.t

  (** To Join Something *)
  val activitystreams_c_Join : Iri.t

  (** To Leave Something *)
  val activitystreams_c_Leave : Iri.t

  (** To Like Something *)
  val activitystreams_c_Like : Iri.t

  (** Represents a qualified reference to another resource. Patterned after the RFC5988 Web Linking Model *)
  val activitystreams_c_Link : Iri.t

  (** The actor listened to the object *)
  val activitystreams_c_Listen : Iri.t

  (** A specialized Link that represents an \@mention *)
  val activitystreams_c_Mention : Iri.t

  (** The actor is moving the object. The target specifies where the object is moving to. The origin specifies where the object is moving from. *)
  val activitystreams_c_Move : Iri.t

  (** A Short note, typically less than a single paragraph. A "tweet" is an example, or a "status update" *)
  val activitystreams_c_Note : Iri.t

  val activitystreams_c_Object : Iri.t

  (** To Offer something to someone or something *)
  val activitystreams_c_Offer : Iri.t

  (** A variation of Collection in which items are strictly ordered *)
  val activitystreams_c_OrderedCollection : Iri.t

  (** An ordered subset of items from an OrderedCollection *)
  val activitystreams_c_OrderedCollectionPage : Iri.t

  (** A rdf:List variant for Objects and Links *)
  val activitystreams_c_OrderedItems : Iri.t

  (** An Organization *)
  val activitystreams_c_Organization : Iri.t

  (** A Web Page *)
  val activitystreams_c_Page : Iri.t

  (** A Person *)
  val activitystreams_c_Person : Iri.t

  (** A physical or logical location *)
  val activitystreams_c_Place : Iri.t

  (** A Profile Document *)
  val activitystreams_c_Profile : Iri.t

  (** "Public" audience from activitypub. *)
  val activitystreams_c_Public : Iri.t

  (** A question of any sort. *)
  val activitystreams_c_Question : Iri.t

  (** The actor read the object *)
  val activitystreams_c_Read : Iri.t

  (** Actor rejects the Object *)
  val activitystreams_c_Reject : Iri.t

  (** Represents a Social Graph relationship between two Individuals (indicated by the 'a' and 'b' properties) *)
  val activitystreams_c_Relationship : Iri.t

  (** To Remove Something *)
  val activitystreams_c_Remove : Iri.t

  (** A service provided by some entity *)
  val activitystreams_c_Service : Iri.t

  (** Actor tentatively accepts the Object *)
  val activitystreams_c_TentativeAccept : Iri.t

  (** Actor tentatively rejects the object *)
  val activitystreams_c_TentativeReject : Iri.t

  (** A placeholder for a deleted object *)
  val activitystreams_c_Tombstone : Iri.t

  (** The actor is traveling to the target. The origin specifies where the actor is traveling from. *)
  val activitystreams_c_Travel : Iri.t

  (** To Undo Something. This would typically be used to indicate that a previous Activity has been undone. *)
  val activitystreams_c_Undo : Iri.t

  (** To Update/Modify Something *)
  val activitystreams_c_Update : Iri.t

  (** A Video document of any kind. *)
  val activitystreams_c_Video : Iri.t

  (** The actor viewed the object *)
  val activitystreams_c_View : Iri.t

  (** Specifies the accuracy around the point established by the longitude and latitude *)
  val activitystreams_accuracy : Iri.t

  (** Subproperty of as:attributedTo that identifies the primary actor *)
  val activitystreams_actor : Iri.t

  (** The altitude of a place *)
  val activitystreams_altitude : Iri.t

  (** Describes a possible inclusive answer or option for a question. *)
  val activitystreams_anyOf : Iri.t

  val activitystreams_attachment : Iri.t

  val activitystreams_attachments : Iri.t

  (** Identifies an entity to which an object is attributed *)
  val activitystreams_attributedTo : Iri.t

  val activitystreams_audience : Iri.t

  (** Identifies the author of an object. Deprecated. Use as:attributedTo instead *)
  val activitystreams_author : Iri.t

  val activitystreams_bcc : Iri.t

  val activitystreams_bto : Iri.t

  val activitystreams_cc : Iri.t

  (** The content of the object. *)
  val activitystreams_content : Iri.t

  (** Specifies the context within which an object exists or an activity was performed *)
  val activitystreams_context : Iri.t

  val activitystreams_current : Iri.t

  (** Specifies the date and time the object was deleted *)
  val activitystreams_deleted : Iri.t

  (** On a Profile object, describes the object described by the profile *)
  val activitystreams_describes : Iri.t

  val activitystreams_downstreamDuplicates : Iri.t

  (** The duration of the object *)
  val activitystreams_duration : Iri.t

  (** The ending time of the object *)
  val activitystreams_endTime : Iri.t

  val activitystreams_first : Iri.t

  (** On a Tombstone object, describes the former type of the deleted object *)
  val activitystreams_formerType : Iri.t

  val activitystreams_generator : Iri.t

  (** The display height expressed as device independent pixels *)
  val activitystreams_height : Iri.t

  (** The target URI of the Link *)
  val activitystreams_href : Iri.t

  (** A hint about the language of the referenced resource *)
  val activitystreams_hreflang : Iri.t

  val activitystreams_icon : Iri.t

  val activitystreams_id : Iri.t

  val activitystreams_image : Iri.t

  val activitystreams_inReplyTo : Iri.t

  (** Indentifies an object used (or to be used) to complete an activity *)
  val activitystreams_instrument : Iri.t

  val activitystreams_items : Iri.t

  val activitystreams_last : Iri.t

  (** The latitude *)
  val activitystreams_latitude : Iri.t

  val activitystreams_location : Iri.t

  (** The longitude *)
  val activitystreams_longitude : Iri.t

  (** The MIME Media Type *)
  val activitystreams_mediaType : Iri.t

  val activitystreams_name : Iri.t

  val activitystreams_next : Iri.t

  val activitystreams_object : Iri.t

  val activitystreams_objectType : Iri.t

  (** Describes a possible exclusive answer or option for a question. *)
  val activitystreams_oneOf : Iri.t

  (** For certain activities, specifies the entity from which the action is directed. *)
  val activitystreams_origin : Iri.t

  val activitystreams_partOf : Iri.t

  val activitystreams_prev : Iri.t

  val activitystreams_preview : Iri.t

  val activitystreams_provider : Iri.t

  (** Specifies the date and time the object was published *)
  val activitystreams_published : Iri.t

  (** Specifies a radius around the point established by the longitude and latitude *)
  val activitystreams_radius : Iri.t

  (** A numeric rating (>= 0.0, <= 5.0) for the object *)
  val activitystreams_rating : Iri.t

  (** The RFC 5988 or HTML5 Link Relation associated with the Link *)
  val activitystreams_rel : Iri.t

  (** On a Relationship object, describes the type of relationship *)
  val activitystreams_relationship : Iri.t

  val activitystreams_replies : Iri.t

  val activitystreams_result : Iri.t

  (** In a strictly ordered logical collection, specifies the index position of the first item in the items list *)
  val activitystreams_startIndex : Iri.t

  (** The starting time of the object *)
  val activitystreams_startTime : Iri.t

  (** On a Relationship object, identifies the subject. e.g. when saying "John is connected to Sally", 'subject' refers to 'John' *)
  val activitystreams_subject : Iri.t

  (** A short summary of the object *)
  val activitystreams_summary : Iri.t

  val activitystreams_tag : Iri.t

  val activitystreams_tags : Iri.t

  val activitystreams_target : Iri.t

  val activitystreams_to : Iri.t

  (** The total number of items in a logical collection *)
  val activitystreams_totalItems : Iri.t

  (** Identifies the unit of measurement used by the radius, altitude and accuracy properties. The value can be expressed either as one of a set of predefined units or as a well-known common URI that identifies units. *)
  val activitystreams_units : Iri.t

  (** Specifies when the object was last updated *)
  val activitystreams_updated : Iri.t

  val activitystreams_upstreamDuplicates : Iri.t

  (** Specifies a link to a specific representation of the Object *)
  val activitystreams_url : Iri.t

  val activitystreams_verb : Iri.t

  (** Specifies the preferred display width of the content, expressed in terms of device independent pixels. *)
  val activitystreams_width : Iri.t

end

class from : ?sub: Term.term -> Graph.graph ->
  object
    method accuracy : Term.literal list
    method accuracy_opt : Term.literal option
    method actor : Term.term list
    method actor_opt : Term.term option
    method actor_iris : Iri.t list
    method actor_opt_iri : Iri.t option
    method altitude : Term.literal list
    method altitude_opt : Term.literal option
    method anyOf : Term.term list
    method anyOf_opt : Term.term option
    method anyOf_iris : Iri.t list
    method anyOf_opt_iri : Iri.t option
    method attachment : Term.term list
    method attachment_opt : Term.term option
    method attachment_iris : Iri.t list
    method attachment_opt_iri : Iri.t option
    method attachments : Term.term list
    method attachments_opt : Term.term option
    method attachments_iris : Iri.t list
    method attachments_opt_iri : Iri.t option
    method attributedTo : Term.term list
    method attributedTo_opt : Term.term option
    method attributedTo_iris : Iri.t list
    method attributedTo_opt_iri : Iri.t option
    method audience : Term.term list
    method audience_opt : Term.term option
    method audience_iris : Iri.t list
    method audience_opt_iri : Iri.t option
    method author : Term.term list
    method author_opt : Term.term option
    method author_iris : Iri.t list
    method author_opt_iri : Iri.t option
    method bcc : Term.term list
    method bcc_opt : Term.term option
    method bcc_iris : Iri.t list
    method bcc_opt_iri : Iri.t option
    method bto : Term.term list
    method bto_opt : Term.term option
    method bto_iris : Iri.t list
    method bto_opt_iri : Iri.t option
    method cc : Term.term list
    method cc_opt : Term.term option
    method cc_iris : Iri.t list
    method cc_opt_iri : Iri.t option
    method content : Term.literal list
    method content_opt : Term.literal option
    method context : Term.term list
    method context_opt : Term.term option
    method context_iris : Iri.t list
    method context_opt_iri : Iri.t option
    method current : Term.term list
    method current_opt : Term.term option
    method current_iris : Iri.t list
    method current_opt_iri : Iri.t option
    method deleted : Term.literal list
    method deleted_opt : Term.literal option
    method describes : Term.term list
    method describes_opt : Term.term option
    method describes_iris : Iri.t list
    method describes_opt_iri : Iri.t option
    method downstreamDuplicates : Term.literal list
    method downstreamDuplicates_opt : Term.literal option
    method duration : Term.literal list
    method duration_opt : Term.literal option
    method endTime : Term.literal list
    method endTime_opt : Term.literal option
    method first : Term.term list
    method first_opt : Term.term option
    method first_iris : Iri.t list
    method first_opt_iri : Iri.t option
    method formerType : Term.term list
    method formerType_opt : Term.term option
    method formerType_iris : Iri.t list
    method formerType_opt_iri : Iri.t option
    method generator : Term.term list
    method generator_opt : Term.term option
    method generator_iris : Iri.t list
    method generator_opt_iri : Iri.t option
    method height : Term.literal list
    method height_opt : Term.literal option
    method href : Term.literal list
    method href_opt : Term.literal option
    method hreflang : Term.literal list
    method hreflang_opt : Term.literal option
    method icon : Term.term list
    method icon_opt : Term.term option
    method icon_iris : Iri.t list
    method icon_opt_iri : Iri.t option
    method id : Term.literal list
    method id_opt : Term.literal option
    method image : Term.term list
    method image_opt : Term.term option
    method image_iris : Iri.t list
    method image_opt_iri : Iri.t option
    method inReplyTo : Term.term list
    method inReplyTo_opt : Term.term option
    method inReplyTo_iris : Iri.t list
    method inReplyTo_opt_iri : Iri.t option
    method instrument : Term.term list
    method instrument_opt : Term.term option
    method instrument_iris : Iri.t list
    method instrument_opt_iri : Iri.t option
    method items : Term.term list
    method items_opt : Term.term option
    method items_iris : Iri.t list
    method items_opt_iri : Iri.t option
    method last : Term.term list
    method last_opt : Term.term option
    method last_iris : Iri.t list
    method last_opt_iri : Iri.t option
    method latitude : Term.literal list
    method latitude_opt : Term.literal option
    method location : Term.term list
    method location_opt : Term.term option
    method location_iris : Iri.t list
    method location_opt_iri : Iri.t option
    method longitude : Term.literal list
    method longitude_opt : Term.literal option
    method mediaType : Term.literal list
    method mediaType_opt : Term.literal option
    method name : Term.literal list
    method name_opt : Term.literal option
    method next : Term.term list
    method next_opt : Term.term option
    method next_iris : Iri.t list
    method next_opt_iri : Iri.t option
    method object_ : Term.term list
    method object__opt : Term.term option
    method object__iris : Iri.t list
    method object__opt_iri : Iri.t option
    method objectType : Term.literal list
    method objectType_opt : Term.literal option
    method oneOf : Term.term list
    method oneOf_opt : Term.term option
    method oneOf_iris : Iri.t list
    method oneOf_opt_iri : Iri.t option
    method origin : Term.term list
    method origin_opt : Term.term option
    method origin_iris : Iri.t list
    method origin_opt_iri : Iri.t option
    method partOf : Term.term list
    method partOf_opt : Term.term option
    method partOf_iris : Iri.t list
    method partOf_opt_iri : Iri.t option
    method prev : Term.term list
    method prev_opt : Term.term option
    method prev_iris : Iri.t list
    method prev_opt_iri : Iri.t option
    method preview : Term.term list
    method preview_opt : Term.term option
    method preview_iris : Iri.t list
    method preview_opt_iri : Iri.t option
    method provider : Term.term list
    method provider_opt : Term.term option
    method provider_iris : Iri.t list
    method provider_opt_iri : Iri.t option
    method published : Term.literal list
    method published_opt : Term.literal option
    method radius : Term.literal list
    method radius_opt : Term.literal option
    method rating : Term.literal list
    method rating_opt : Term.literal option
    method rel : Term.literal list
    method rel_opt : Term.literal option
    method relationship : Term.term list
    method relationship_opt : Term.term option
    method relationship_iris : Iri.t list
    method relationship_opt_iri : Iri.t option
    method replies : Term.term list
    method replies_opt : Term.term option
    method replies_iris : Iri.t list
    method replies_opt_iri : Iri.t option
    method result : Term.term list
    method result_opt : Term.term option
    method result_iris : Iri.t list
    method result_opt_iri : Iri.t option
    method startIndex : Term.literal list
    method startIndex_opt : Term.literal option
    method startTime : Term.literal list
    method startTime_opt : Term.literal option
    method subject : Term.term list
    method subject_opt : Term.term option
    method subject_iris : Iri.t list
    method subject_opt_iri : Iri.t option
    method summary : Term.literal list
    method summary_opt : Term.literal option
    method tag : Term.term list
    method tag_opt : Term.term option
    method tag_iris : Iri.t list
    method tag_opt_iri : Iri.t option
    method tags : Term.term list
    method tags_opt : Term.term option
    method tags_iris : Iri.t list
    method tags_opt_iri : Iri.t option
    method target : Term.term list
    method target_opt : Term.term option
    method target_iris : Iri.t list
    method target_opt_iri : Iri.t option
    method to_ : Term.term list
    method to__opt : Term.term option
    method to__iris : Iri.t list
    method to__opt_iri : Iri.t option
    method totalItems : Term.literal list
    method totalItems_opt : Term.literal option
    method units : Term.literal list
    method units_opt : Term.literal option
    method updated : Term.literal list
    method updated_opt : Term.literal option
    method upstreamDuplicates : Term.literal list
    method upstreamDuplicates_opt : Term.literal option
    method url : Term.term list
    method url_opt : Term.term option
    method url_iris : Iri.t list
    method url_opt_iri : Iri.t option
    method verb : Term.literal list
    method verb_opt : Term.literal option
    method width : Term.literal list
    method width_opt : Term.literal option
  end
