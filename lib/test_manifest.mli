(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Elements of [http://www.w3.org/2001/sw/DataAccess/tests/test-manifest#] *)

(** [http://www.w3.org/2001/sw/DataAccess/tests/test-manifest#] *)
val test_manifest : Iri.t
val test_manifest_ : string -> Iri.t

(** The class of manifests *)
val c_Manifest : Iri.t

(** One entry in rdf:type list of entries *)
val c_ManifestEntry : Iri.t

(** A type of test specifically for syntax testing. Syntax tests are not required to have an associated result, only an action. Negative syntax tests are tests of which the result should be a parser error. *)
val c_NegativeSyntaxTest : Iri.t

(** A type of test specifically for syntax testing of new features in the SPARQL1.1 Query Language. Syntax tests are not required to have an associated result, only an action. Negative syntax tests are tests of which the result should be a parser error. *)
val c_NegativeSyntaxTest11 : Iri.t

(** A type of test specifically for syntax testing of SPARQL1.1 Update. Syntax tests are not required to have an associated result, only an action. Negative syntax tests are tests of which the result should be a parser error. *)
val c_NegativeUpdateSyntaxTest11 : Iri.t

(** Requirements for a  particular test *)
val c_Notable : Iri.t

(** A type of test specifically for syntax testing. Syntax tests are not required to have an associated result, only an action. *)
val c_PositiveSyntaxTest : Iri.t

(** A type of test specifically for syntax testing of new features in the SPARQL1.1 Query Language. Syntax tests are not required to have an associated result, only an action. *)
val c_PositiveSyntaxTest11 : Iri.t

(** A type of test specifically for syntax testing of SPARQL1.1 Update. Syntax tests are not required to have an associated result, only an action. *)
val c_PositiveUpdateSyntaxTest11 : Iri.t

(** A type of test specifically for query evaluation testing. Query evaluation tests are required to have an associated input dataset, a query, and an expected output dataset. *)
val c_QueryEvaluationTest : Iri.t

(** Requirements for a  particular test *)
val c_Requirement : Iri.t

(** Potential modes of evaluating a test's results with respect to solution cardinality *)
val c_ResultCardinality : Iri.t

(** Statuses a test can have *)
val c_TestStatus : Iri.t

(** The class of all SPARQL 1.1 Update evaluation tests *)
val c_UpdateEvaluationTest : Iri.t

(** Action to perform *)
val action : Iri.t

(** Connects the manifest resource to rdf:type list of entries *)
val entries : Iri.t

(** Connects the manifest resource to rdf:type list of manifests *)
val include_ : Iri.t

(** Optional name of this entry *)
val name : Iri.t

(** Notable feature of this test (advisory) *)
val notable : Iri.t

(** Required functionality for execution of this test *)
val requires : Iri.t

(** The expected outcome *)
val result : Iri.t

(** Specifies whether passing the test requires strict or lax cardinality adherence *)
val resultCardinality : Iri.t

(** The test status *)
val status : Iri.t


module Open : sig
  (** The class of manifests *)
  val test_manifest_c_Manifest : Iri.t

  (** One entry in rdf:type list of entries *)
  val test_manifest_c_ManifestEntry : Iri.t

  (** A type of test specifically for syntax testing. Syntax tests are not required to have an associated result, only an action. Negative syntax tests are tests of which the result should be a parser error. *)
  val test_manifest_c_NegativeSyntaxTest : Iri.t

  (** A type of test specifically for syntax testing of new features in the SPARQL1.1 Query Language. Syntax tests are not required to have an associated result, only an action. Negative syntax tests are tests of which the result should be a parser error. *)
  val test_manifest_c_NegativeSyntaxTest11 : Iri.t

  (** A type of test specifically for syntax testing of SPARQL1.1 Update. Syntax tests are not required to have an associated result, only an action. Negative syntax tests are tests of which the result should be a parser error. *)
  val test_manifest_c_NegativeUpdateSyntaxTest11 : Iri.t

  (** Requirements for a  particular test *)
  val test_manifest_c_Notable : Iri.t

  (** A type of test specifically for syntax testing. Syntax tests are not required to have an associated result, only an action. *)
  val test_manifest_c_PositiveSyntaxTest : Iri.t

  (** A type of test specifically for syntax testing of new features in the SPARQL1.1 Query Language. Syntax tests are not required to have an associated result, only an action. *)
  val test_manifest_c_PositiveSyntaxTest11 : Iri.t

  (** A type of test specifically for syntax testing of SPARQL1.1 Update. Syntax tests are not required to have an associated result, only an action. *)
  val test_manifest_c_PositiveUpdateSyntaxTest11 : Iri.t

  (** A type of test specifically for query evaluation testing. Query evaluation tests are required to have an associated input dataset, a query, and an expected output dataset. *)
  val test_manifest_c_QueryEvaluationTest : Iri.t

  (** Requirements for a  particular test *)
  val test_manifest_c_Requirement : Iri.t

  (** Potential modes of evaluating a test's results with respect to solution cardinality *)
  val test_manifest_c_ResultCardinality : Iri.t

  (** Statuses a test can have *)
  val test_manifest_c_TestStatus : Iri.t

  (** The class of all SPARQL 1.1 Update evaluation tests *)
  val test_manifest_c_UpdateEvaluationTest : Iri.t

  (** Action to perform *)
  val test_manifest_action : Iri.t

  (** Connects the manifest resource to rdf:type list of entries *)
  val test_manifest_entries : Iri.t

  (** Connects the manifest resource to rdf:type list of manifests *)
  val test_manifest_include : Iri.t

  (** Optional name of this entry *)
  val test_manifest_name : Iri.t

  (** Notable feature of this test (advisory) *)
  val test_manifest_notable : Iri.t

  (** Required functionality for execution of this test *)
  val test_manifest_requires : Iri.t

  (** The expected outcome *)
  val test_manifest_result : Iri.t

  (** Specifies whether passing the test requires strict or lax cardinality adherence *)
  val test_manifest_resultCardinality : Iri.t

  (** The test status *)
  val test_manifest_status : Iri.t

end

class from : ?sub: Term.term -> Graph.graph ->
  object
    method action : Term.term list
    method action_opt : Term.term option
    method action_iris : Iri.t list
    method action_opt_iri : Iri.t option
    method entries : Term.term list
    method entries_opt : Term.term option
    method entries_iris : Iri.t list
    method entries_opt_iri : Iri.t option
    method include_ : Term.term list
    method include__opt : Term.term option
    method include__iris : Iri.t list
    method include__opt_iri : Iri.t option
    method name : Term.literal list
    method name_opt : Term.literal option
    method notable : Term.term list
    method notable_opt : Term.term option
    method notable_iris : Iri.t list
    method notable_opt_iri : Iri.t option
    method requires : Term.term list
    method requires_opt : Term.term option
    method requires_iris : Iri.t list
    method requires_opt_iri : Iri.t option
    method result : Term.term list
    method result_opt : Term.term option
    method result_iris : Iri.t list
    method result_opt_iri : Iri.t option
    method resultCardinality : Term.term list
    method resultCardinality_opt : Term.term option
    method resultCardinality_iris : Iri.t list
    method resultCardinality_opt_iri : Iri.t option
    method status : Term.term list
    method status_opt : Term.term option
    method status_iris : Iri.t list
    method status_opt_iri : Iri.t option
  end
