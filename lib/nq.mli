(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Reading and writing N-quads. *)

open Graph ;;
open Ttl_types;;

type error =
| Parse_error of Loc.loc * string

exception Error of error
val string_of_error : error -> string

(** Input dataset from string. Base IRI is the default graph name. *)
val from_string : Ds.dataset -> string -> unit

(** Same as {!from_string} but read from the given file. *)
val from_file : Ds.dataset -> string -> unit

val to_string : Ds.dataset -> string
val graph_to_string : Graph.graph -> string

val to_file : Ds.dataset -> string -> unit
val graph_to_file : Graph.graph -> string -> unit
