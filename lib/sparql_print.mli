(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

val map_opt : ('a -> 'b) -> 'a option -> 'b option
val do_opt : ('a -> unit) -> 'a option -> unit
val p : Buffer.t -> string -> unit
val pp : Buffer.t -> ('a, Buffer.t, unit) format -> 'a
val print_list :
  ?sep:string -> Buffer.t -> (Buffer.t -> 'a -> unit) -> 'a list -> unit
val print_iriref : Buffer.t -> Sparql_types.iriref -> unit
val print_iriloc : Buffer.t -> Sparql_types.iriloc -> unit
val print_var : Buffer.t -> Sparql_types.var -> unit
val print_bnode : Buffer.t -> Sparql_types.blank_node -> unit
val print_path_mod : Buffer.t -> Sparql_types.path_mod -> unit
val print_iri : Buffer.t -> Sparql_types.iri -> unit
val print_query_prolog_decl :
  Buffer.t -> Sparql_types.query_prolog_decl -> unit
val print_query_prolog :
  Buffer.t -> Sparql_types.query_prolog_decl list -> unit
val print_string_lit : Buffer.t -> string -> unit
val print_rdf_literal : Buffer.t -> Sparql_types.rdf_literal -> unit
val print_data_block_value :
  Buffer.t -> Sparql_types.data_block_value -> unit
val print_data_full_block_value :
  Buffer.t -> Sparql_types.data_full_block_value -> unit
val print_inline_data_one_var :
  Buffer.t -> Sparql_types.inline_data_one_var -> unit
val print_inline_data_full :
  Buffer.t -> Sparql_types.inline_data_full -> unit
val print_datablock : Buffer.t -> Sparql_types.datablock -> unit
val print_values_clause :
  Buffer.t -> Sparql_types.datablock option -> unit
val print_var_or_iri : Buffer.t -> Sparql_types.var_or_iri -> unit
val print_select_var : Buffer.t -> Sparql_types.select_var -> unit
val print_select_vars : Buffer.t -> Sparql_types.select_vars -> unit
val print_select_clause : Buffer.t -> Sparql_types.select_clause -> unit
val print_source_selector :
  Buffer.t -> Sparql_types.source_selector -> unit
val print_dataset_clause :
  Buffer.t -> Sparql_types.dataset_clause -> unit
val print_arg_list : Buffer.t -> Sparql_types.arg_list -> unit
val print_function_call : Buffer.t -> Sparql_types.function_call -> unit
val string_of_bin_op : Sparql_types.binary_op -> string
val print_expr : Buffer.t -> Sparql_types.expr -> unit
val print_expression : Buffer.t -> Sparql_types.expression -> unit
val print_aggregate : Buffer.t -> Sparql_types.aggregate -> unit
val print_built_in_call : Buffer.t -> Sparql_types.built_in_call -> unit
val print_group_condition :
  Buffer.t -> Sparql_types.group_condition -> unit
val print_constraint : Buffer.t -> Sparql_types.having_condition -> unit
val print_having_condition :
  Buffer.t -> Sparql_types.having_condition -> unit
val print_order_condition :
  Buffer.t -> Sparql_types.order_condition -> unit
val print_limit_offset_clause :
  Buffer.t -> Sparql_types.limit_offset_clause -> unit
val print_solution_modifier :
  Buffer.t -> Sparql_types.solution_modifier -> unit
val print_bind : Buffer.t -> Sparql_types.bind -> unit
val print_service_graph_pattern :
  Buffer.t -> Sparql_types.service_graph_pattern -> unit
val print_graph_graph_pattern :
  Buffer.t -> Sparql_types.graph_graph_pattern -> unit
val print_graph_pattern_elt :
  Buffer.t -> Sparql_types.graph_pattern_elt -> unit
val print_graph_term : Buffer.t -> Sparql_types.graph_term -> unit
val print_var_or_term : Buffer.t -> Sparql_types.var_or_term -> unit
val print_path_one_in_prop_set :
  Buffer.t -> Sparql_types.path_one_in_prop_set -> unit
val print_path_primary : Buffer.t -> Sparql_types.path_primary -> unit
val print_path_elt : Buffer.t -> Sparql_types.path_elt -> unit
val print_path_elt_or_inverse :
  Buffer.t -> Sparql_types.path_elt_or_inverse -> unit
val print_path_sequence : Buffer.t -> Sparql_types.path_sequence -> unit
val print_path : Buffer.t -> Sparql_types.path -> unit
val print_verb : Buffer.t -> Sparql_types.verb -> unit
val print_triples_node : Buffer.t -> Sparql_types.triples_node -> unit
val print_graph_node : Buffer.t -> Sparql_types.object_ -> unit
val print_object : Buffer.t -> Sparql_types.object_ -> unit
val print_prop_object_list :
  Buffer.t -> Sparql_types.prop_object_list -> unit
val print_triples_same_subject :
  Buffer.t -> Sparql_types.triples_same_subject -> unit
val print_triples_block : Buffer.t -> Sparql_types.triples_block -> unit
val print_ggp_sub : Buffer.t -> Sparql_types.ggp_sub -> unit
val print_group_graph_pattern :
  Buffer.t -> Sparql_types.group_graph_pattern -> unit
val print_sub_select : Buffer.t -> Sparql_types.sub_select -> unit
val print_select_query : Buffer.t -> Sparql_types.select_query -> unit
val print_triples_template :
  Buffer.t -> Sparql_types.triples_same_subject list -> unit
val print_construct_template :
  Buffer.t -> Sparql_types.triples_same_subject list -> unit
val print_construct_where :
  Buffer.t -> Sparql_types.construct_where -> unit
val print_construct_query :
  Buffer.t -> Sparql_types.construct_query -> unit
val print_describe_query :
  Buffer.t -> Sparql_types.describe_query -> unit
val print_ask_query : Buffer.t -> Sparql_types.ask_query -> unit
val print_query_kind : Buffer.t -> Sparql_types.query_kind -> unit
val print_query : Buffer.t -> Sparql_types.query -> unit
