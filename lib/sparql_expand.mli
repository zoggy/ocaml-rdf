(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

val map_opt : ('a -> 'b) -> 'a option -> 'b option
module SMap = Xml.SMap
type env = { base : Iri.t; prefixes : Iri.t SMap.t; }
type dataset = { from : Ds.name list; from_named : Ds.NameSet.t; }
val create_env : Iri.t -> env
val iriref_a : Sparql_types.iri
val expand_relative_iri : env -> Iri.t -> Iri.t
val expand_iri : env -> Sparql_types.iri -> Sparql_types.iri
val expand_query_prolog_decl :
  env * Sparql_types.query_prolog_decl list ->
  Sparql_types.query_prolog_decl ->
  env * Sparql_types.query_prolog_decl list
val expand_query_prolog :
  env ->
  Sparql_types.query_prolog_decl list ->
  env * Sparql_types.query_prolog_decl list
val expand_rdf_literal :
  env -> Sparql_types.rdf_literal -> Sparql_types.rdf_literal
val expand_data_block_value :
  env ->
  Sparql_types.data_block_value -> Sparql_types.data_block_value
val expand_data_full_block_value :
  env ->
  Sparql_types.data_full_block_value ->
  Sparql_types.data_full_block_value
val expand_inline_data_one_var :
  env ->
  Sparql_types.inline_data_one_var ->
  Sparql_types.inline_data_one_var
val expand_inline_data_full :
  env ->
  Sparql_types.inline_data_full -> Sparql_types.inline_data_full
val expand_datablock :
  env -> Sparql_types.datablock -> Sparql_types.datablock
val expand_values_clause :
  env ->
  Sparql_types.datablock option -> Sparql_types.datablock option
val expand_var_or_iri :
  env -> Sparql_types.var_or_iri -> Sparql_types.var_or_iri
val expand_select_var :
  env -> Sparql_types.select_var -> Sparql_types.select_var
val expand_select_vars :
  env -> Sparql_types.select_vars -> Sparql_types.select_vars
val expand_select_clause :
  env -> Sparql_types.select_clause -> Sparql_types.select_clause
val expand_source_selector :
  env -> Sparql_types.source_selector -> Sparql_types.source_selector
val expand_dataset_clause :
  env -> Sparql_types.dataset_clause -> Sparql_types.dataset_clause
val expand_arg_list :
  env -> Sparql_types.arg_list -> Sparql_types.arg_list
val expand_function_call :
  env -> Sparql_types.function_call -> Sparql_types.function_call
val expand_expr : env -> Sparql_types.expr -> Sparql_types.expr
val expand_expression :
  env -> Sparql_types.expression -> Sparql_types.expression
val expand_aggregate :
  env -> Sparql_types.aggregate -> Sparql_types.aggregate
val expand_built_in_call :
  env -> Sparql_types.built_in_call -> Sparql_types.built_in_call
val expand_group_var :
  env -> Sparql_types.group_var -> Sparql_types.group_var
val expand_group_condition :
  env -> Sparql_types.group_condition -> Sparql_types.group_condition
val expand_constraint :
  env ->
  Sparql_types.having_condition -> Sparql_types.having_condition
val expand_having_condition :
  env ->
  Sparql_types.having_condition -> Sparql_types.having_condition
val expand_order_condition :
  env -> Sparql_types.order_condition -> Sparql_types.order_condition
val expand_limit_offset_clause :
  env ->
  Sparql_types.limit_offset_clause ->
  Sparql_types.limit_offset_clause
val expand_solution_modifier :
  env ->
  Sparql_types.solution_modifier -> Sparql_types.solution_modifier
val expand_bind : env -> Sparql_types.bind -> Sparql_types.bind
val expand_service_graph_pattern :
  env ->
  Sparql_types.service_graph_pattern ->
  Sparql_types.service_graph_pattern
val expand_graph_graph_pattern :
  env ->
  Sparql_types.graph_graph_pattern ->
  Sparql_types.graph_graph_pattern
val expand_graph_pattern_elt :
  env ->
  Sparql_types.graph_pattern_elt -> Sparql_types.graph_pattern_elt
val expand_graph_term :
  env -> Sparql_types.graph_term -> Sparql_types.graph_term
val expand_var_or_term :
  env -> Sparql_types.var_or_term -> Sparql_types.var_or_term
val expand_path_one_in_prop_set :
  env ->
  Sparql_types.path_one_in_prop_set ->
  Sparql_types.path_one_in_prop_set
val expand_path_primary :
  env -> Sparql_types.path_primary -> Sparql_types.path_primary
val expand_path_elt :
  env -> Sparql_types.path_elt -> Sparql_types.path_elt
val expand_path_elt_or_inverse :
  env ->
  Sparql_types.path_elt_or_inverse ->
  Sparql_types.path_elt_or_inverse
val expand_path_sequence :
  env -> Sparql_types.path_sequence -> Sparql_types.path_sequence
val expand_path : env -> Sparql_types.path -> Sparql_types.path
val expand_verb : env -> Sparql_types.verb -> Sparql_types.verb
val expand_triples_node :
  env -> Sparql_types.triples_node -> Sparql_types.triples_node
val expand_graph_node :
  env -> Sparql_types.object_ -> Sparql_types.object_
val expand_object :
  env -> Sparql_types.object_ -> Sparql_types.object_
val expand_prop_object_list :
  env ->
  Sparql_types.prop_object_list -> Sparql_types.prop_object_list
val expand_triples_block :
  env -> Sparql_types.triples_block -> Sparql_types.triples_block
val expand_triples_same_subject :
  env ->
  Sparql_types.triples_same_subject ->
  Sparql_types.triples_same_subject
val expand_ggp_sub :
  env -> Sparql_types.ggp_sub -> Sparql_types.ggp_sub
val expand_group_graph_pattern :
  env ->
  Sparql_types.group_graph_pattern ->
  Sparql_types.group_graph_pattern
val expand_sub_select :
  env -> Sparql_types.sub_select -> Sparql_types.sub_select
val expand_select_query :
  env -> Sparql_types.select_query -> Sparql_types.select_query
val expand_triples_template :
  env ->
  Sparql_types.triples_same_subject list ->
  Sparql_types.triples_same_subject list
val expand_construct_template :
  env ->
  Sparql_types.triples_same_subject list ->
  Sparql_types.triples_same_subject list
val expand_construct_where :
  env -> Sparql_types.construct_where -> Sparql_types.construct_where
val expand_construct_query :
  env -> Sparql_types.construct_query -> Sparql_types.construct_query
val expand_describe_query :
  env -> Sparql_types.describe_query -> Sparql_types.describe_query
val expand_ask_query :
  env -> Sparql_types.ask_query -> Sparql_types.ask_query
val expand_query_kind :
  env -> Sparql_types.query_kind -> Sparql_types.query_kind
val build_dataset : env -> Sparql_types.query_kind -> dataset
val expand_query :
  Iri.t -> Sparql_types.query -> Iri.t * dataset * Sparql_types.query
val expand_update_query :
  Iri.t -> Sparql_types.query -> Iri.t * Sparql_types.query
