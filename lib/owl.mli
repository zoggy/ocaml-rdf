(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Elements of [http://www.w3.org/2002/07/owl#] *)

(** [http://www.w3.org/2002/07/owl#] *)
val owl : Iri.t
val owl_ : string -> Iri.t

(** The class of collections of pairwise different individuals. *)
val c_AllDifferent : Iri.t

(** The class of collections of pairwise disjoint classes. *)
val c_AllDisjointClasses : Iri.t

(** The class of collections of pairwise disjoint properties. *)
val c_AllDisjointProperties : Iri.t

(** The class of annotated annotations for which the RDF serialization consists of an annotated subject, predicate and object. *)
val c_Annotation : Iri.t

(** The class of annotation properties. *)
val c_AnnotationProperty : Iri.t

(** The class of asymmetric properties. *)
val c_AsymmetricProperty : Iri.t

(** The class of annotated axioms for which the RDF serialization consists of an annotated subject, predicate and object. *)
val c_Axiom : Iri.t

(** The class of OWL classes. *)
val c_Class : Iri.t

(** The class of OWL data ranges, which are special kinds of datatypes. Note: The use of the IRI owl:DataRange has been deprecated as of OWL 2. The IRI rdfs:Datatype SHOULD be used instead. *)
val c_DataRange : Iri.t

(** The class of data properties. *)
val c_DatatypeProperty : Iri.t

(** The class of deprecated classes. *)
val c_DeprecatedClass : Iri.t

(** The class of deprecated properties. *)
val c_DeprecatedProperty : Iri.t

(** The class of functional properties. *)
val c_FunctionalProperty : Iri.t

(** The class of inverse-functional properties. *)
val c_InverseFunctionalProperty : Iri.t

(** The class of irreflexive properties. *)
val c_IrreflexiveProperty : Iri.t

(** The class of named individuals. *)
val c_NamedIndividual : Iri.t

(** The class of negative property assertions. *)
val c_NegativePropertyAssertion : Iri.t

(** This is the empty class. *)
val c_Nothing : Iri.t

(** The class of object properties. *)
val c_ObjectProperty : Iri.t

(** The class of ontologies. *)
val c_Ontology : Iri.t

(** The class of ontology properties. *)
val c_OntologyProperty : Iri.t

(** The class of reflexive properties. *)
val c_ReflexiveProperty : Iri.t

(** The class of property restrictions. *)
val c_Restriction : Iri.t

(** The class of symmetric properties. *)
val c_SymmetricProperty : Iri.t

(** The class of OWL individuals. *)
val c_Thing : Iri.t

(** The class of transitive properties. *)
val c_TransitiveProperty : Iri.t

(** The property that determines the class that a universal property restriction refers to. *)
val allValuesFrom : Iri.t

(** The property that determines the predicate of an annotated axiom or annotated annotation. *)
val annotatedProperty : Iri.t

(** The property that determines the subject of an annotated axiom or annotated annotation. *)
val annotatedSource : Iri.t

(** The property that determines the object of an annotated axiom or annotated annotation. *)
val annotatedTarget : Iri.t

(** The property that determines the predicate of a negative property assertion. *)
val assertionProperty : Iri.t

(** The annotation property that indicates that a given ontology is backward compatible with another ontology. *)
val backwardCompatibleWith : Iri.t

(** The data property that does not relate any individual to any data value. *)
val bottomDataProperty : Iri.t

(** The object property that does not relate any two individuals. *)
val bottomObjectProperty : Iri.t

(** The property that determines the cardinality of an exact cardinality restriction. *)
val cardinality : Iri.t

(** The property that determines that a given class is the complement of another class. *)
val complementOf : Iri.t

(** The property that determines that a given data range is the complement of another data range with respect to the data domain. *)
val datatypeComplementOf : Iri.t

(** The annotation property that indicates that a given entity has been deprecated. *)
val deprecated : Iri.t

(** The property that determines that two given individuals are different. *)
val differentFrom : Iri.t

(** The property that determines that a given class is equivalent to the disjoint union of a collection of other classes. *)
val disjointUnionOf : Iri.t

(** The property that determines that two given classes are disjoint. *)
val disjointWith : Iri.t

(** The property that determines the collection of pairwise different individuals in a owl:AllDifferent axiom. *)
val distinctMembers : Iri.t

(** The property that determines that two given classes are equivalent, and that is used to specify datatype definitions. *)
val equivalentClass : Iri.t

(** The property that determines that two given properties are equivalent. *)
val equivalentProperty : Iri.t

(** The property that determines the collection of properties that jointly build a key. *)
val hasKey : Iri.t

(** The property that determines the property that a self restriction refers to. *)
val hasSelf : Iri.t

(** The property that determines the individual that a has-value restriction refers to. *)
val hasValue : Iri.t

(** The annotation property that indicates that a given ontology is incompatible with another ontology. *)
val incompatibleWith : Iri.t

(** The property that determines the collection of classes or data ranges that build an intersection. *)
val intersectionOf : Iri.t

(** The property that determines that two given properties are inverse. *)
val inverseOf : Iri.t

(** The property that determines the cardinality of a maximum cardinality restriction. *)
val maxCardinality : Iri.t

(** The property that determines the cardinality of a maximum qualified cardinality restriction. *)
val maxQualifiedCardinality : Iri.t

(** The property that determines the collection of members in either a owl:AllDifferent, owl:AllDisjointClasses or owl:AllDisjointProperties axiom. *)
val members : Iri.t

(** The property that determines the cardinality of a minimum cardinality restriction. *)
val minCardinality : Iri.t

(** The property that determines the cardinality of a minimum qualified cardinality restriction. *)
val minQualifiedCardinality : Iri.t

(** The property that determines the class that a qualified object cardinality restriction refers to. *)
val onClass : Iri.t

(** The property that determines the data range that a qualified data cardinality restriction refers to. *)
val onDataRange : Iri.t

(** The property that determines the datatype that a datatype restriction refers to. *)
val onDatatype : Iri.t

(** The property that determines the n-tuple of properties that a property restriction on an n-ary data range refers to. *)
val onProperties : Iri.t

(** The property that determines the property that a property restriction refers to. *)
val onProperty : Iri.t

(** The property that determines the collection of individuals or data values that build an enumeration. *)
val oneOf : Iri.t

(** The annotation property that indicates the predecessor ontology of a given ontology. *)
val priorVersion : Iri.t

(** The property that determines the n-tuple of properties that build a sub property chain of a given property. *)
val propertyChainAxiom : Iri.t

(** The property that determines that two given properties are disjoint. *)
val propertyDisjointWith : Iri.t

(** The property that determines the cardinality of an exact qualified cardinality restriction. *)
val qualifiedCardinality : Iri.t

(** The property that determines that two given individuals are equal. *)
val sameAs : Iri.t

(** The property that determines the class that an existential property restriction refers to. *)
val someValuesFrom : Iri.t

(** The property that determines the subject of a negative property assertion. *)
val sourceIndividual : Iri.t

(** The property that determines the object of a negative object property assertion. *)
val targetIndividual : Iri.t

(** The property that determines the value of a negative data property assertion. *)
val targetValue : Iri.t

(** The data property that relates every individual to every data value. *)
val topDataProperty : Iri.t

(** The object property that relates every two individuals. *)
val topObjectProperty : Iri.t

(** The property that determines the collection of classes or data ranges that build a union. *)
val unionOf : Iri.t

(** The annotation property that provides version information for an ontology or another OWL construct. *)
val versionInfo : Iri.t

(** The property that determines the collection of facet-value pairs that define a datatype restriction. *)
val withRestrictions : Iri.t


module Open : sig
  (** The class of collections of pairwise different individuals. *)
  val owl_c_AllDifferent : Iri.t

  (** The class of collections of pairwise disjoint classes. *)
  val owl_c_AllDisjointClasses : Iri.t

  (** The class of collections of pairwise disjoint properties. *)
  val owl_c_AllDisjointProperties : Iri.t

  (** The class of annotated annotations for which the RDF serialization consists of an annotated subject, predicate and object. *)
  val owl_c_Annotation : Iri.t

  (** The class of annotation properties. *)
  val owl_c_AnnotationProperty : Iri.t

  (** The class of asymmetric properties. *)
  val owl_c_AsymmetricProperty : Iri.t

  (** The class of annotated axioms for which the RDF serialization consists of an annotated subject, predicate and object. *)
  val owl_c_Axiom : Iri.t

  (** The class of OWL classes. *)
  val owl_c_Class : Iri.t

  (** The class of OWL data ranges, which are special kinds of datatypes. Note: The use of the IRI owl:DataRange has been deprecated as of OWL 2. The IRI rdfs:Datatype SHOULD be used instead. *)
  val owl_c_DataRange : Iri.t

  (** The class of data properties. *)
  val owl_c_DatatypeProperty : Iri.t

  (** The class of deprecated classes. *)
  val owl_c_DeprecatedClass : Iri.t

  (** The class of deprecated properties. *)
  val owl_c_DeprecatedProperty : Iri.t

  (** The class of functional properties. *)
  val owl_c_FunctionalProperty : Iri.t

  (** The class of inverse-functional properties. *)
  val owl_c_InverseFunctionalProperty : Iri.t

  (** The class of irreflexive properties. *)
  val owl_c_IrreflexiveProperty : Iri.t

  (** The class of named individuals. *)
  val owl_c_NamedIndividual : Iri.t

  (** The class of negative property assertions. *)
  val owl_c_NegativePropertyAssertion : Iri.t

  (** This is the empty class. *)
  val owl_c_Nothing : Iri.t

  (** The class of object properties. *)
  val owl_c_ObjectProperty : Iri.t

  (** The class of ontologies. *)
  val owl_c_Ontology : Iri.t

  (** The class of ontology properties. *)
  val owl_c_OntologyProperty : Iri.t

  (** The class of reflexive properties. *)
  val owl_c_ReflexiveProperty : Iri.t

  (** The class of property restrictions. *)
  val owl_c_Restriction : Iri.t

  (** The class of symmetric properties. *)
  val owl_c_SymmetricProperty : Iri.t

  (** The class of OWL individuals. *)
  val owl_c_Thing : Iri.t

  (** The class of transitive properties. *)
  val owl_c_TransitiveProperty : Iri.t

  (** The property that determines the class that a universal property restriction refers to. *)
  val owl_allValuesFrom : Iri.t

  (** The property that determines the predicate of an annotated axiom or annotated annotation. *)
  val owl_annotatedProperty : Iri.t

  (** The property that determines the subject of an annotated axiom or annotated annotation. *)
  val owl_annotatedSource : Iri.t

  (** The property that determines the object of an annotated axiom or annotated annotation. *)
  val owl_annotatedTarget : Iri.t

  (** The property that determines the predicate of a negative property assertion. *)
  val owl_assertionProperty : Iri.t

  (** The annotation property that indicates that a given ontology is backward compatible with another ontology. *)
  val owl_backwardCompatibleWith : Iri.t

  (** The data property that does not relate any individual to any data value. *)
  val owl_bottomDataProperty : Iri.t

  (** The object property that does not relate any two individuals. *)
  val owl_bottomObjectProperty : Iri.t

  (** The property that determines the cardinality of an exact cardinality restriction. *)
  val owl_cardinality : Iri.t

  (** The property that determines that a given class is the complement of another class. *)
  val owl_complementOf : Iri.t

  (** The property that determines that a given data range is the complement of another data range with respect to the data domain. *)
  val owl_datatypeComplementOf : Iri.t

  (** The annotation property that indicates that a given entity has been deprecated. *)
  val owl_deprecated : Iri.t

  (** The property that determines that two given individuals are different. *)
  val owl_differentFrom : Iri.t

  (** The property that determines that a given class is equivalent to the disjoint union of a collection of other classes. *)
  val owl_disjointUnionOf : Iri.t

  (** The property that determines that two given classes are disjoint. *)
  val owl_disjointWith : Iri.t

  (** The property that determines the collection of pairwise different individuals in a owl:AllDifferent axiom. *)
  val owl_distinctMembers : Iri.t

  (** The property that determines that two given classes are equivalent, and that is used to specify datatype definitions. *)
  val owl_equivalentClass : Iri.t

  (** The property that determines that two given properties are equivalent. *)
  val owl_equivalentProperty : Iri.t

  (** The property that determines the collection of properties that jointly build a key. *)
  val owl_hasKey : Iri.t

  (** The property that determines the property that a self restriction refers to. *)
  val owl_hasSelf : Iri.t

  (** The property that determines the individual that a has-value restriction refers to. *)
  val owl_hasValue : Iri.t

  (** The annotation property that indicates that a given ontology is incompatible with another ontology. *)
  val owl_incompatibleWith : Iri.t

  (** The property that determines the collection of classes or data ranges that build an intersection. *)
  val owl_intersectionOf : Iri.t

  (** The property that determines that two given properties are inverse. *)
  val owl_inverseOf : Iri.t

  (** The property that determines the cardinality of a maximum cardinality restriction. *)
  val owl_maxCardinality : Iri.t

  (** The property that determines the cardinality of a maximum qualified cardinality restriction. *)
  val owl_maxQualifiedCardinality : Iri.t

  (** The property that determines the collection of members in either a owl:AllDifferent, owl:AllDisjointClasses or owl:AllDisjointProperties axiom. *)
  val owl_members : Iri.t

  (** The property that determines the cardinality of a minimum cardinality restriction. *)
  val owl_minCardinality : Iri.t

  (** The property that determines the cardinality of a minimum qualified cardinality restriction. *)
  val owl_minQualifiedCardinality : Iri.t

  (** The property that determines the class that a qualified object cardinality restriction refers to. *)
  val owl_onClass : Iri.t

  (** The property that determines the data range that a qualified data cardinality restriction refers to. *)
  val owl_onDataRange : Iri.t

  (** The property that determines the datatype that a datatype restriction refers to. *)
  val owl_onDatatype : Iri.t

  (** The property that determines the n-tuple of properties that a property restriction on an n-ary data range refers to. *)
  val owl_onProperties : Iri.t

  (** The property that determines the property that a property restriction refers to. *)
  val owl_onProperty : Iri.t

  (** The property that determines the collection of individuals or data values that build an enumeration. *)
  val owl_oneOf : Iri.t

  (** The annotation property that indicates the predecessor ontology of a given ontology. *)
  val owl_priorVersion : Iri.t

  (** The property that determines the n-tuple of properties that build a sub property chain of a given property. *)
  val owl_propertyChainAxiom : Iri.t

  (** The property that determines that two given properties are disjoint. *)
  val owl_propertyDisjointWith : Iri.t

  (** The property that determines the cardinality of an exact qualified cardinality restriction. *)
  val owl_qualifiedCardinality : Iri.t

  (** The property that determines that two given individuals are equal. *)
  val owl_sameAs : Iri.t

  (** The property that determines the class that an existential property restriction refers to. *)
  val owl_someValuesFrom : Iri.t

  (** The property that determines the subject of a negative property assertion. *)
  val owl_sourceIndividual : Iri.t

  (** The property that determines the object of a negative object property assertion. *)
  val owl_targetIndividual : Iri.t

  (** The property that determines the value of a negative data property assertion. *)
  val owl_targetValue : Iri.t

  (** The data property that relates every individual to every data value. *)
  val owl_topDataProperty : Iri.t

  (** The object property that relates every two individuals. *)
  val owl_topObjectProperty : Iri.t

  (** The property that determines the collection of classes or data ranges that build a union. *)
  val owl_unionOf : Iri.t

  (** The annotation property that provides version information for an ontology or another OWL construct. *)
  val owl_versionInfo : Iri.t

  (** The property that determines the collection of facet-value pairs that define a datatype restriction. *)
  val owl_withRestrictions : Iri.t

end

class from : ?sub: Term.term -> Graph.graph ->
  object
    method allValuesFrom : Term.term list
    method allValuesFrom_opt : Term.term option
    method allValuesFrom_iris : Iri.t list
    method allValuesFrom_opt_iri : Iri.t option
    method annotatedProperty : Term.term list
    method annotatedProperty_opt : Term.term option
    method annotatedProperty_iris : Iri.t list
    method annotatedProperty_opt_iri : Iri.t option
    method annotatedSource : Term.term list
    method annotatedSource_opt : Term.term option
    method annotatedSource_iris : Iri.t list
    method annotatedSource_opt_iri : Iri.t option
    method annotatedTarget : Term.term list
    method annotatedTarget_opt : Term.term option
    method annotatedTarget_iris : Iri.t list
    method annotatedTarget_opt_iri : Iri.t option
    method assertionProperty : Term.term list
    method assertionProperty_opt : Term.term option
    method assertionProperty_iris : Iri.t list
    method assertionProperty_opt_iri : Iri.t option
    method backwardCompatibleWith : Term.term list
    method backwardCompatibleWith_opt : Term.term option
    method backwardCompatibleWith_iris : Iri.t list
    method backwardCompatibleWith_opt_iri : Iri.t option
    method bottomDataProperty : Term.literal list
    method bottomDataProperty_opt : Term.literal option
    method bottomObjectProperty : Term.term list
    method bottomObjectProperty_opt : Term.term option
    method bottomObjectProperty_iris : Iri.t list
    method bottomObjectProperty_opt_iri : Iri.t option
    method cardinality : Term.term list
    method cardinality_opt : Term.term option
    method cardinality_iris : Iri.t list
    method cardinality_opt_iri : Iri.t option
    method complementOf : Term.term list
    method complementOf_opt : Term.term option
    method complementOf_iris : Iri.t list
    method complementOf_opt_iri : Iri.t option
    method datatypeComplementOf : Term.literal list
    method datatypeComplementOf_opt : Term.literal option
    method deprecated : Term.term list
    method deprecated_opt : Term.term option
    method deprecated_iris : Iri.t list
    method deprecated_opt_iri : Iri.t option
    method differentFrom : Term.term list
    method differentFrom_opt : Term.term option
    method differentFrom_iris : Iri.t list
    method differentFrom_opt_iri : Iri.t option
    method disjointUnionOf : Term.term list
    method disjointUnionOf_opt : Term.term option
    method disjointUnionOf_iris : Iri.t list
    method disjointUnionOf_opt_iri : Iri.t option
    method disjointWith : Term.term list
    method disjointWith_opt : Term.term option
    method disjointWith_iris : Iri.t list
    method disjointWith_opt_iri : Iri.t option
    method distinctMembers : Term.term list
    method distinctMembers_opt : Term.term option
    method distinctMembers_iris : Iri.t list
    method distinctMembers_opt_iri : Iri.t option
    method equivalentClass : Term.term list
    method equivalentClass_opt : Term.term option
    method equivalentClass_iris : Iri.t list
    method equivalentClass_opt_iri : Iri.t option
    method equivalentProperty : Term.term list
    method equivalentProperty_opt : Term.term option
    method equivalentProperty_iris : Iri.t list
    method equivalentProperty_opt_iri : Iri.t option
    method hasKey : Term.term list
    method hasKey_opt : Term.term option
    method hasKey_iris : Iri.t list
    method hasKey_opt_iri : Iri.t option
    method hasSelf : Term.term list
    method hasSelf_opt : Term.term option
    method hasSelf_iris : Iri.t list
    method hasSelf_opt_iri : Iri.t option
    method hasValue : Term.term list
    method hasValue_opt : Term.term option
    method hasValue_iris : Iri.t list
    method hasValue_opt_iri : Iri.t option
    method incompatibleWith : Term.term list
    method incompatibleWith_opt : Term.term option
    method incompatibleWith_iris : Iri.t list
    method incompatibleWith_opt_iri : Iri.t option
    method intersectionOf : Term.term list
    method intersectionOf_opt : Term.term option
    method intersectionOf_iris : Iri.t list
    method intersectionOf_opt_iri : Iri.t option
    method inverseOf : Term.term list
    method inverseOf_opt : Term.term option
    method inverseOf_iris : Iri.t list
    method inverseOf_opt_iri : Iri.t option
    method maxCardinality : Term.term list
    method maxCardinality_opt : Term.term option
    method maxCardinality_iris : Iri.t list
    method maxCardinality_opt_iri : Iri.t option
    method maxQualifiedCardinality : Term.term list
    method maxQualifiedCardinality_opt : Term.term option
    method maxQualifiedCardinality_iris : Iri.t list
    method maxQualifiedCardinality_opt_iri : Iri.t option
    method members : Term.term list
    method members_opt : Term.term option
    method members_iris : Iri.t list
    method members_opt_iri : Iri.t option
    method minCardinality : Term.term list
    method minCardinality_opt : Term.term option
    method minCardinality_iris : Iri.t list
    method minCardinality_opt_iri : Iri.t option
    method minQualifiedCardinality : Term.term list
    method minQualifiedCardinality_opt : Term.term option
    method minQualifiedCardinality_iris : Iri.t list
    method minQualifiedCardinality_opt_iri : Iri.t option
    method onClass : Term.term list
    method onClass_opt : Term.term option
    method onClass_iris : Iri.t list
    method onClass_opt_iri : Iri.t option
    method onDataRange : Term.literal list
    method onDataRange_opt : Term.literal option
    method onDatatype : Term.literal list
    method onDatatype_opt : Term.literal option
    method onProperties : Term.term list
    method onProperties_opt : Term.term option
    method onProperties_iris : Iri.t list
    method onProperties_opt_iri : Iri.t option
    method onProperty : Term.term list
    method onProperty_opt : Term.term option
    method onProperty_iris : Iri.t list
    method onProperty_opt_iri : Iri.t option
    method oneOf : Term.term list
    method oneOf_opt : Term.term option
    method oneOf_iris : Iri.t list
    method oneOf_opt_iri : Iri.t option
    method priorVersion : Term.term list
    method priorVersion_opt : Term.term option
    method priorVersion_iris : Iri.t list
    method priorVersion_opt_iri : Iri.t option
    method propertyChainAxiom : Term.term list
    method propertyChainAxiom_opt : Term.term option
    method propertyChainAxiom_iris : Iri.t list
    method propertyChainAxiom_opt_iri : Iri.t option
    method propertyDisjointWith : Term.term list
    method propertyDisjointWith_opt : Term.term option
    method propertyDisjointWith_iris : Iri.t list
    method propertyDisjointWith_opt_iri : Iri.t option
    method qualifiedCardinality : Term.term list
    method qualifiedCardinality_opt : Term.term option
    method qualifiedCardinality_iris : Iri.t list
    method qualifiedCardinality_opt_iri : Iri.t option
    method sameAs : Term.term list
    method sameAs_opt : Term.term option
    method sameAs_iris : Iri.t list
    method sameAs_opt_iri : Iri.t option
    method someValuesFrom : Term.term list
    method someValuesFrom_opt : Term.term option
    method someValuesFrom_iris : Iri.t list
    method someValuesFrom_opt_iri : Iri.t option
    method sourceIndividual : Term.term list
    method sourceIndividual_opt : Term.term option
    method sourceIndividual_iris : Iri.t list
    method sourceIndividual_opt_iri : Iri.t option
    method targetIndividual : Term.term list
    method targetIndividual_opt : Term.term option
    method targetIndividual_iris : Iri.t list
    method targetIndividual_opt_iri : Iri.t option
    method targetValue : Term.literal list
    method targetValue_opt : Term.literal option
    method topDataProperty : Term.literal list
    method topDataProperty_opt : Term.literal option
    method topObjectProperty : Term.term list
    method topObjectProperty_opt : Term.term option
    method topObjectProperty_iris : Iri.t list
    method topObjectProperty_opt_iri : Iri.t option
    method unionOf : Term.term list
    method unionOf_opt : Term.term option
    method unionOf_iris : Iri.t list
    method unionOf_opt_iri : Iri.t option
    method versionInfo : Term.term list
    method versionInfo_opt : Term.term option
    method versionInfo_iris : Iri.t list
    method versionInfo_opt_iri : Iri.t option
    method withRestrictions : Term.term list
    method withRestrictions_opt : Term.term option
    method withRestrictions_iris : Iri.t list
    method withRestrictions_opt_iri : Iri.t option
  end
