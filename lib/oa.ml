(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

let oa_str = "http://www.w3.org/ns/oa#";;
let oa = Iri.of_string oa_str ;;
let oa_ s = Iri.of_string (oa_str ^ s);;

let c_Annotation = oa_ "Annotation" ;;
let c_Choice = oa_ "Choice" ;;
let c_CssSelector = oa_ "CssSelector" ;;
let c_CssStyle = oa_ "CssStyle" ;;
let c_DataPositionSelector = oa_ "DataPositionSelector" ;;
let c_Direction = oa_ "Direction" ;;
let c_FragmentSelector = oa_ "FragmentSelector" ;;
let c_HttpRequestState = oa_ "HttpRequestState" ;;
let c_Motivation = oa_ "Motivation" ;;
let c_RangeSelector = oa_ "RangeSelector" ;;
let c_ResourceSelection = oa_ "ResourceSelection" ;;
let c_Selector = oa_ "Selector" ;;
let c_SpecificResource = oa_ "SpecificResource" ;;
let c_State = oa_ "State" ;;
let c_Style = oa_ "Style" ;;
let c_SvgSelector = oa_ "SvgSelector" ;;
let c_TextPositionSelector = oa_ "TextPositionSelector" ;;
let c_TextQuoteSelector = oa_ "TextQuoteSelector" ;;
let c_TextualBody = oa_ "TextualBody" ;;
let c_TimeState = oa_ "TimeState" ;;
let c_XPathSelector = oa_ "XPathSelector" ;;
let annotationService = oa_ "annotationService" ;;
let bodyValue = oa_ "bodyValue" ;;
let cachedSource = oa_ "cachedSource" ;;
let canonical = oa_ "canonical" ;;
let end_ = oa_ "end" ;;
let exact = oa_ "exact" ;;
let hasBody = oa_ "hasBody" ;;
let hasEndSelector = oa_ "hasEndSelector" ;;
let hasPurpose = oa_ "hasPurpose" ;;
let hasScope = oa_ "hasScope" ;;
let hasSelector = oa_ "hasSelector" ;;
let hasSource = oa_ "hasSource" ;;
let hasStartSelector = oa_ "hasStartSelector" ;;
let hasState = oa_ "hasState" ;;
let hasTarget = oa_ "hasTarget" ;;
let motivatedBy = oa_ "motivatedBy" ;;
let prefix = oa_ "prefix" ;;
let processingLanguage = oa_ "processingLanguage" ;;
let refinedBy = oa_ "refinedBy" ;;
let renderedVia = oa_ "renderedVia" ;;
let sourceDate = oa_ "sourceDate" ;;
let sourceDateEnd = oa_ "sourceDateEnd" ;;
let sourceDateStart = oa_ "sourceDateStart" ;;
let start = oa_ "start" ;;
let styleClass = oa_ "styleClass" ;;
let styledBy = oa_ "styledBy" ;;
let suffix = oa_ "suffix" ;;
let textDirection = oa_ "textDirection" ;;
let via = oa_ "via" ;;

module Open = struct
  let oa_c_Annotation = c_Annotation
  let oa_c_Choice = c_Choice
  let oa_c_CssSelector = c_CssSelector
  let oa_c_CssStyle = c_CssStyle
  let oa_c_DataPositionSelector = c_DataPositionSelector
  let oa_c_Direction = c_Direction
  let oa_c_FragmentSelector = c_FragmentSelector
  let oa_c_HttpRequestState = c_HttpRequestState
  let oa_c_Motivation = c_Motivation
  let oa_c_RangeSelector = c_RangeSelector
  let oa_c_ResourceSelection = c_ResourceSelection
  let oa_c_Selector = c_Selector
  let oa_c_SpecificResource = c_SpecificResource
  let oa_c_State = c_State
  let oa_c_Style = c_Style
  let oa_c_SvgSelector = c_SvgSelector
  let oa_c_TextPositionSelector = c_TextPositionSelector
  let oa_c_TextQuoteSelector = c_TextQuoteSelector
  let oa_c_TextualBody = c_TextualBody
  let oa_c_TimeState = c_TimeState
  let oa_c_XPathSelector = c_XPathSelector
  let oa_annotationService = annotationService
  let oa_bodyValue = bodyValue
  let oa_cachedSource = cachedSource
  let oa_canonical = canonical
  let oa_end = end_
  let oa_exact = exact
  let oa_hasBody = hasBody
  let oa_hasEndSelector = hasEndSelector
  let oa_hasPurpose = hasPurpose
  let oa_hasScope = hasScope
  let oa_hasSelector = hasSelector
  let oa_hasSource = hasSource
  let oa_hasStartSelector = hasStartSelector
  let oa_hasState = hasState
  let oa_hasTarget = hasTarget
  let oa_motivatedBy = motivatedBy
  let oa_prefix = prefix
  let oa_processingLanguage = processingLanguage
  let oa_refinedBy = refinedBy
  let oa_renderedVia = renderedVia
  let oa_sourceDate = sourceDate
  let oa_sourceDateEnd = sourceDateEnd
  let oa_sourceDateStart = sourceDateStart
  let oa_start = start
  let oa_styleClass = styleClass
  let oa_styledBy = styledBy
  let oa_suffix = suffix
  let oa_textDirection = textDirection
  let oa_via = via
end

class from ?sub g =
  let sub = match sub with None -> Term.Iri (g.Graph.name()) | Some t -> t in
  object(self)
  method annotationService = g.Graph.objects_of ~sub ~pred: annotationService
  method annotationService_opt = match self#annotationService with [] -> None | x::_ -> Some x
  method annotationService_iris = Graph.only_iris (self#annotationService)
  method annotationService_opt_iri = match self#annotationService_iris with [] -> None | x::_ -> Some x
  method bodyValue = Graph.literal_objects_of g ~sub ~pred: bodyValue
  method bodyValue_opt = match self#bodyValue with [] -> None | x::_ -> Some x
  method cachedSource = g.Graph.objects_of ~sub ~pred: cachedSource
  method cachedSource_opt = match self#cachedSource with [] -> None | x::_ -> Some x
  method cachedSource_iris = Graph.only_iris (self#cachedSource)
  method cachedSource_opt_iri = match self#cachedSource_iris with [] -> None | x::_ -> Some x
  method canonical = g.Graph.objects_of ~sub ~pred: canonical
  method canonical_opt = match self#canonical with [] -> None | x::_ -> Some x
  method canonical_iris = Graph.only_iris (self#canonical)
  method canonical_opt_iri = match self#canonical_iris with [] -> None | x::_ -> Some x
  method end_ = g.Graph.objects_of ~sub ~pred: end_
  method end__opt = match self#end_ with [] -> None | x::_ -> Some x
  method end__iris = Graph.only_iris (self#end_)
  method end__opt_iri = match self#end__iris with [] -> None | x::_ -> Some x
  method exact = Graph.literal_objects_of g ~sub ~pred: exact
  method exact_opt = match self#exact with [] -> None | x::_ -> Some x
  method hasBody = g.Graph.objects_of ~sub ~pred: hasBody
  method hasBody_opt = match self#hasBody with [] -> None | x::_ -> Some x
  method hasBody_iris = Graph.only_iris (self#hasBody)
  method hasBody_opt_iri = match self#hasBody_iris with [] -> None | x::_ -> Some x
  method hasEndSelector = g.Graph.objects_of ~sub ~pred: hasEndSelector
  method hasEndSelector_opt = match self#hasEndSelector with [] -> None | x::_ -> Some x
  method hasEndSelector_iris = Graph.only_iris (self#hasEndSelector)
  method hasEndSelector_opt_iri = match self#hasEndSelector_iris with [] -> None | x::_ -> Some x
  method hasPurpose = g.Graph.objects_of ~sub ~pred: hasPurpose
  method hasPurpose_opt = match self#hasPurpose with [] -> None | x::_ -> Some x
  method hasPurpose_iris = Graph.only_iris (self#hasPurpose)
  method hasPurpose_opt_iri = match self#hasPurpose_iris with [] -> None | x::_ -> Some x
  method hasScope = g.Graph.objects_of ~sub ~pred: hasScope
  method hasScope_opt = match self#hasScope with [] -> None | x::_ -> Some x
  method hasScope_iris = Graph.only_iris (self#hasScope)
  method hasScope_opt_iri = match self#hasScope_iris with [] -> None | x::_ -> Some x
  method hasSelector = g.Graph.objects_of ~sub ~pred: hasSelector
  method hasSelector_opt = match self#hasSelector with [] -> None | x::_ -> Some x
  method hasSelector_iris = Graph.only_iris (self#hasSelector)
  method hasSelector_opt_iri = match self#hasSelector_iris with [] -> None | x::_ -> Some x
  method hasSource = g.Graph.objects_of ~sub ~pred: hasSource
  method hasSource_opt = match self#hasSource with [] -> None | x::_ -> Some x
  method hasSource_iris = Graph.only_iris (self#hasSource)
  method hasSource_opt_iri = match self#hasSource_iris with [] -> None | x::_ -> Some x
  method hasStartSelector = g.Graph.objects_of ~sub ~pred: hasStartSelector
  method hasStartSelector_opt = match self#hasStartSelector with [] -> None | x::_ -> Some x
  method hasStartSelector_iris = Graph.only_iris (self#hasStartSelector)
  method hasStartSelector_opt_iri = match self#hasStartSelector_iris with [] -> None | x::_ -> Some x
  method hasState = g.Graph.objects_of ~sub ~pred: hasState
  method hasState_opt = match self#hasState with [] -> None | x::_ -> Some x
  method hasState_iris = Graph.only_iris (self#hasState)
  method hasState_opt_iri = match self#hasState_iris with [] -> None | x::_ -> Some x
  method hasTarget = g.Graph.objects_of ~sub ~pred: hasTarget
  method hasTarget_opt = match self#hasTarget with [] -> None | x::_ -> Some x
  method hasTarget_iris = Graph.only_iris (self#hasTarget)
  method hasTarget_opt_iri = match self#hasTarget_iris with [] -> None | x::_ -> Some x
  method motivatedBy = g.Graph.objects_of ~sub ~pred: motivatedBy
  method motivatedBy_opt = match self#motivatedBy with [] -> None | x::_ -> Some x
  method motivatedBy_iris = Graph.only_iris (self#motivatedBy)
  method motivatedBy_opt_iri = match self#motivatedBy_iris with [] -> None | x::_ -> Some x
  method prefix = Graph.literal_objects_of g ~sub ~pred: prefix
  method prefix_opt = match self#prefix with [] -> None | x::_ -> Some x
  method processingLanguage = Graph.literal_objects_of g ~sub ~pred: processingLanguage
  method processingLanguage_opt = match self#processingLanguage with [] -> None | x::_ -> Some x
  method refinedBy = g.Graph.objects_of ~sub ~pred: refinedBy
  method refinedBy_opt = match self#refinedBy with [] -> None | x::_ -> Some x
  method refinedBy_iris = Graph.only_iris (self#refinedBy)
  method refinedBy_opt_iri = match self#refinedBy_iris with [] -> None | x::_ -> Some x
  method renderedVia = g.Graph.objects_of ~sub ~pred: renderedVia
  method renderedVia_opt = match self#renderedVia with [] -> None | x::_ -> Some x
  method renderedVia_iris = Graph.only_iris (self#renderedVia)
  method renderedVia_opt_iri = match self#renderedVia_iris with [] -> None | x::_ -> Some x
  method sourceDate = g.Graph.objects_of ~sub ~pred: sourceDate
  method sourceDate_opt = match self#sourceDate with [] -> None | x::_ -> Some x
  method sourceDate_iris = Graph.only_iris (self#sourceDate)
  method sourceDate_opt_iri = match self#sourceDate_iris with [] -> None | x::_ -> Some x
  method sourceDateEnd = g.Graph.objects_of ~sub ~pred: sourceDateEnd
  method sourceDateEnd_opt = match self#sourceDateEnd with [] -> None | x::_ -> Some x
  method sourceDateEnd_iris = Graph.only_iris (self#sourceDateEnd)
  method sourceDateEnd_opt_iri = match self#sourceDateEnd_iris with [] -> None | x::_ -> Some x
  method sourceDateStart = g.Graph.objects_of ~sub ~pred: sourceDateStart
  method sourceDateStart_opt = match self#sourceDateStart with [] -> None | x::_ -> Some x
  method sourceDateStart_iris = Graph.only_iris (self#sourceDateStart)
  method sourceDateStart_opt_iri = match self#sourceDateStart_iris with [] -> None | x::_ -> Some x
  method start = g.Graph.objects_of ~sub ~pred: start
  method start_opt = match self#start with [] -> None | x::_ -> Some x
  method start_iris = Graph.only_iris (self#start)
  method start_opt_iri = match self#start_iris with [] -> None | x::_ -> Some x
  method styleClass = Graph.literal_objects_of g ~sub ~pred: styleClass
  method styleClass_opt = match self#styleClass with [] -> None | x::_ -> Some x
  method styledBy = g.Graph.objects_of ~sub ~pred: styledBy
  method styledBy_opt = match self#styledBy with [] -> None | x::_ -> Some x
  method styledBy_iris = Graph.only_iris (self#styledBy)
  method styledBy_opt_iri = match self#styledBy_iris with [] -> None | x::_ -> Some x
  method suffix = Graph.literal_objects_of g ~sub ~pred: suffix
  method suffix_opt = match self#suffix with [] -> None | x::_ -> Some x
  method textDirection = g.Graph.objects_of ~sub ~pred: textDirection
  method textDirection_opt = match self#textDirection with [] -> None | x::_ -> Some x
  method textDirection_iris = Graph.only_iris (self#textDirection)
  method textDirection_opt_iri = match self#textDirection_iris with [] -> None | x::_ -> Some x
  method via = g.Graph.objects_of ~sub ~pred: via
  method via_opt = match self#via with [] -> None | x::_ -> Some x
  method via_iris = Graph.only_iris (self#via)
  method via_opt_iri = match self#via_iris with [] -> None | x::_ -> Some x
  end
