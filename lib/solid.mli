(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Elements of [http://www.w3.org/ns/solid/terms#] *)

(** [http://www.w3.org/ns/solid/terms#] *)
val solid : Iri.t
val solid_ : string -> Iri.t

(** A Solid account. *)
val c_Account : Iri.t

(** A resource containing notifications. *)
val c_Inbox : Iri.t

(** A notification resource. *)
val c_Notification : Iri.t

(** A resource containing time ordered items and sub-containers.  Sub-containers may be desirable in file based systems to split the timeline into logical components e.g. /yyyy-mm-dd/ as used in ISO 8061. *)
val c_Timeline : Iri.t

(** A index of type registries for resources. Applications can register the RDF type they use and list them in the index resource. *)
val c_TypeIndex : Iri.t

(** A solid account belonging to an Agent. *)
val account : Iri.t

(** Inbox resource for notifications. *)
val inbox : Iri.t

(** Notification resource for an inbox. *)
val notification : Iri.t

(** Indicates if a message has been read or not. This property should have a boolean datatype. *)
val read : Iri.t

(** Timeline for a given resource. *)
val timeline : Iri.t

(** Points to a TypeIndex resource. *)
val typeIndex : Iri.t


module Open : sig
  (** A Solid account. *)
  val solid_c_Account : Iri.t

  (** A resource containing notifications. *)
  val solid_c_Inbox : Iri.t

  (** A notification resource. *)
  val solid_c_Notification : Iri.t

  (** A resource containing time ordered items and sub-containers.  Sub-containers may be desirable in file based systems to split the timeline into logical components e.g. /yyyy-mm-dd/ as used in ISO 8061. *)
  val solid_c_Timeline : Iri.t

  (** A index of type registries for resources. Applications can register the RDF type they use and list them in the index resource. *)
  val solid_c_TypeIndex : Iri.t

  (** A solid account belonging to an Agent. *)
  val solid_account : Iri.t

  (** Inbox resource for notifications. *)
  val solid_inbox : Iri.t

  (** Notification resource for an inbox. *)
  val solid_notification : Iri.t

  (** Indicates if a message has been read or not. This property should have a boolean datatype. *)
  val solid_read : Iri.t

  (** Timeline for a given resource. *)
  val solid_timeline : Iri.t

  (** Points to a TypeIndex resource. *)
  val solid_typeIndex : Iri.t

end

class from : ?sub: Term.term -> Graph.graph ->
  object
    method account : Term.term list
    method account_opt : Term.term option
    method account_iris : Iri.t list
    method account_opt_iri : Iri.t option
    method inbox : Term.term list
    method inbox_opt : Term.term option
    method inbox_iris : Iri.t list
    method inbox_opt_iri : Iri.t option
    method notification : Term.term list
    method notification_opt : Term.term option
    method notification_iris : Iri.t list
    method notification_opt_iri : Iri.t option
    method read : Term.term list
    method read_opt : Term.term option
    method read_iris : Iri.t list
    method read_opt_iri : Iri.t option
    method timeline : Term.term list
    method timeline_opt : Term.term option
    method timeline_iris : Iri.t list
    method timeline_opt_iri : Iri.t option
    method typeIndex : Term.term list
    method typeIndex_opt : Term.term option
    method typeIndex_iris : Iri.t list
    method typeIndex_opt_iri : Iri.t option
  end
