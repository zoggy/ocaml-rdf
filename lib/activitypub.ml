(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

include Activitystreams

let endpoints = activitystreams_"endpoints"
let following = activitystreams_"following"
let followers = activitystreams_"followers"
let inbox = Ldp.inbox
let liked = activitystreams_"liked"
let shares = activitystreams_"shares"
let likes = activitystreams_"likes"
let oauthAuthorizationEndpoint = activitystreams_"oauthAuthorizationEndpoint"
let oauthTokenEndpoint = activitystreams_"oauthTokenEndpoint"
let outbox = activitystreams_"outbox"
let preferredUsername = activitystreams_"preferredUsername"
let provideClientKey = activitystreams_"provideClientKey"
let proxyUrl = activitystreams_"proxyUrl"
let sharedInbox = activitystreams_"sharedInbox"
let signClientKey = activitystreams_"signClientKey"
let source = activitystreams_"source"
let streams = activitystreams_"streams"
let uploadMedia = activitystreams_"uploadMedia"

module Open = struct
  include Activitystreams.Open
  let activitypub_endpoints = endpoints
  let activitypub_following = following
  let activitypub_followers = followers
  let activitypub_inbox = inbox
  let activitypub_liked = liked
  let activitypub_shares = shares
  let activitypub_likes = likes
  let activitypub_oauthAuthorizationEndpoint = oauthAuthorizationEndpoint
  let activitypub_oauthTokenEndpoint = oauthTokenEndpoint
  let activitypub_outbox = outbox
  let activitypub_preferredUsername = preferredUsername
  let activitypub_provideClientKey = provideClientKey
  let activitypub_proxyUrl = proxyUrl
  let activitypub_sharedInbox = sharedInbox
  let activitypub_signClientKey = signClientKey
  let activitypub_source = source
  let activitypub_streams = streams
  let activitypub_uploadMedia = uploadMedia

end

class from ?sub g =
  let sub = match sub with None -> Term.Iri (g.Graph.name()) | Some t -> t in
  object(self)
    inherit Activitystreams.from ~sub g
    method endpoints = g.Graph.objects_of ~sub ~pred: endpoints
    method endpoints_opt = match self#endpoints with [] -> None | x::_ -> Some x
    method endpoints_iris = Graph.only_iris (self#endpoints)

    method following = g.Graph.objects_of ~sub ~pred: following
    method following_opt = match self#following with [] -> None | x::_ -> Some x
    method following_iris = Graph.only_iris (self#following)

    method followers = g.Graph.objects_of ~sub ~pred: followers
    method followers_opt = match self#followers with [] -> None | x::_ -> Some x
    method followers_iris = Graph.only_iris (self#followers)

    method liked = g.Graph.objects_of ~sub ~pred: liked
    method liked_opt = match self#liked with [] -> None | x::_ -> Some x
    method liked_iris = Graph.only_iris (self#liked)

    method shares = g.Graph.objects_of ~sub ~pred: shares
    method shares_opt = match self#shares with [] -> None | x::_ -> Some x
    method shares_iris = Graph.only_iris (self#shares)

    method likes = g.Graph.objects_of ~sub ~pred: likes
    method likes_opt = match self#likes with [] -> None | x::_ -> Some x
    method likes_iris = Graph.only_iris (self#likes)

    method oauthAuthorizationEndpoint = g.Graph.objects_of ~sub ~pred: oauthAuthorizationEndpoint
    method oauthAuthorizationEndpoint_opt = match self#oauthAuthorizationEndpoint with [] -> None | x::_ -> Some x
    method oauthAuthorizationEndpoint_iris = Graph.only_iris (self#oauthAuthorizationEndpoint)

    method oauthTokenEndpoint = g.Graph.objects_of ~sub ~pred: oauthTokenEndpoint
    method oauthTokenEndpoint_opt = match self#oauthTokenEndpoint with [] -> None | x::_ -> Some x
    method oauthTokenEndpoint_iris = Graph.only_iris (self#oauthTokenEndpoint)

    method outbox = g.Graph.objects_of ~sub ~pred: outbox
    method outbox_opt = match self#outbox with [] -> None | x::_ -> Some x
    method outbox_iris = Graph.only_iris (self#outbox)

    method preferredUsername = g.Graph.objects_of ~sub ~pred: preferredUsername
    method preferredUsername_opt = match self#preferredUsername with [] -> None | x::_ -> Some x

    method provideClientKey = g.Graph.objects_of ~sub ~pred: provideClientKey
    method provideClientKey_opt = match self#provideClientKey with [] -> None | x::_ -> Some x
    method provideClientKey_iris = Graph.only_iris (self#provideClientKey)

    method proxyUrl = g.Graph.objects_of ~sub ~pred: proxyUrl
    method proxyUrl_opt = match self#proxyUrl with [] -> None | x::_ -> Some x
    method proxyUrl_iris = Graph.only_iris (self#proxyUrl)

    method sharedInbox = g.Graph.objects_of ~sub ~pred: sharedInbox
    method sharedInbox_opt = match self#sharedInbox with [] -> None | x::_ -> Some x
    method sharedInbox_iris = Graph.only_iris (self#sharedInbox)

    method signClientKey = g.Graph.objects_of ~sub ~pred: signClientKey
    method signClientKey_opt = match self#signClientKey with [] -> None | x::_ -> Some x
    method signClientKey_iris = Graph.only_iris (self#signClientKey)

    method source = g.Graph.objects_of ~sub ~pred: source
    method source_opt = match self#source with [] -> None | x::_ -> Some x

    method streams = g.Graph.objects_of ~sub ~pred: streams
    method streams_opt = match self#streams with [] -> None | x::_ -> Some x
    method streams_iris = Graph.only_iris (self#streams)

    method uploadMedia = g.Graph.objects_of ~sub ~pred: uploadMedia
    method uploadMedia_opt = match self#uploadMedia with [] -> None | x::_ -> Some x
    method uploadMedia_iris = Graph.only_iris (self#uploadMedia)

  end
