(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

module N = Term
type error =
    | Unbound_variable of Sparql_types.var
    | Not_a_integer of Term.literal
    | Not_a_double_or_decimal of Term.literal
    | Type_mismatch of Dt.value * Dt.value
    | Invalid_fun_argument of Iri.t
    | Unknown_fun of Iri.t
    | Invalid_built_in_fun_argument of string * Sparql_types.expression list
    | Unknown_built_in_fun of string
    | No_term
    | Cannot_compare_for_datatype of Iri.t
    | Unhandled_regex_flag of char
    | Incompatible_string_literals of Dt.value * Dt.value
    | Empty_set of string
    | Missing_values_in_inline_data of Sparql_types.inline_data_full
    | Missing_implementation of string
    | No_such_graph of Ds.name

exception Error of error
val error : error -> 'a
val string_of_error : error -> string
module NMap = Ds.NameMap
module NSet = Ds.NameSet
type context = {
  base : Iri.t;
  named : NSet.t;
  dataset : Ds.dataset;
  active : Graph.graph;
  now : Term.datetime ;
}
val context :
  base:Iri.t ->
  ?from:Ds.name list -> ?from_named:NSet.t -> Ds.dataset -> context
module GExprOrdered :
  sig
    type t = Term.term option list
    val compare :
      Term.term option list -> Term.term option list -> int
  end
module GExprMap :
  sig
    type key = GExprOrdered.t
    type 'a t = 'a Map.Make(GExprOrdered).t
    val empty : 'a t
    val is_empty : 'a t -> bool
    val mem : key -> 'a t -> bool
    val add : key -> 'a -> 'a t -> 'a t
    val singleton : key -> 'a -> 'a t
    val remove : key -> 'a t -> 'a t
    val merge :
      (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t
    val compare : ('a -> 'a -> int) -> 'a t -> 'a t -> int
    val equal : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool
    val iter : (key -> 'a -> unit) -> 'a t -> unit
    val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
    val for_all : (key -> 'a -> bool) -> 'a t -> bool
    val exists : (key -> 'a -> bool) -> 'a t -> bool
    val filter : (key -> 'a -> bool) -> 'a t -> 'a t
    val partition : (key -> 'a -> bool) -> 'a t -> 'a t * 'a t
    val cardinal : 'a t -> int
    val bindings : 'a t -> (key * 'a) list
    val min_binding : 'a t -> key * 'a
    val max_binding : 'a t -> key * 'a
    val choose : 'a t -> key * 'a
    val split : key -> 'a t -> 'a t * 'a option * 'a t
    val find : key -> 'a t -> 'a
    val map : ('a -> 'b) -> 'a t -> 'b t
    val mapi : (key -> 'a -> 'b) -> 'a t -> 'b t
  end
val ebv : Dt.value -> bool
val compare : ?sameterm:bool -> Dt.value -> Dt.value -> int
val sortby_compare : Dt.value -> Dt.value -> int
val xsd_datetime : Iri.t
val fun_datetime : Dt.value list -> Dt.value
val iri_funs_ : (Iri.t * (Dt.value list -> Dt.value)) list
val iri_funs :
  (Sparql_ms.VMap.key list -> Sparql_ms.VMap.key) Iri.Map.t ref
val add_iri_fun :
  Iri.Map.key ->
  (Sparql_ms.VMap.key list -> Sparql_ms.VMap.key) -> unit
val bi_bnode :
  string ->
  ('a ->
   Sparql_ms.mu -> Sparql_types.expression -> Sparql_ms.VMap.key) ->
  'a -> Sparql_ms.mu -> Sparql_types.expression list -> Dt.value
val bi_coalesce :
  'a ->
  ('b -> 'c -> 'd -> Dt.value) -> 'b -> 'c -> 'd list -> Dt.value
val bi_datatype :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_if :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_iri :
  string ->
  (context -> 'a -> Sparql_types.expression -> Dt.value) ->
  context -> 'a -> Sparql_types.expression list -> Dt.value
val bi_uri :
  string ->
  (context -> 'a -> Sparql_types.expression -> Dt.value) ->
  context -> 'a -> Sparql_types.expression list -> Dt.value
val bi_isblank :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_isiri :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_isliteral :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_lang :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_isnumeric :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val regex_flag_of_char : char -> [> `CASELESS | `DOTALL | `MULTILINE ]
val bi_regex :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_sameterm :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_str :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_strdt :
  string ->
  (context -> 'a -> Sparql_types.expression -> Dt.value) ->
  context -> 'a -> Sparql_types.expression list -> Dt.value
val bi_strlang :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val string_lit_compatible : 'a * 'b option -> 'c * 'b option -> bool
val bi_strlen :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_substr :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_strends :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_strstarts :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_contains :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_strbefore :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_strafter :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_struuid :
  string ->
  'a -> 'b -> 'c -> Sparql_types.expression list -> Dt.value
val bi_uuid :
  string ->
  'a -> 'b -> 'c -> Sparql_types.expression list -> Dt.value
val bi_encode_for_uri :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_concat :
  'a ->
  ('b -> 'c -> 'd -> Dt.value) -> 'b -> 'c -> 'd list -> Dt.value
val bi_langmatches :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_replace :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_numeric :
  (Dt.value -> Dt.value) ->
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_num_abs : Dt.value -> Dt.value
val bi_num_round : Dt.value -> Dt.value
val bi_num_ceil : Dt.value -> Dt.value
val bi_num_floor : Dt.value -> Dt.value
val bi_rand :
  string ->
  'a -> 'b -> 'c -> Sparql_types.expression list -> Dt.value
val bi_now :
  string ->
  'a -> context -> 'b -> Sparql_types.expression list -> Dt.value
val bi_on_date :
  (Term.datetime -> Dt.value) ->
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value

val bi_date_year : Term.datetime -> Dt.value
val bi_date_month : Term.datetime -> Dt.value
val bi_date_day : Term.datetime -> Dt.value
val bi_date_hours : Term.datetime -> Dt.value
val bi_date_minutes : Term.datetime -> Dt.value
val bi_date_seconds : Term.datetime -> Dt.value
val bi_hash :
  (string -> Dt.value) ->
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_md5 : string -> Dt.value
val bi_sha1 : string -> Dt.value
val bi_sha256 : string -> Dt.value
val bi_lcase :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val bi_ucase :
  string ->
  ('a -> 'b -> Sparql_types.expression -> Dt.value) ->
  'a -> 'b -> Sparql_types.expression list -> Dt.value
val built_in_funs :
  ((context ->
    Sparql_ms.MuSet.elt ->
    Sparql_types.expression -> Sparql_ms.VMap.key) ->
   context ->
   Sparql_ms.MuSet.elt ->
   Sparql_types.expression list -> Sparql_ms.VMap.key)
  Sparql_types.SMap.t
val get_built_in_fun :
  string ->
  (context ->
   Sparql_ms.MuSet.elt ->
   Sparql_types.expression -> Sparql_ms.VMap.key) ->
  context ->
  Sparql_ms.MuSet.elt ->
  Sparql_types.expression list -> Sparql_ms.VMap.key
val eval_var : Sparql_ms.mu -> Sparql_types.var -> Dt.value
val eval_iri : Sparql_types.iri -> Dt.value
val eval_numeric2 :
  (int -> int -> int) ->
  (float -> float -> float) -> Dt.value * Dt.value -> Dt.value
val eval_plus :
  Sparql_ms.VMap.key * Sparql_ms.VMap.key -> Dt.value
val eval_minus :
  Sparql_ms.VMap.key * Sparql_ms.VMap.key -> Dt.value
val eval_mult :
  Sparql_ms.VMap.key * Sparql_ms.VMap.key -> Dt.value
val eval_div :
  Sparql_ms.VMap.key * Sparql_ms.VMap.key -> Dt.value
val eval_equal : Dt.value * Dt.value -> Dt.value
val eval_not_equal : Dt.value * Dt.value -> Dt.value
val eval_lt : Dt.value * Dt.value -> Dt.value
val eval_lte : Dt.value * Dt.value -> Dt.value
val eval_gt : Dt.value * Dt.value -> Dt.value
val eval_gte : Dt.value * Dt.value -> Dt.value
val eval_or : Dt.value * Dt.value -> Dt.value
val eval_and : Dt.value * Dt.value -> Dt.value
val eval_bin :
  Sparql_types.binary_op ->
  Sparql_ms.VMap.key * Sparql_ms.VMap.key -> Dt.value
val eval_expr :
  context ->
  Sparql_ms.MuSet.elt ->
  Sparql_types.expression -> Sparql_ms.VMap.key
val eval_bic :
  context ->
  Sparql_ms.MuSet.elt ->
  Sparql_types.built_in_call -> Sparql_ms.VMap.key
val eval_funcall :
  context ->
  Sparql_ms.MuSet.elt ->
  Sparql_types.function_call -> Sparql_ms.VMap.key
val eval_in :
  context ->
  Sparql_ms.MuSet.elt ->
  Sparql_types.expression ->
  Sparql_types.expression list -> Sparql_ms.VMap.key
val ebv_lit : Dt.value -> Term.literal
val eval_filter :
  context -> Sparql_ms.MuSet.elt -> Sparql_types.constraint_ -> bool
val filter_omega :
  context ->
  Sparql_types.constraint_ list ->
  Sparql_ms.Multimu.t -> Sparql_ms.Multimu.t
val join_omega :
  'a ->
  Sparql_ms.Multimu.t ->
  Sparql_ms.Multimu.t -> Sparql_ms.Multimu.t
val union_omega :
  Sparql_ms.Multimu.t ->
  Sparql_ms.Multimu.t -> Sparql_ms.Multimu.t
val leftjoin_omega :
  context ->
  Sparql_ms.Multimu.t ->
  Sparql_ms.Multimu.t ->
  Sparql_types.constraint_ list -> Sparql_ms.Multimu.t
val minus_omega :
  Sparql_ms.Multimu.t ->
  Sparql_ms.Multimu.t -> Sparql_ms.Multimu.t
val extend_omega :
  context ->
  Sparql_ms.Multimu.t ->
  Sparql_types.var ->
  Sparql_types.expression -> Sparql_ms.Multimu.t
val build_sort_comp_fun :
  Sparql_types.order_condition ->
  context -> Sparql_ms.MuSet.elt -> Sparql_ms.MuSet.elt -> int
val sort_solutions : 'a -> ('a -> 'b -> 'c -> int) list -> 'b -> 'c -> int
val sort_sequence :
  context ->
  Sparql_types.order_condition list ->
  Sparql_ms.MuSet.elt list -> Sparql_ms.MuSet.elt list
val project_sequence :
  Sparql_algebra.VS.t -> Sparql_ms.mu list -> Sparql_ms.mu list
val distinct : Sparql_ms.MuSet.elt list -> Sparql_ms.MuSet.elt list
val slice : 'a list -> int option -> int option -> 'a list
val group_omega :
  context ->
  Sparql_types.group_condition list ->
  Sparql_ms.Multimu.t -> Sparql_ms.Multimu.t GExprMap.t
val agg_count :
  context ->
  bool ->
  Sparql_ms.Multimu.t ->
  Sparql_types.expression option -> Dt.value
val agg_sum :
  context ->
  bool ->
  Sparql_ms.Multimu.t ->
  Sparql_types.expression -> Sparql_ms.VMap.key
val agg_fold :
  ('a -> Sparql_ms.VMap.key -> 'a) ->
  'a ->
  context ->
  bool -> Sparql_ms.Multimu.t -> Sparql_types.expression -> 'a
val agg_min :
  context ->
  bool ->
  Sparql_ms.Multimu.t ->
  Sparql_types.expression -> Sparql_ms.VMap.key
val agg_max :
  context ->
  bool ->
  Sparql_ms.Multimu.t ->
  Sparql_types.expression -> Sparql_ms.VMap.key
val agg_avg :
  context ->
  bool ->
  Sparql_ms.Multimu.t -> Sparql_types.expression -> Dt.value
val agg_sample : 'a -> 'b -> 'c -> 'd -> 'e
val agg_group_concat :
  context ->
  bool ->
  Sparql_ms.Multimu.t ->
  Sparql_types.expression -> string option -> Dt.value
val eval_agg :
  context ->
  Sparql_types.aggregate ->
  Sparql_ms.Multimu.t -> Sparql_ms.VMap.key
val aggregation :
  context ->
  Sparql_types.aggregate ->
  Sparql_ms.Multimu.t GExprMap.t -> Sparql_ms.VMap.key GExprMap.t
val aggregate_join :
  (context -> 'a -> Sparql_ms.Multimu.t) ->
  context ->
  Sparql_types.group_condition list * 'a ->
  Sparql_algebra.algebra list -> Sparql_ms.Multimu.t
val cons : 'a -> 'a list -> 'a list
val __print_mu : Sparql_ms.mu -> unit
val __print_omega : Sparql_ms.Multimu.t -> unit
val eval_datablock : Sparql_types.datablock -> Sparql_ms.Multimu.t
val eval : context -> Sparql_algebra.algebra -> Sparql_ms.Multimu.t
val eval_list :
  context -> Sparql_algebra.algebra -> Sparql_ms.MuSet.elt list
