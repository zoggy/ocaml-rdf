(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Elements of [http://www.w3.org/ns/oa#] *)

(** [http://www.w3.org/ns/oa#] *)
val oa : Iri.t
val oa_ : string -> Iri.t

(** The class for Web Annotations. *)
val c_Annotation : Iri.t

(** A subClass of  as:OrderedCollection  that conveys to a consuming application that it should select one of the resources in the  as:items  list to use, rather than all of them.  This is typically used to provide a choice of resources to render to the user, based on further supplied properties.  If the consuming application cannot determine the user's preference, then it should use the first in the list. *)
val c_Choice : Iri.t

(** A CssSelector describes a Segment of interest in a representation that conforms to the Document Object Model through the use of the CSS selector specification. *)
val c_CssSelector : Iri.t

(** A resource which describes styles for resources participating in the Annotation using CSS. *)
val c_CssStyle : Iri.t

(** DataPositionSelector describes a range of data by recording the start and end positions of the selection in the stream. Position 0 would be immediately before the first byte, position 1 would be immediately before the second byte, and so on. The start byte is thus included in the list, but the end byte is not. *)
val c_DataPositionSelector : Iri.t

(** A class to encapsulate the different text directions that a textual resource might take.  It is not used directly in the Annotation Model, only its three instances. *)
val c_Direction : Iri.t

(** The FragmentSelector class is used to record the segment of a representation using the IRI fragment specification defined by the representation's media type. *)
val c_FragmentSelector : Iri.t

(** The HttpRequestState class is used to record the HTTP request headers that a client SHOULD use to request the correct representation from the resource.  *)
val c_HttpRequestState : Iri.t

(** The Motivation class is used to record the user's intent or motivation for the creation of the Annotation, or the inclusion of the body or target, that it is associated with. *)
val c_Motivation : Iri.t

(** A Range Selector can be used to identify the beginning and the end of the selection by using other Selectors. The selection consists of everything from the beginning of the starting selector through to the beginning of the ending selector, but not including it. *)
val c_RangeSelector : Iri.t

(** Instances of the ResourceSelection class identify part (described by an oa:Selector) of another resource (referenced with oa:hasSource), possibly from a particular representation of a resource (described by an oa:State). Please note that ResourceSelection is not used directly in the Web Annotation model, but is provided as a separate class for further application profiles to use, separate from oa:SpecificResource which has many Annotation specific features. *)
val c_ResourceSelection : Iri.t

(** A resource which describes the segment of interest in a representation of a Source resource, indicated with oa:hasSelector from the Specific Resource. This class is not used directly in the Annotation model, only its subclasses. *)
val c_Selector : Iri.t

(** Instances of the SpecificResource class identify part of another resource (referenced with oa:hasSource), a particular representation of a resource, a resource with styling hints for renders, or any combination of these, as used within an Annotation. *)
val c_SpecificResource : Iri.t

(** A State describes the intended state of a resource as applied to the particular Annotation, and thus provides the information needed to retrieve the correct representation of that resource. *)
val c_State : Iri.t

(** A Style describes the intended styling of a resource as applied to the particular Annotation, and thus provides the information to ensure that rendering is consistent across implementations. *)
val c_Style : Iri.t

(** An SvgSelector defines an area through the use of the Scalable Vector Graphics [SVG] standard. This allows the user to select a non-rectangular area of the content, such as a circle or polygon by describing the region using SVG. The SVG may be either embedded within the Annotation or referenced as an External Resource. *)
val c_SvgSelector : Iri.t

(** The TextPositionSelector describes a range of text by recording the start and end positions of the selection in the stream. Position 0 would be immediately before the first character, position 1 would be immediately before the second character, and so on. *)
val c_TextPositionSelector : Iri.t

(** The TextQuoteSelector describes a range of text by copying it, and including some of the text immediately before (a prefix) and after (a suffix) it to distinguish between multiple copies of the same sequence of characters. *)
val c_TextQuoteSelector : Iri.t

(**  *)
val c_TextualBody : Iri.t

(** A TimeState records the time at which the resource's state is appropriate for the Annotation, typically the time that the Annotation was created and/or a link to a persistent copy of the current version. *)
val c_TimeState : Iri.t

(**  An XPathSelector is used to select elements and content within a resource that supports the Document Object Model via a specified XPath value. *)
val c_XPathSelector : Iri.t

(** The object of the relationship is the end point of a service that conforms to the annotation-protocol, and it may be associated with any resource.  The expectation of asserting the relationship is that the object is the preferred service for maintaining annotations about the subject resource, according to the publisher of the relationship.

  This relationship is intended to be used both within Linked Data descriptions and as the  rel  type of a Link, via HTTP Link Headers rfc5988 for binary resources and in HTML <link> elements.  For more information about these, please see the Annotation Protocol specification annotation-protocol.
   *)
val annotationService : Iri.t

(** The object of the predicate is a plain text string to be used as the content of the body of the Annotation.  The value MUST be an  xsd:string  and that data type MUST NOT be expressed in the serialization. Note that language MUST NOT be associated with the value either as a language tag, as that is only available for  rdf:langString .
   *)
val bodyValue : Iri.t

(** A object of the relationship is a copy of the Source resource's representation, appropriate for the Annotation. *)
val cachedSource : Iri.t

(** A object of the relationship is the canonical IRI that can always be used to deduplicate the Annotation, regardless of the current IRI used to access the representation. *)
val canonical : Iri.t

(** The end property is used to convey the 0-based index of the end position of a range of content. *)
val end_ : Iri.t

(** The object of the predicate is a copy of the text which is being selected, after normalization. *)
val exact : Iri.t

(** The object of the relationship is a resource that is a body of the Annotation. *)
val hasBody : Iri.t

(** The relationship between a RangeSelector and the Selector that describes the end position of the range.  *)
val hasEndSelector : Iri.t

(** The purpose served by the resource in the Annotation. *)
val hasPurpose : Iri.t

(** The scope or context in which the resource is used within the Annotation. *)
val hasScope : Iri.t

(** The object of the relationship is a Selector that describes the segment or region of interest within the source resource.  Please note that the domain ( oa:ResourceSelection ) is not used directly in the Web Annotation model. *)
val hasSelector : Iri.t

(** The resource that the ResourceSelection, or its subclass SpecificResource, is refined from, or more specific than. Please note that the domain ( oa:ResourceSelection ) is not used directly in the Web Annotation model. *)
val hasSource : Iri.t

(** The relationship between a RangeSelector and the Selector that describes the start position of the range.  *)
val hasStartSelector : Iri.t

(** The relationship between the ResourceSelection, or its subclass SpecificResource, and a State resource. Please note that the domain ( oa:ResourceSelection ) is not used directly in the Web Annotation model. *)
val hasState : Iri.t

(** The relationship between an Annotation and its Target. *)
val hasTarget : Iri.t

(** The relationship between an Annotation and a Motivation that describes the reason for the Annotation's creation. *)
val motivatedBy : Iri.t

(** The object of the property is a snippet of content that occurs immediately before the content which is being selected by the Selector. *)
val prefix : Iri.t

(** The object of the property is the language that should be used for textual processing algorithms when dealing with the content of the resource, including hyphenation, line breaking, which font to use for rendering and so forth.  The value must follow the recommendations of BCP47. *)
val processingLanguage : Iri.t

(** The relationship between a Selector and another Selector or a State and a Selector or State that should be applied to the results of the first to refine the processing of the source resource.  *)
val refinedBy : Iri.t

(** A system that was used by the application that created the Annotation to render the resource. *)
val renderedVia : Iri.t

(** The timestamp at which the Source resource should be interpreted as being applicable to the Annotation. *)
val sourceDate : Iri.t

(** The end timestamp of the interval over which the Source resource should be interpreted as being applicable to the Annotation. *)
val sourceDateEnd : Iri.t

(** The start timestamp of the interval over which the Source resource should be interpreted as being applicable to the Annotation. *)
val sourceDateStart : Iri.t

(** The start position in a 0-based index at which a range of content is selected from the data in the source resource. *)
val start : Iri.t

(** The name of the class used in the CSS description referenced from the Annotation that should be applied to the Specific Resource. *)
val styleClass : Iri.t

(** A reference to a Stylesheet that should be used to apply styles to the Annotation rendering. *)
val styledBy : Iri.t

(** The snippet of text that occurs immediately after the text which is being selected. *)
val suffix : Iri.t

(** The direction of the text of the subject resource. There MUST only be one text direction associated with any given resource. *)
val textDirection : Iri.t

(** A object of the relationship is a resource from which the source resource was retrieved by the providing system. *)
val via : Iri.t


module Open : sig
  (** The class for Web Annotations. *)
  val oa_c_Annotation : Iri.t

  (** A subClass of  as:OrderedCollection  that conveys to a consuming application that it should select one of the resources in the  as:items  list to use, rather than all of them.  This is typically used to provide a choice of resources to render to the user, based on further supplied properties.  If the consuming application cannot determine the user's preference, then it should use the first in the list. *)
  val oa_c_Choice : Iri.t

  (** A CssSelector describes a Segment of interest in a representation that conforms to the Document Object Model through the use of the CSS selector specification. *)
  val oa_c_CssSelector : Iri.t

  (** A resource which describes styles for resources participating in the Annotation using CSS. *)
  val oa_c_CssStyle : Iri.t

  (** DataPositionSelector describes a range of data by recording the start and end positions of the selection in the stream. Position 0 would be immediately before the first byte, position 1 would be immediately before the second byte, and so on. The start byte is thus included in the list, but the end byte is not. *)
  val oa_c_DataPositionSelector : Iri.t

  (** A class to encapsulate the different text directions that a textual resource might take.  It is not used directly in the Annotation Model, only its three instances. *)
  val oa_c_Direction : Iri.t

  (** The FragmentSelector class is used to record the segment of a representation using the IRI fragment specification defined by the representation's media type. *)
  val oa_c_FragmentSelector : Iri.t

  (** The HttpRequestState class is used to record the HTTP request headers that a client SHOULD use to request the correct representation from the resource.  *)
  val oa_c_HttpRequestState : Iri.t

  (** The Motivation class is used to record the user's intent or motivation for the creation of the Annotation, or the inclusion of the body or target, that it is associated with. *)
  val oa_c_Motivation : Iri.t

  (** A Range Selector can be used to identify the beginning and the end of the selection by using other Selectors. The selection consists of everything from the beginning of the starting selector through to the beginning of the ending selector, but not including it. *)
  val oa_c_RangeSelector : Iri.t

  (** Instances of the ResourceSelection class identify part (described by an oa:Selector) of another resource (referenced with oa:hasSource), possibly from a particular representation of a resource (described by an oa:State). Please note that ResourceSelection is not used directly in the Web Annotation model, but is provided as a separate class for further application profiles to use, separate from oa:SpecificResource which has many Annotation specific features. *)
  val oa_c_ResourceSelection : Iri.t

  (** A resource which describes the segment of interest in a representation of a Source resource, indicated with oa:hasSelector from the Specific Resource. This class is not used directly in the Annotation model, only its subclasses. *)
  val oa_c_Selector : Iri.t

  (** Instances of the SpecificResource class identify part of another resource (referenced with oa:hasSource), a particular representation of a resource, a resource with styling hints for renders, or any combination of these, as used within an Annotation. *)
  val oa_c_SpecificResource : Iri.t

  (** A State describes the intended state of a resource as applied to the particular Annotation, and thus provides the information needed to retrieve the correct representation of that resource. *)
  val oa_c_State : Iri.t

  (** A Style describes the intended styling of a resource as applied to the particular Annotation, and thus provides the information to ensure that rendering is consistent across implementations. *)
  val oa_c_Style : Iri.t

  (** An SvgSelector defines an area through the use of the Scalable Vector Graphics [SVG] standard. This allows the user to select a non-rectangular area of the content, such as a circle or polygon by describing the region using SVG. The SVG may be either embedded within the Annotation or referenced as an External Resource. *)
  val oa_c_SvgSelector : Iri.t

  (** The TextPositionSelector describes a range of text by recording the start and end positions of the selection in the stream. Position 0 would be immediately before the first character, position 1 would be immediately before the second character, and so on. *)
  val oa_c_TextPositionSelector : Iri.t

  (** The TextQuoteSelector describes a range of text by copying it, and including some of the text immediately before (a prefix) and after (a suffix) it to distinguish between multiple copies of the same sequence of characters. *)
  val oa_c_TextQuoteSelector : Iri.t

  (**  *)
  val oa_c_TextualBody : Iri.t

  (** A TimeState records the time at which the resource's state is appropriate for the Annotation, typically the time that the Annotation was created and/or a link to a persistent copy of the current version. *)
  val oa_c_TimeState : Iri.t

  (**  An XPathSelector is used to select elements and content within a resource that supports the Document Object Model via a specified XPath value. *)
  val oa_c_XPathSelector : Iri.t

  (** The object of the relationship is the end point of a service that conforms to the annotation-protocol, and it may be associated with any resource.  The expectation of asserting the relationship is that the object is the preferred service for maintaining annotations about the subject resource, according to the publisher of the relationship.

  This relationship is intended to be used both within Linked Data descriptions and as the  rel  type of a Link, via HTTP Link Headers rfc5988 for binary resources and in HTML <link> elements.  For more information about these, please see the Annotation Protocol specification annotation-protocol.
   *)
  val oa_annotationService : Iri.t

  (** The object of the predicate is a plain text string to be used as the content of the body of the Annotation.  The value MUST be an  xsd:string  and that data type MUST NOT be expressed in the serialization. Note that language MUST NOT be associated with the value either as a language tag, as that is only available for  rdf:langString .
   *)
  val oa_bodyValue : Iri.t

  (** A object of the relationship is a copy of the Source resource's representation, appropriate for the Annotation. *)
  val oa_cachedSource : Iri.t

  (** A object of the relationship is the canonical IRI that can always be used to deduplicate the Annotation, regardless of the current IRI used to access the representation. *)
  val oa_canonical : Iri.t

  (** The end property is used to convey the 0-based index of the end position of a range of content. *)
  val oa_end : Iri.t

  (** The object of the predicate is a copy of the text which is being selected, after normalization. *)
  val oa_exact : Iri.t

  (** The object of the relationship is a resource that is a body of the Annotation. *)
  val oa_hasBody : Iri.t

  (** The relationship between a RangeSelector and the Selector that describes the end position of the range.  *)
  val oa_hasEndSelector : Iri.t

  (** The purpose served by the resource in the Annotation. *)
  val oa_hasPurpose : Iri.t

  (** The scope or context in which the resource is used within the Annotation. *)
  val oa_hasScope : Iri.t

  (** The object of the relationship is a Selector that describes the segment or region of interest within the source resource.  Please note that the domain ( oa:ResourceSelection ) is not used directly in the Web Annotation model. *)
  val oa_hasSelector : Iri.t

  (** The resource that the ResourceSelection, or its subclass SpecificResource, is refined from, or more specific than. Please note that the domain ( oa:ResourceSelection ) is not used directly in the Web Annotation model. *)
  val oa_hasSource : Iri.t

  (** The relationship between a RangeSelector and the Selector that describes the start position of the range.  *)
  val oa_hasStartSelector : Iri.t

  (** The relationship between the ResourceSelection, or its subclass SpecificResource, and a State resource. Please note that the domain ( oa:ResourceSelection ) is not used directly in the Web Annotation model. *)
  val oa_hasState : Iri.t

  (** The relationship between an Annotation and its Target. *)
  val oa_hasTarget : Iri.t

  (** The relationship between an Annotation and a Motivation that describes the reason for the Annotation's creation. *)
  val oa_motivatedBy : Iri.t

  (** The object of the property is a snippet of content that occurs immediately before the content which is being selected by the Selector. *)
  val oa_prefix : Iri.t

  (** The object of the property is the language that should be used for textual processing algorithms when dealing with the content of the resource, including hyphenation, line breaking, which font to use for rendering and so forth.  The value must follow the recommendations of BCP47. *)
  val oa_processingLanguage : Iri.t

  (** The relationship between a Selector and another Selector or a State and a Selector or State that should be applied to the results of the first to refine the processing of the source resource.  *)
  val oa_refinedBy : Iri.t

  (** A system that was used by the application that created the Annotation to render the resource. *)
  val oa_renderedVia : Iri.t

  (** The timestamp at which the Source resource should be interpreted as being applicable to the Annotation. *)
  val oa_sourceDate : Iri.t

  (** The end timestamp of the interval over which the Source resource should be interpreted as being applicable to the Annotation. *)
  val oa_sourceDateEnd : Iri.t

  (** The start timestamp of the interval over which the Source resource should be interpreted as being applicable to the Annotation. *)
  val oa_sourceDateStart : Iri.t

  (** The start position in a 0-based index at which a range of content is selected from the data in the source resource. *)
  val oa_start : Iri.t

  (** The name of the class used in the CSS description referenced from the Annotation that should be applied to the Specific Resource. *)
  val oa_styleClass : Iri.t

  (** A reference to a Stylesheet that should be used to apply styles to the Annotation rendering. *)
  val oa_styledBy : Iri.t

  (** The snippet of text that occurs immediately after the text which is being selected. *)
  val oa_suffix : Iri.t

  (** The direction of the text of the subject resource. There MUST only be one text direction associated with any given resource. *)
  val oa_textDirection : Iri.t

  (** A object of the relationship is a resource from which the source resource was retrieved by the providing system. *)
  val oa_via : Iri.t

end

class from : ?sub: Term.term -> Graph.graph ->
  object
    method annotationService : Term.term list
    method annotationService_opt : Term.term option
    method annotationService_iris : Iri.t list
    method annotationService_opt_iri : Iri.t option
    method bodyValue : Term.literal list
    method bodyValue_opt : Term.literal option
    method cachedSource : Term.term list
    method cachedSource_opt : Term.term option
    method cachedSource_iris : Iri.t list
    method cachedSource_opt_iri : Iri.t option
    method canonical : Term.term list
    method canonical_opt : Term.term option
    method canonical_iris : Iri.t list
    method canonical_opt_iri : Iri.t option
    method end_ : Term.term list
    method end__opt : Term.term option
    method end__iris : Iri.t list
    method end__opt_iri : Iri.t option
    method exact : Term.literal list
    method exact_opt : Term.literal option
    method hasBody : Term.term list
    method hasBody_opt : Term.term option
    method hasBody_iris : Iri.t list
    method hasBody_opt_iri : Iri.t option
    method hasEndSelector : Term.term list
    method hasEndSelector_opt : Term.term option
    method hasEndSelector_iris : Iri.t list
    method hasEndSelector_opt_iri : Iri.t option
    method hasPurpose : Term.term list
    method hasPurpose_opt : Term.term option
    method hasPurpose_iris : Iri.t list
    method hasPurpose_opt_iri : Iri.t option
    method hasScope : Term.term list
    method hasScope_opt : Term.term option
    method hasScope_iris : Iri.t list
    method hasScope_opt_iri : Iri.t option
    method hasSelector : Term.term list
    method hasSelector_opt : Term.term option
    method hasSelector_iris : Iri.t list
    method hasSelector_opt_iri : Iri.t option
    method hasSource : Term.term list
    method hasSource_opt : Term.term option
    method hasSource_iris : Iri.t list
    method hasSource_opt_iri : Iri.t option
    method hasStartSelector : Term.term list
    method hasStartSelector_opt : Term.term option
    method hasStartSelector_iris : Iri.t list
    method hasStartSelector_opt_iri : Iri.t option
    method hasState : Term.term list
    method hasState_opt : Term.term option
    method hasState_iris : Iri.t list
    method hasState_opt_iri : Iri.t option
    method hasTarget : Term.term list
    method hasTarget_opt : Term.term option
    method hasTarget_iris : Iri.t list
    method hasTarget_opt_iri : Iri.t option
    method motivatedBy : Term.term list
    method motivatedBy_opt : Term.term option
    method motivatedBy_iris : Iri.t list
    method motivatedBy_opt_iri : Iri.t option
    method prefix : Term.literal list
    method prefix_opt : Term.literal option
    method processingLanguage : Term.literal list
    method processingLanguage_opt : Term.literal option
    method refinedBy : Term.term list
    method refinedBy_opt : Term.term option
    method refinedBy_iris : Iri.t list
    method refinedBy_opt_iri : Iri.t option
    method renderedVia : Term.term list
    method renderedVia_opt : Term.term option
    method renderedVia_iris : Iri.t list
    method renderedVia_opt_iri : Iri.t option
    method sourceDate : Term.term list
    method sourceDate_opt : Term.term option
    method sourceDate_iris : Iri.t list
    method sourceDate_opt_iri : Iri.t option
    method sourceDateEnd : Term.term list
    method sourceDateEnd_opt : Term.term option
    method sourceDateEnd_iris : Iri.t list
    method sourceDateEnd_opt_iri : Iri.t option
    method sourceDateStart : Term.term list
    method sourceDateStart_opt : Term.term option
    method sourceDateStart_iris : Iri.t list
    method sourceDateStart_opt_iri : Iri.t option
    method start : Term.term list
    method start_opt : Term.term option
    method start_iris : Iri.t list
    method start_opt_iri : Iri.t option
    method styleClass : Term.literal list
    method styleClass_opt : Term.literal option
    method styledBy : Term.term list
    method styledBy_opt : Term.term option
    method styledBy_iris : Iri.t list
    method styledBy_opt_iri : Iri.t option
    method suffix : Term.literal list
    method suffix_opt : Term.literal option
    method textDirection : Term.term list
    method textDirection_opt : Term.term option
    method textDirection_iris : Iri.t list
    method textDirection_opt_iri : Iri.t option
    method via : Term.term list
    method via_opt : Term.term option
    method via_iris : Iri.t list
    method via_opt_iri : Iri.t option
  end
