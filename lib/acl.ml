(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

let acl_str = "http://www.w3.org/ns/auth/acl#";;
let acl = Iri.of_string acl_str ;;
let acl_ s = Iri.of_string (acl_str ^ s);;

let c_Access = acl_ "Access" ;;
let c_Append = acl_ "Append" ;;
let c_Authorization = acl_ "Authorization" ;;
let c_Control = acl_ "Control" ;;
let c_Read = acl_ "Read" ;;
let c_Write = acl_ "Write" ;;
let accessControl = acl_ "accessControl" ;;
let accessTo = acl_ "accessTo" ;;
let accessToClass = acl_ "accessToClass" ;;
let agent = acl_ "agent" ;;
let agentClass = acl_ "agentClass" ;;
let agentGroup = acl_ "agentGroup" ;;
let defaultForNew = acl_ "defaultForNew" ;;
let delegates = acl_ "delegates" ;;
let mode = acl_ "mode" ;;
let owner = acl_ "owner" ;;

module Open = struct
  let acl_c_Access = c_Access
  let acl_c_Append = c_Append
  let acl_c_Authorization = c_Authorization
  let acl_c_Control = c_Control
  let acl_c_Read = c_Read
  let acl_c_Write = c_Write
  let acl_accessControl = accessControl
  let acl_accessTo = accessTo
  let acl_accessToClass = accessToClass
  let acl_agent = agent
  let acl_agentClass = agentClass
  let acl_agentGroup = agentGroup
  let acl_defaultForNew = defaultForNew
  let acl_delegates = delegates
  let acl_mode = mode
  let acl_owner = owner
end

class from ?sub g =
  let sub = match sub with None -> Term.Iri (g.Graph.name()) | Some t -> t in
  object(self)
  method accessControl = g.Graph.objects_of ~sub ~pred: accessControl
  method accessControl_opt = match self#accessControl with [] -> None | x::_ -> Some x
  method accessControl_iris = Graph.only_iris (self#accessControl)
  method accessControl_opt_iri = match self#accessControl_iris with [] -> None | x::_ -> Some x
  method accessTo = g.Graph.objects_of ~sub ~pred: accessTo
  method accessTo_opt = match self#accessTo with [] -> None | x::_ -> Some x
  method accessTo_iris = Graph.only_iris (self#accessTo)
  method accessTo_opt_iri = match self#accessTo_iris with [] -> None | x::_ -> Some x
  method accessToClass = g.Graph.objects_of ~sub ~pred: accessToClass
  method accessToClass_opt = match self#accessToClass with [] -> None | x::_ -> Some x
  method accessToClass_iris = Graph.only_iris (self#accessToClass)
  method accessToClass_opt_iri = match self#accessToClass_iris with [] -> None | x::_ -> Some x
  method agent = g.Graph.objects_of ~sub ~pred: agent
  method agent_opt = match self#agent with [] -> None | x::_ -> Some x
  method agent_iris = Graph.only_iris (self#agent)
  method agent_opt_iri = match self#agent_iris with [] -> None | x::_ -> Some x
  method agentClass = g.Graph.objects_of ~sub ~pred: agentClass
  method agentClass_opt = match self#agentClass with [] -> None | x::_ -> Some x
  method agentClass_iris = Graph.only_iris (self#agentClass)
  method agentClass_opt_iri = match self#agentClass_iris with [] -> None | x::_ -> Some x
  method agentGroup = g.Graph.objects_of ~sub ~pred: agentGroup
  method agentGroup_opt = match self#agentGroup with [] -> None | x::_ -> Some x
  method agentGroup_iris = Graph.only_iris (self#agentGroup)
  method agentGroup_opt_iri = match self#agentGroup_iris with [] -> None | x::_ -> Some x
  method defaultForNew = g.Graph.objects_of ~sub ~pred: defaultForNew
  method defaultForNew_opt = match self#defaultForNew with [] -> None | x::_ -> Some x
  method defaultForNew_iris = Graph.only_iris (self#defaultForNew)
  method defaultForNew_opt_iri = match self#defaultForNew_iris with [] -> None | x::_ -> Some x
  method delegates = g.Graph.objects_of ~sub ~pred: delegates
  method delegates_opt = match self#delegates with [] -> None | x::_ -> Some x
  method delegates_iris = Graph.only_iris (self#delegates)
  method delegates_opt_iri = match self#delegates_iris with [] -> None | x::_ -> Some x
  method mode = g.Graph.objects_of ~sub ~pred: mode
  method mode_opt = match self#mode with [] -> None | x::_ -> Some x
  method mode_iris = Graph.only_iris (self#mode)
  method mode_opt_iri = match self#mode_iris with [] -> None | x::_ -> Some x
  method owner = g.Graph.objects_of ~sub ~pred: owner
  method owner_opt = match self#owner with [] -> None | x::_ -> Some x
  method owner_iris = Graph.only_iris (self#owner)
  method owner_opt_iri = match self#owner_iris with [] -> None | x::_ -> Some x
  end
