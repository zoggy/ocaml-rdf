<page title="Using graphs"
navbar-doc="active"
with-contents="true"
>
<contents>
<prepare-toc>
<toc/>
<section id="creategraph" title="Creating a graph">
<p>
An RDF graph needs a base IRI, which can be used to name the graph.
Then we use the <refdoc mod="Graph" id="val-open_graph" name="Rdf.Graph.open_graph"/>
function to create an empty graph:
</p>
<ocaml-eval>
<![CDATA[open Rdf;;
let base_iri = Iri.of_string "http://foo.bar.net";;
let g = Graph.open_graph base_iri ;;]]>
</ocaml-eval>
<p>
If no option is provided, the graph is created in memory.
You can <doc href="doc-databases">create graphs stored in a MySQL or Postgresql
datatabase</doc>.
</p>
<p>
Once the graph is created, let's insert triples in the graph,
to associate the name "Alice" and the mailbox address IRI
"mailto:alice@mbox.net" to a new blank term:
</p>
<ocaml-eval toplevel="true">
let foaf_name = Iri.of_string "http://xmlns.com/foaf/0.1/name";;
let foaf_mbox = Iri.of_string "http://xmlns.com/foaf/0.1/mbox";;

let a = Term.Blank_ (g.Graph.new_blank_id());;
g.Graph.add_triple
   ~sub: a ~pred: foaf_name
   ~obj: (Term.term_of_literal_string "Alice");;

g.Graph.add_triple
   ~sub: a ~pred: foaf_mbox
   ~obj: (Term.Iri (Iri.of_string "mailto:alice@mbox.net"));;

let b = Term.Blank_ (g.Graph.new_blank_id());;
g.Graph.add_triple
   ~sub: b ~pred: foaf_name
   ~obj: (Term.term_of_literal_string "Bob");;

g.Graph.add_triple
   ~sub: b ~pred: foaf_mbox
   ~obj: (Term.Iri (Iri.of_string "mailto:bob@mbox.net"));;
</ocaml-eval>
<p>Here is the content of our graph, in Turtle format:</p>
<showttl graph="g"/>
</section>
<section id="querying" title="Querying the graph">
<p>
Let's use some of the <reftyp mod="Graph" name="graph"/> structure fields to
retrieve information from the graph.
</p>
<p>Here we look for the mailboxes of Alice, by querying the objects of
triples having a specified subject and object:
</p>
<ocaml-eval toplevel="true">
let alice_mbox = g.Graph.objects_of
  ~sub: a ~pred: foaf_mbox;;
List.iter (fun n -> print_endline (Term.string_of_term n)) alice_mbox;;
</ocaml-eval>
<p>
Let's now query all triples having the predicate <code>foaf_name</code>,
and use the <refval mod="Ttl" name="string_of_triple"/> function
to display the result:
</p>
<ocaml-eval toplevel="true">
let triples = g.Graph.find ~pred: foaf_name ();;
List.iter
   (fun (sub,pred,obj) ->
     print_endline (Ttl.string_of_triple ~sub ~pred ~obj))
   triples;;
</ocaml-eval>
<p>These are simple ways to query the graph.
<page href="doc-sparql">Sparql queries can be used</page> to execute
more powerful and more complex queries.
</p>
</section>

<section id="import" title="Importing and exporting">
<p>
The <ext-a href="http://www.w3.org/TR/rdf-syntax-grammar">RDF/XML</ext-a> and
<ext-a href="http://www.w3.org/TeamSubmission/turtle/">Turtle</ext-a> formats
are supported to import and export triples.
The corresponding functions are provided in modules
<refmod mod="Xml"/> and <refmod mod="Ttl"/>.
</p>
<p>Here is an example of code creating a graph, importing from a
RDF/XML file <code>example.rdf</code> and exporting to a Turtle file
<code>example.ttl</code>:
</p>
<ocaml-eval toplevel="true">
let base = Iri.of_string "http://hello.fr" ;;
let g = Graph.open_graph base ;;
Xml.from_file g "example.rdf";;
Ttl.to_file g "example.ttl";;
</ocaml-eval>
</section>

<section id="transactions" title="Transactions">
<p>
Transactions can be used if the backend support them.
The storage in memory supports nested transactions, and database
backends support one level of transaction.
</p>
<p>Transactions are handled with the
<code>transaction_start</code>, <code>transaction_commit</code>
and <code>transaction_rollback</code> of the the
<reftyp mod="Graph" name="graph"/> structure.
</p>
<p>As an example, let's load data from a file:</p>
<ocaml-eval>
let base = Iri.of_string "http://hello.fr" ;;
let g = Graph.open_graph base ;;
Ttl.from_file g "example.ttl";;
</ocaml-eval>
<p>This is our loaded data:</p>
<showttl graph="g"/>
<p>Now let's start a transaction, and modify the graph:</p>
<ocaml-eval>
g.Graph.transaction_start();;
let pred = Iri.of_string "http://example.org/stuff/1.0/fullName" ;;
match g.Graph.find ~pred () with
| (sub,_,obj) :: _ ->
  (* remove a triple *)
  g.Graph.rem_triple ~sub ~pred ~obj;
  (* add a new triple, with a different name *)
  g.Graph.add_triple ~sub ~pred
    ~obj: (Term.term_of_literal_string "Jean Dupont")
| _ -> assert false;;
</ocaml-eval>
<p>Now the graph looks like:</p>
<showttl graph="g"/>
<p>If we rollback, we get the previous graph:</p>
<ocaml-eval>g.Graph.transaction_rollback();;</ocaml-eval>
<showttl graph="g"/>
</section>

<section id="dot" title="Dot output">
<p>
RDF graphs can be dumped in Graphviz (dot) format, with the
<refmod mod="Dot"/> module. The following code
generates the dot code, then creates a .svg file by running
a dot command. The result is inserted below.
</p>
<ocaml-eval><![CDATA[
let dot_file = "example.dot";;
let svg_file = (Filename.chop_extension dot_file)^".svg";;
let dot_code = Dot.dot_of_graph g;;
let () =
  let oc = open_out dot_file in
  output_string oc dot_code;
  close_out oc;;
let com = Printf.sprintf "dot -Grankdir=TB -Tsvg -o %s %s"
  (Filename.quote svg_file) (Filename.quote dot_file);;
if Sys.command com <> 0 then failwith "Exec error";;
]]></ocaml-eval>
<img src="example.svg"/>
</section>

<section id="namespaces" title="Namespaces">
<p>
Each graph has associated namespaces, i.e. pairs or (prefix, IRI), used
to shorten the (XML, Turtle, ...) outputs.
</p>
<p>
When loading a graph from a file, the namespaces used in the file
are automatically added to the graph, so that outputting the graph
will make use of the same namespaces.
</p>
<p>
Namespaces of a graph can also be set programmatically, using the
functions provided in each graph:
</p>
<ocaml-eval toplevel="true">
List.map
  (fun (iri, name) -> (Iri.to_string iri, name))
  (g.Graph.namespaces());;
</ocaml-eval>
<p>See <reftyp mod="Graph" name="graph"/> for other functions.</p>
</section>
</prepare-toc>
</contents>
</page>
