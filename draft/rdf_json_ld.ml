(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** *)

module SMap = Map.Make(String)
module SSet = Set.Make(String)

type loc = int * int (* 1-based line * 0-based column *)
type range = loc * loc
let string_of_range ((l1,c1), (l2,c2)) =
  if l1 = l2
  then Printf.sprintf "line %d, characters %d-%d" l1 c1 c2
  else Printf.sprintf "line %d, character %d to line %d, character %d" l1 c1 l2 c2

type 'a ranged = { loc : range option ; data : 'a }
let ranged ?loc data = { loc ; data }

type error =
| Bad_iri of string * string (* iri as string, message *)
| Iri_get of Iri.t * string (* iri, message *)
| Invalid_json of string
| Invalid_subject of Rdf_term.term
| Invalid_type of Rdf_term.term

exception Error of error
let string_of_error = function
| Bad_iri (iri, s) -> Printf.sprintf "Invalid IRI %s: %s" iri s
| Iri_get (iri, s) ->
    Printf.sprintf "Could not retrieve %s: %s" (Iri.to_string iri) s
| Invalid_json s  ->
    Printf.sprintf "Invalid json: %s" s
| Invalid_subject t ->
    Printf.sprintf "Invalid triple subject: %s" (Rdf_term.string_of_term t)
| Invalid_type t ->
    Printf.sprintf "Invalid type: %s" (Rdf_term.string_of_term t)

let () = Printexc.register_printer
  (function Error e -> Some (string_of_error e) | _ -> None)
let error e = raise (Error e)

module Json =
  struct
    type key = string ranged
    type json_t = [
      | `Obj of (key * json) list
      | `List of json list
      | `String of string
      | `Bool of bool
      | `Float of float
      | `Null
      ]
    and json = json_t ranged
    let json ?loc data = { loc ; data }

    exception Escape of ((int * int) * (int * int)) * Jsonm.error
    let string_of_error range err =
      let b = Buffer.create 256 in
      Buffer.add_string b (string_of_range range) ;
      let fmt = Format.formatter_of_buffer b in
      Jsonm.pp_error fmt err;
      Format.pp_print_flush fmt ();
      Buffer.contents b

    let dec d = match Jsonm.decode d with
      | `Lexeme l -> l
      | `Error e -> raise (Escape (Jsonm.decoded_range d, e))
      | `End | `Await -> assert false

    let loc_start d = fst (Jsonm.decoded_range d)
    let loc_end d = snd (Jsonm.decoded_range d)
    let range d = Jsonm.decoded_range d

    let rec value v k d =
      match v with
      | `Os -> obj (loc_start d) [] k d
      | `As -> arr (loc_start d) [] k d
      | `Null | `Bool _ | `String _ | `Float _ as v ->
          let v = json ~loc: (range d) v in
          k v d
      | _ -> assert false
    and arr loc_start vs k d =
      match dec d with
      | `Ae ->
         let loc = (loc_start, loc_end d) in
         let v = json ~loc (`List (List.rev vs)) in
         k v d
      | v -> value v (fun v -> arr loc_start (v :: vs) k) d
    and obj loc_start ms k d =
      match dec d with
      | `Oe ->
         let loc = (loc_start, loc_end d) in
         let v = json ~loc (`Obj (List.rev ms)) in
         k v d
      | `Name n ->
         let key = ranged n in
         value (dec d) (fun v -> obj loc_start ((key, v) :: ms) k) d
      | _ -> assert false

    let json_of_src ?encoding
      (src : [`Channel of in_channel | `String of string])
      =
      let d = Jsonm.decoder ?encoding src in
      try Ok (value (dec d) (fun v _ -> v) d) with
      | Escape (r, e) -> Error (r, e)

    let enc e l = ignore (Jsonm.encode e (`Lexeme l))
    let rec value v k e = match v with
      | `List vs -> arr vs k e
      | `Obj ms -> obj ms k e
      | `Null | `Bool _ | `Float _ | `String _ as v -> enc e v; k e
    and arr vs k e = enc e `As; arr_vs vs k e
    and arr_vs vs k e = match vs with
      | v :: vs' -> value v.data (arr_vs vs' k) e
      | [] -> enc e `Ae; k e
    and obj ms k e = enc e `Os; obj_ms ms k e
    and obj_ms ms k e = match ms with
      | (key, v) :: ms -> enc e (`Name key.data); value v.data (obj_ms ms k) e
      | [] -> enc e `Oe; k e

    let json_to_dst ~minify
      (dst : [`Channel of out_channel | `Buffer of Buffer.t ])
        (json : json)
        =
      let e = Jsonm.encoder ~minify dst in
      let finish e = ignore (Jsonm.encode e `End) in
      match json.data with `List _ | `Obj _ as json -> value json finish e
      | _ -> invalid_arg "invalid json text"

    let from_string ?encoding s = json_of_src ?encoding (`String s)
    let from_string_exn ?encoding s =
      match from_string ?encoding s with
      | Ok x -> x
      | Error (range, e) -> error (Invalid_json (string_of_error range e))

    let to_string ?(minify=false) json =
      let b = Buffer.create 256 in
      json_to_dst ~minify (`Buffer b) json;
      Buffer.contents b

    let get =
      let rec find k = function
      | [] -> None
      | ({ data }, v) :: _ when data = k -> Some v
      | _ :: q -> find k q
      in
      fun fields k -> find k (List.rev fields)

    let (-->>) = get
    let (-->) fields name = Option.map (fun { data } -> data) (fields-->>name)
  end

module type IO =
  sig
    val get : Iri.t -> (string, string) result Lwt.t (* json string, error message *)
  end

type direction = [`Ltr | `Rtl]
type typ = [`Json | `Id | `Vocab | `Iri of Iri.t | `None | `Null]
type ctx_def =
  { id : Iri.t ;
    protected : bool ;
    ctx : ctx option ;
    typ: typ option ;
    language: [`Str of string | `Null] option ;
    container: string option ; (* for example "@language" *)
    direction: direction option ;
    (* TODO add and handle "reverse" field *)
  }

and ctx =
  { base: [`Iri of Iri.t | `Null] ;
    map : [ `Null | `Nest | `Def of ctx_def] SMap.t ;
    vocab : Iri.t option ;
    version : string option ;
    propagate : bool ;
    protected : bool ;
    language : string option ;
    direction : direction option ;
    type_container_set : bool option
      (** None=no @type info; Some b: @type with container=@set and @protected=b *) ;
  }

type value_def =
    { value : Json.json ;
      typ : typ option ;
      ctx : ctx option ;
      language : [`Str of string | `Null] option ;
      direction: direction option ;
      index : string option ;
    }
let mk_value_def ?typ ?ctx ?language ?direction ?index value =
  { value ; typ ; ctx ; language ; direction ; index }

let json_of_ctx =
  let rec fm s d acc =
    match d with
    | `Null -> (ranged s, ranged `Null) :: acc
    | `Def d ->
        let id = (ranged "@id", ranged (`String (Iri.to_string d.id))) in
        let protected = (ranged "@protected", ranged (`Bool d.protected)) in
        let fields = match d.ctx with
          | None -> []
          | Some ctx -> [(ranged "@context", f ctx)]
        in
        let fields = match d.typ with
          | None -> fields
          | Some t ->
              let j = match t with
                | `Id -> `String "@id"
                | `Json -> `String "@json"
                | `Vocab -> `String "@vocab"
                | `None -> `String "@none"
                | `Null -> `Null
                | `Iri i -> `String (Iri.to_string i)
              in
              (ranged "@type", ranged j) :: fields
        in
        let fields = match d.direction with
          | None -> fields
          | Some d ->
              let s = match d with
                | `Ltr -> "ltr"
                | `Rtl -> "rtl"
              in
              (ranged "@direction", ranged (`String s)) :: fields
        in
        let fields = match d.language with
          | None -> fields
          | Some x ->
              let j = match x with
              | `Null -> `Null
              | `Str s -> `String s
              in
              (ranged "@language", ranged j) :: fields
        in
        let fields = match d.container with
          | None -> fields
          | Some s -> (ranged "@container", ranged (`String s)) :: fields
        in
        let fields = id :: protected :: fields in
        (ranged s, ranged (`Obj fields)) :: acc

  and f ctx =
    let base =
      let j = match ctx.base with
       | `Null -> `Null
       | `Iri i -> `String (Iri.to_string i)
      in
      (ranged "@base", ranged j)
    in
    let propagate = (ranged "@propagate", ranged (`Bool ctx.propagate)) in
    let protected = (ranged "@protected", ranged (`Bool ctx.protected)) in
    let fields = match ctx.version with
      | None -> []
      | Some v -> [(ranged "@version", ranged (`String v))]
    in
    let fields = match ctx.direction with
          | None -> fields
          | Some d ->
              let s = match d with
                | `Ltr -> "ltr"
                | `Rtl -> "rtl"
              in
              (ranged "@direction", ranged (`String s)) :: fields
    in
    let fields = match ctx.language with
      | None -> fields
      | Some s -> (ranged "@language", ranged (`String s)) :: fields
    in
    let fields = match ctx.type_container_set with
      | None -> fields
      | Some b ->
          let j = `Obj [
              (ranged "@container", ranged (`String "@set")) ;
              (ranged "@protected", ranged (`Bool b)) ]
          in
          (ranged "@type", ranged j) :: fields
    in
    let fields = match ctx.vocab with
      | None -> fields
      | Some i -> (ranged "vocab", ranged (`String (Iri.to_string i))) :: fields
    in
    let fields = base :: propagate :: protected :: fields in
    ranged (`Obj (List.rev (SMap.fold fm ctx.map fields)))
  in
  f

let string_of_ctx ctx =
  let json = json_of_ctx ctx in
  Json.to_string json

let mk_def ?(protected=false) ?ctx ?typ ?language ?container ?direction id =
  { id ; protected ; ctx ; typ ; language ; container ; direction }

let null_ctx =
  { base = `Null ;
    map = SMap.empty ;
    vocab = None ;
    version = None ;
    propagate = true ;
    protected = false ;
    language = None ;
    direction = None ;
    type_container_set = None ;
  }

let empty_ctx base = { null_ctx with base = `Iri base }

module Make(IO:IO) =
  struct
    open Json
    let as_compact_iri s =
      match String.index_from_opt s 0 ':' with
      | None -> None
      | Some p ->
          let len = String.length s in
          let suffix = if len > p then String.sub s (p+1) (len - p - 1) else "" in
          if String.length suffix >= 2 && suffix.[0] = '/' && suffix.[1] = '/' then
            None
          else
            Some (String.sub s 0 p, suffix)

    let resolve_iri ctx iri =
      match ctx.base with
      | `Null -> iri
      | `Iri base -> Iri.resolve ~base iri

    let iri_append i s = Iri.of_string ((Iri.to_string i)^s)

    let iri_of_string ?ctx s =
      try
        match ctx with
        | None -> Iri.of_string s
        | Some ctx ->
            match as_compact_iri s with
            | None ->
                let i = Iri.of_string s in
                resolve_iri ctx i
            | Some (prefix, suffix) ->
                match SMap.find_opt prefix ctx.map with
                | None ->
                    let i = Iri.of_string s in
                    resolve_iri ctx i
                | Some (`Def def) -> iri_append def.id suffix
                | Some `Null -> Iri.of_string s
      with Iri.Error e -> error (Bad_iri (s, Iri.string_of_error e))

    let map_to_rdfterm ctx ?typ s =
      try
        match as_compact_iri s with
        | None ->
            let i = Iri.of_string s in
            if Iri.is_relative i then
              match SMap.find_opt s ctx.map with
              | None ->
                  (match typ, ctx.vocab with
                   | None, None -> None
                   | None, Some vi ->
                       (* no type, we are expanding a key, and there is a vocab *)
                       Some (Rdf_term.Iri (iri_append vi s))
                   | Some `Vocab, Some vi ->
                       (* type must be append to vocab, and there is a vocab *)
                       Some (Rdf_term.Iri (iri_append vi s))
                   | Some `Vocab, None ->
                       (* type must be append to voca, but no vocab, we expand from base *)
                       Some (Rdf_term.Iri (resolve_iri ctx i))
                   | Some `Id, _ ->
                     (* expand from base *)
                       Some (Rdf_term.Iri (resolve_iri ctx i))
                   | Some `Json, _ ->
                       Some (Rdf_term.term_of_literal_string
                        ~typ:Rdf_rdf.json s)
                   | Some (`Iri typ), _ ->
                       Some (Rdf_term.term_of_literal_string ~typ s)
                   | Some (`None|`Null), _ ->
                       Some (Rdf_term.term_of_literal_string s)
                  )
              | Some `Null -> None
              | Some `Nest -> None
              | Some (`Def d) -> Some (Rdf_term.Iri d.id)
            else
              Some (Rdf_term.Iri i)
        | Some ("_",_) -> Some (Rdf_term.(blank_ (blank_id_of_string s)))
        | Some (prefix,suffix) ->
            match SMap.find_opt prefix ctx.map with
            | None
            | Some `Null -> Some (Rdf_term.term_of_iri_string s)
            | Some `Nest -> Some (Rdf_term.term_of_iri_string s)
            | Some `Def { id } ->
               Some (Rdf_term.Iri (iri_append id suffix))

      with Iri.Error e -> error (Bad_iri (s, Iri.string_of_error e))

    let ctx_keywords = SSet.of_list
      ["@base" ; "@import" ; "@language" ; "@propagate" ;
        "@protected" ; "@type" ; "@version" ; "@vocab" ]

    let typ_of_json ?ctx json =
      (* TODO: @type can also be a term *)
      match j with
      | `Null  -> Some `Null
      | `String "@none" -> Some `None
      | `String "@id" -> Some `Id
      | `String "@json" -> Some `Json
      | `String "@vocab" -> Some `Vocab
      | `String s -> Some (`Iri (iri_of_string ?ctx s))
      | _ -> None

    let rec ctx_add_def ctx (term_, def_) =
      let term = term_.data in
      let def = def_.data in
      if SSet.mem term ctx_keywords then
        Lwt.return ctx
      else
        let (key, key_iri) =
          match as_compact_iri term with
          | Some _ ->
              let i = iri_of_string ~ctx term in
              (Iri.to_string i, Some i)
          | None ->
            try
                let i = Iri.of_string term in
                (term, Some i)
            with Iri.Error _ -> (term, None)
        in
        match def with
        | `String s ->
            let id = iri_of_string ~ctx s in
            let map = SMap.add key (`Def (mk_def id)) ctx.map in
            Lwt.return { ctx with map }
        | `Null ->
            let map = SMap.add key `Null ctx.map in
            Lwt.return { ctx with map }
        | `Float _ | `Bool _ | `List _ ->
            (* ignore definition *)
            Lwt.return ctx
        | `Obj l ->
            (* reference: https://www.w3.org/TR/json-ld11/#expanded-term-definition *)
            (* @id, @reverse, @type, @language, @container, @context, @prefix, @propagate, or @protected. *)
            let id = match l-->"@id" with
              | Some (`String s) -> Some (iri_of_string ~ctx s)
              | _ ->
                  match key_iri with
                  | None ->
                      (match ctx.vocab with
                      | None ->
                        (* there should have been an @id field, let's ignore this mapping *)
                        None
                      | Some iri_vocab ->
                          let s = (Iri.to_string iri_vocab)^term in
                          Some (iri_of_string s)
                      )
                  | Some ki when Iri.is_relative ki ->
                      (match ctx.vocab with
                      | None ->
                           (* there should have been an @id field, let's ignore this mapping *)
                           None
                       | Some iri_vocab ->
                           let s = (Iri.to_string iri_vocab)^term in
                           Some (iri_of_string s)
                      )
                  | x -> x
            in
            match id with
            | None -> Lwt.return ctx
            | Some id ->
                let protected = match l-->"@protected" with
                  | Some (`Bool b) -> b
                  | _ -> ctx.protected
                in
                let%lwt scoped_ctx = match l-->>"@context" with
                  | Some json -> let%lwt ctx = eval_ctx ctx json in Lwt.return_some ctx
                  | None -> Lwt.return_none
                in
                let typ = match l-->"@type" with
                  | None -> None
                  | Some j -> typ_of_json j
                in
                let language = match l-->"@language" with
                  | None -> None
                  | Some `Null -> Some `Null
                  | Some (`String s) -> Some (`Str s)
                  | _ -> None
                in
                let container = match l-->"@container" with
                  | None -> None
                  | Some (`String s) -> Some s
                  | _ -> None
                in
                let direction = match l-->"@direction" with
                  | Some `Null -> None
                  | Some (`String "rtl") -> Some `Rtl
                  | Some (`String "ltr") -> Some `Ltr
                  | _ -> None
                in
                let def = mk_def ~protected ?ctx:scoped_ctx ?typ ?language ?container ?direction id in
                let map = SMap.add key (`Def def) ctx.map in
                Lwt.return { ctx with map }


    and eval_ctx ctx json =
      match json.data with
      | `Obj l -> eval_ctx_obj ctx l
      | `List l -> Lwt_list.fold_left_s eval_ctx ctx l
      | `Null -> Lwt.return null_ctx (* TODO: check for protected terms in ctx *)
      | `String s_iri ->
          let iri = iri_of_string ~ctx s_iri in
          begin
            match%lwt IO.get iri with
            | Ok code ->
                begin
                  let json = Json.from_string_exn code in
                  match json.data with
                  | `Obj l ->
                      (match l-->>"@context" with
                      | None -> Lwt.return ctx
                      | Some json ->
                          let%lwt ctx2 = eval_ctx
                            { ctx with base = `Iri iri }
                              json
                          in
                          Lwt.return { ctx2 with base = ctx.base }
                      )
                  | _ -> Lwt.return ctx
                end
            | Error msg ->
                error (Iri_get (iri, msg))
          end
      | `Bool _ | `Float _ -> Lwt.return ctx

    and eval_ctx_obj ctx fields =
        let%lwt fields =
            match fields-->"@import" with
            | Some (`String s_iri) ->
                begin
                  let iri = iri_of_string s_iri in
                  match%lwt IO.get iri with
                  | Ok code ->
                      let fields =
                        match (Json.from_string_exn code).data with
                        | `Obj l ->
                            (match l-->"@context" with
                            | Some (`Obj l) ->
                                 (* TODO: what todo if the imported context contains a list
                                    of context definitions ? *)
                                 l @ fields
                             | _ -> fields
                            )
                        | _ -> fields
                      in
                      Lwt.return fields
                  | Error msg -> error (Iri_get (iri, msg))
                end
            | _ -> Lwt.return fields
          in
          let ctx = match fields-->"@language" with
            | Some `Null -> { ctx with language = None }
            | Some `String s -> { ctx with language = Some s }
            | _ -> ctx
          in
          let ctx = match fields-->"@direction" with
            | Some `Null -> { ctx with direction = None }
            | Some (`String "rtl") -> { ctx with direction = Some `Rtl }
            | Some (`String "ltr") -> { ctx with direction = Some `Ltr }
            | _ -> ctx
          in
          let ctx = match fields-->"@version" with
            | Some (`String s) -> { ctx with version = Some s}
            | _ -> ctx
          in
          let ctx = match fields-->"@propagate" with
            | Some (`Bool propagate) -> { ctx with propagate }
            | _ -> ctx
          in
          let ctx = match fields-->"@protected" with
            | Some (`Bool protected) -> { ctx with protected }
            | _ -> ctx
          in
          let ctx = match fields-->"@base" with
            | Some (`String s_iri) ->
                let iri = iri_of_string ~ctx s_iri in
                { ctx with base = `Iri iri }
            | Some `Null -> { ctx with base = `Null }
            | Some _ -> ctx
            | None -> ctx
          in
          let ctx = match fields-->"@vocab" with
            | Some (`String s_iri) ->
                let iri = iri_of_string ~ctx s_iri in
                { ctx with vocab = Some iri }
            | Some `Null -> { ctx with vocab = None }
            | _ -> ctx
          in
          let ctx = match fields-->"@type" with
            | Some (`Obj l) ->
                begin
                  match l-->"container" with
                  | Some (`String "@set") ->
                      let b = match l-->"protected" with
                        | Some (`Bool b) -> b
                        | _ -> false
                      in
                      { ctx with type_container_set = Some b }
                  | _ -> ctx
                end
            | Some (`Null) -> { ctx with type_container_set = None }
            | _ -> ctx
          in
          Lwt_list.fold_left_s ctx_add_def ctx fields

    let node_keywords = SSet.of_list ["@context" ; "@id" ; "@type"]

    let active_language (ctx:ctx) def =
      match def.language with
      | Some `Null -> None
      | Some (`Str s) -> Some s
      | None -> ctx.language

    let rec value_def_of_json ctx def json =
      match json.data with
      | `String value -> `Value (mk_value_def ?typ:def.typ json.data)
      | `Null -> `Null
      | `Bool b ->
          let str = if b then "true" else "false" in
          `Value (value_def_of_json def (`String str))
      | `Float f ->
          let str = string_of_float f in
          value_def_of_json def (`String str)
      | `List _ -> assert false
      | `Obj l ->
          match l-->"@value" with
          | None -> `Node json
          | Some value ->
              match l-->>"@id" with
              | Some j -> `Node j
              | None  ->
                  let ctx =  = match def.ctx with
                    | None -> ctx
                    | Some c -> c
                  in
                  let%lwt ctx =
                    match l-->>"@context" with
                    | Some json ->
                        let%lwt ctx = eval_ctx ctx0 json in Lwt.return ctx
                    | None -> Lwt.return ctx
                  in
                  let typ = match l-->"@type" with
                    | None -> None
                    | Some j -> typ_of_json ctx j
                  in
                  let direction = match fields-->"@direction" with
                    | Some `Null -> { ctx with direction = None }
                    | Some (`String "rtl") -> { ctx with direction = Some `Rtl }
                    | Some (`String "ltr") -> { ctx with direction = Some `Ltr }
                    | _ -> ctx
          in



    let rec add_prop_values g ctx sub def (json:Json.json) =
      match def.typ with
      | Some `Json ->
        let lang = active_language ctx def in
        let obj = Rdf_term.term_of_literal_string
          ~typ:Rdf_rdf.json ?lang (Json.to_string json)
        in
        g.Rdf_graph.add_triple ~sub ~pred:def.id ~obj

      | _ ->
          match json.data with
          | `List l -> List.iter (add_prop_values g ctx sub def) l
          | `Null -> ()
          | `
          | `String str ->
              let obj =
                match def.typ with
                | Some `Id -> Some (Ref_term.Iri (iri_of_string str))
                | Some typ -> map_to_rdfterm ctx ~typ str
                | None -> Some (Rdf_term.term_of_literal_string str)
              in
              match obj with
              | None -> None
              | Some obj -> g.Rdf_graph.add_triple ~sub ~pred:def.id ~obj

    let rec eval g ctx json =
      match json.data with
      | `Obj l -> eval_obj g ctx l
      | `List l -> eval_list g ctx l
      | `String _
      | `Bool _
      | `Float _
      | `Null -> Lwt.return_unit

    and eval_list g ctx items =
      Lwt_list.iter_s (eval g ctx) items

    and eval_obj g ctx0 fields =
          let%lwt ctx = match fields-->>"@context" with
            | Some json -> eval_ctx ctx0 json
            | None -> Lwt.return ctx0
          in
          let sub = match fields-->"@id" with
            | Some (`String s) ->
                (match map_to_rdfterm ctx s with
                 | None -> Rdf_term.Blank_ (g.Rdf_graph.new_blank_id ())
                 | Some ((Literal _) as t) ->
                     error (Invalid_subject t)
                 | Some t -> t
                )
            | _ -> Rdf_term.Blank_ (g.Rdf_graph.new_blank_id ())
          in
          let () = match fields-->"@type" with
            | Some (`String s) ->
                (match map_to_rdfterm ctx s with
                 | None -> ()
                 | Some ((Literal _) as t) ->
                     error (Invalid_type t)
                 | Some obj ->
                     g.Rdf_graph.add_triple ~sub ~pred:Rdf_rdf.type_ ~obj
                )
            | _ -> ()
          in
          Lwt.return_unit

    and eval_add_props g ctx0 ctx fields =
      let f (key, v) =
        if not (SSet.mem key.data node_keywords) then
              let (mapped_key,iri) = match map_to_rdfterm ctx key with
                | None -> None, None
                | Some (Rdf_term.Literal { lit_value }) -> Some lit_value, None
                | Some (Rdf_term.Blank_ id) -> Some (Rdf_term.string_of_blank_id id), None
                | Some (Rdf_term.Iri i) -> Some (Iri.to_string i), Some i
                | Some Rdf_term.Blank -> None, None
              in
              let def =
                match SMap.find_opt mapped_key ctx.map, ctx.vocab with
                | Some `Null, _ -> None
                | Some (`Def d), _ -> Some d
                | None, None -> None
                | None, Some vi ->
                    (* create a default definition *)
                    match iri with
                    | None -> error (Invalid_prop mapped_key)
                    | Some id -> mkdef ~typ:`None id
              in
              match def with
              | None -> ()
              | Some def -> add_prop_values g ctx def v
      in
      List.iter f fields



  end
*)