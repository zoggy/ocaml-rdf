#!/bin/bash

export FILE=$1
TMP=$1.tmp

cp -f "$1" "$1.tmp"

echo "{" > ${TMP}

for i in title desc query default_graph; do
echo -n "  $i:" >> ${TMP}
echo -n `grep -E "^$i" ${FILE} | cut -d'=' -f 2-20` >> ${TMP}
echo ',' >> ${TMP}
done

echo "}" >> ${TMP}
/bin/mv -f ${TMP} ${FILE}