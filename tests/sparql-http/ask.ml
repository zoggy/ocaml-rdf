open Rdf

let query_dbpedia =
  "PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX : <http://dbpedia.org/resource/>
PREFIX dbpedia2: <http://dbpedia.org/property/>
PREFIX dbpedia: <http://dbpedia.org/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>
PREFIX dbpprop: <http://dbpedia.org/property/>

  SELECT ?city ?codeinsee
  WHERE
   { ?city dbpedia-owl:inseeCode ?codeinsee .
           ?city dbpprop:name ?ville .
           ?city dbpedia-owl:populationTotal ?population .
           ?city dbpprop:populationDate ?date .
           ?city rdf:type dbpedia-owl:Settlement .
           ?city dbpedia-owl:country :France .
           FILTER ( (?population > 10000) &&
                    (?date >= \"2008\"^^<http://www.w3.org/2001/XMLSchema#integer>)
                  )
         }
   "

let (>>=) = Lwt.bind;;

open Sparql_protocol;;

let query iri q =
  let mes = {
      in_query = q ;
      in_dataset = Sparql_protocol.empty_dataset ;
    }
  in
  let graph = Graph.open_graph iri in
  Rdf_sparql_http_lwt.get ~graph ~base: iri
    (Uri.of_string (Iri.to_uri iri)) mes
    ~accept: "application/sparql-results+xml"
    >>=
    function
      Ok -> Lwt_io.write Lwt_io.stdout "OK"
    | Error e -> failwith (Sparql_protocol.string_of_error e)
    | Result r ->
        match r with
          Sparql.Solutions l ->
            Lwt_io.write Lwt_io.stdout (Printf.sprintf "%d solutions returned" (List.length l))
        | Sparql.Bool b ->
            Lwt_io.write Lwt_io.stdout (if b then "true" else "false")
        | _ -> assert false
;;

Lwt_main.run
 (query (Iri.of_string "https://dbpedia.org/sparql") query_dbpedia);;

