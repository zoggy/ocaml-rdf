open Rdf

let term = Term.term_of_iri_string "http://xmlns.com/foaf/0.1/"
let q1 = [%sparql {|PREFIX foaf:<http://xmlns.com/foaf/0.1/> SELECT ?name WHERE { ?x foaf:name ?name}|}]
let q2 = [%sparql
  "PREFIX foaf: %{term}
   SELECT ?name
   WHERE { ?x foaf:name ?name }"
      term
  ]

let base = Iri.of_string "http://foo.bar"
let g = Graph.open_graph base
let () = Ttl.from_string g ~base
{|
@prefix foaf:        <http://xmlns.com/foaf/0.1/> .

_:a  foaf:name       "Alice" .
_:a  foaf:mbox       <mailto:alice@work.example> .

_:b  foaf:name       "Bob" .
_:b  foaf:mbox       <mailto:bob@work.example> .
|}

let eval_query query =
  let dataset = Ds.dataset g in
  match Sparql.execute ~base dataset query with
  | Sparql.Bool true -> print_endline "true"
  | Sparql.Bool false -> print_endline "false"
  | Sparql.Graph _ -> print_endline "graph"
  | Sparql.Solutions sols ->
      let f_sol sol =
        Sparql.solution_iter
          (fun name term -> print_string (name^"->"^(Term.string_of_term term)^" ; "))
          sol;
        print_newline()
      in
      print_endline "Solutions:";
      List.iter f_sol sols
;;

let () = List.iter eval_query [q1 ; q2]
