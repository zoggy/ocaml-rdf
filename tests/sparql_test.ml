(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Testing. *)

open Rdf
open Sparql_types;;
open Sparql;;

let verb = print_endline;;

type test_spec =
  { base : Iri.t option ;
    title : string ;
    desc : string option;
    query : string ;
    default_graph : string option ;
    named : (Iri.t * string) list ;
    options : (string * string) list ;
  }

type result = Error of string | Ok of Sparql.query_result

type test = {
  spec : test_spec ;
  result : result ;
  }

let load_file ?graph_options file =
  verb ("Loading file "^file);
  let g = Ocf.group in
  let base = Ocf.option_ Ocf.Wrapper.string ~doc:"Base iri" None in
  let g = Ocf.add g ["base"] base in
  let title = Ocf.string ~doc:"Title of the test" "" in
  let g = Ocf.add g ["title"] title in
  let desc = Ocf.string ~doc:"Description of the test" "" in
  let g = Ocf.add g ["descr"] desc in
  let query = Ocf.string ~doc:"File containing the query" ""  in
  let g = Ocf.add g ["query"] query in
  let default_graph = Ocf.string ~doc:"File containing the default graph" ""  in
  let g = Ocf.add g ["default_graph"] default_graph in
  let named = Ocf.(list (Wrapper.(pair string string))
     ~doc:"Named graphs in the form (iri, file.ttl)" [])
  in
  let g = Ocf.add g ["named_graphs"] named in
  let options = Ocf.(list (Wrapper.(pair string string))
    ~doc: "Graph creation options (storage, ...)" [])
  in
  let g = Ocf.add g [ "graph_options"] options in
  Ocf.from_file g file;
  let mk_filename =
    let path = Filename.dirname file in
    fun file ->
      if Filename.is_relative file then
        Filename.concat path file
      else
        file
  in
  let options =
    match graph_options with
      None -> Ocf.get options
    | Some s ->
      let json = Printf.sprintf "{o:[%s]}" s in
      let g = Ocf.add Ocf.group ["o"] options in
      Ocf.from_string g json;
      Ocf.get options
  in
  let named = List.map (fun (iri, s) -> (Iri.of_string iri, mk_filename s)) (Ocf.get named) in
  { base = Misc.map_opt Iri.of_string (Ocf.get base) ;
    title = Ocf.get title ;
    desc = Misc.opt_of_string (Ocf.get desc) ;
    query = mk_filename (Ocf.get query) ;
    default_graph = Misc.map_opt mk_filename (Misc.opt_of_string (Ocf.get default_graph)) ;
    named ;
    options ;
  }
;;

let load_ttl g base file =
  verb ("Loading graph from "^file);
  try Ttl.from_file g ~base file
  with
  | Ttl.Error e ->
      prerr_endline (Ttl.string_of_error e);
      exit 1
;;

let mk_graph spec =
  let base =
    match spec.base with
      None -> Iri.of_string "http://localhost/"
    | Some iri -> iri
  in
  let default =
    let g = Graph.open_graph ~options: spec.options base in
    match spec.default_graph with
       None -> g
     | Some file ->
        let size = g.Graph.size () in
        if size <= 0 then
          ignore(load_ttl g base file);
        g
  in
  default

let mk_dataset spec =
  let base =
    match spec.base with
      None -> Iri.of_string "http://localhost/"
    | Some iri -> iri
  in
  let default =
    let g = Graph.open_graph ~options: spec.options base in
    match spec.default_graph with
       None -> g
     | Some file ->
        let size = g.Graph.size () in
        if size <= 0 then
          ignore(load_ttl g base file);
        g
  in
  let f (iri, file) =
    let g = Graph.open_graph base in
    load_ttl g base file ;
    (`I iri, g)
  in
  let named = List.map f spec.named in
  Ds.simple_dataset ~named default
;;

let print_solution solution =
  Sparql.solution_iter
    (fun name term -> print_string (name^"->"^(Term.string_of_term term)^" ; "))
    solution;
  print_newline()
;;

let print_result = function
  Error s -> print_endline ("ERROR: "^s)
| Ok (Solutions solutions) ->
   Printf.printf "%d Solution(s):\n" (List.length solutions);
   List.iter print_solution solutions
| Ok (Bool b) ->
   print_endline (if b then "true" else "false")
| Ok (Graph g) ->
    print_endline (Ttl.to_string g)
;;
