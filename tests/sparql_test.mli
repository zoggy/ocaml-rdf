(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

val verb : string -> unit
type test_spec = {
  base : Iri.t option;
  title : string;
  desc : string option;
  query : string;
  default_graph : string option;
  named : (Iri.t * string) list;
  options : (string * string) list;
}
type result = Error of string | Ok of Rdf.Sparql.query_result
type test = { spec : test_spec; result : result; }
val load_file : ?graph_options:string -> string -> test_spec
val load_ttl : Rdf.Graph.graph -> Iri.t -> string -> unit
val mk_graph : test_spec -> Rdf.Graph.graph
val mk_dataset : test_spec -> Rdf.Ds.dataset
val print_solution : Rdf.Sparql.solution -> unit
val print_result : result -> unit
