(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Just read json ld file, transform into rdf and output ttl
  from default graph. *)

open Rdf_json_ld

let lwt_reporter () =
  (* beware to set style before defining the lwt_reporter, or style
     will not be taken into account *)
  Fmt_tty.setup_std_outputs ();
  let buf_fmt ~like =
    let b = Buffer.create 512 in
    Fmt.with_buffer ~like b,
    fun () -> let m = Buffer.contents b in Buffer.reset b; m
  in
  let app, app_flush = buf_fmt ~like:Fmt.stdout in
  let dst, dst_flush = buf_fmt ~like:Fmt.stderr in
  let reporter = Logs_fmt.reporter ~app ~dst () in
  let report src level ~over k msgf =
    let k () =
      let write () = match level with
        | Logs.App -> Lwt_io.write Lwt_io.stdout (app_flush ())
        | _ -> Lwt_io.write Lwt_io.stderr (dst_flush ())
      in
      let unblock () = over (); Lwt.return_unit in
      Lwt.finalize write unblock |> Lwt.ignore_result;
      k ()
    in
    reporter.Logs.report src level ~over:(fun () -> ()) k msgf;
  in
  { Logs.report = report }

let install_log_reporter () = Logs.set_reporter (lwt_reporter ())

type action = | Expand | ToRDF | FromRDF

let on_file ~options ~base ~action file =
  let base_iri = Iri.of_string (base^"/"^(Filename.basename file)) in
  try%lwt
    let%lwt input = Lwt_io.(with_file ~mode:Input file read) in
    let g = Rdf.Graph.open_graph base_iri in
    match action with
    | FromRDF ->
        let ds = Rdf.Ds.mem_dataset g in
        Rdf.Nq.from_string ds input ;
        let json = Json_ld.from_rdf options ds in
        let res = J.normalize json in
        Lwt_io.(write_line stdout (J.to_string res))
    | Expand ->
        let json = J.from_string_exn input in
        let%lwt () =Lwt_io.(write_line stderr (J.to_string json)) in
        let ctx = Json_ld.init_ctx base_iri in
        let%lwt ctx =
          match options.expand_context with
          | None -> Lwt.return ctx
          | Some (`Json _) ->
              Rdf.Log.warn (fun m -> m "expand_context given as json");
              Lwt.return ctx
          | Some (`Iri iri) ->
              let%lwt (_, json)= Expand.load_remote_context options Iri.Map.empty iri in
              let%lwt (_,ctx) = Expand.process_ctx options ctx base_iri Iri.Map.empty json in
              Lwt.return ctx
        in
        let%lwt res = Json_ld.expansion options ctx json base_iri in
        let res = J.normalize res in
        Lwt_io.(write_line stdout (J.to_string res))
    | ToRDF ->
        let json = J.from_string_exn input in
        let%lwt () = Lwt_io.(write_line stderr (J.to_string json)) in
        let%lwt (ds,_) = Json_ld.to_rdf options json g in
        (*Rdf.Ds.merge_to_default ds;
           let ttl = Rdf.Ttl.to_string ds.default in*)
        let nq = Rdf.Nq.to_string ds in
        Lwt_io.(write_line stdout nq)
  with
  | Iri.Error e ->
      Lwt_io.(write stderr (Iri.string_of_error e))
(*  with e ->
     let%lwt () = Lwt_io.(write stderr (Printexc.to_string e)) in
      raise e
*)

let base_iri = ref (Printf.sprintf "file://%s" (Sys.getcwd()))
let action = ref ToRDF
let use_native_types = ref false

let options = [
    "--use-native-types", Arg.Set use_native_types, " set useNativeTypes flag to true" ;
    "-b", Arg.Set_string base_iri, " [iri] use [iri]/<file> as base IRI" ;
    "--expand", Arg.Unit (fun () -> action := Expand), " expand JSON-LD input" ;
    "--from-rdf", Arg.Unit (fun () -> action := FromRDF), " output JSON-LD from N-quads input" ;
    "--to-rdf", Arg.Unit (fun () -> action := ToRDF), " output N-quards from JSON-LD input" ;
]

let usage = Printf.sprintf "Usage: %s [options] <files>" Sys.argv.(0)
let main () =
  Logs.(set_level (Some Debug));
  install_log_reporter ();
  let files = ref [] in
  Arg.parse options (fun s -> files := s :: !files)
    (Printf.sprintf "%s\nwhere options are:" usage);
  match !files with
  | [] -> failwith usage
  | files -> 
      let options = Rdf_json_ld.T.(options
         ~use_native_types:!use_native_types
           load_remote_curl
        )
      in
      Lwt_list.iter_s (on_file ~options ~base:!base_iri ~action:!action) files

let () = Lwt_main.run (main ())