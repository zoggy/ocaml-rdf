(** Elements of [https://w3c.github.io/json-ld-api/tests/vocab#] *)

open Rdf

(** [https://w3c.github.io/json-ld-api/tests/vocab#] *)
val jsonld_test_vocab : Iri.t
val jsonld_test_vocab_ : string -> Iri.t

(** A `CompactTest` modifies either a `PositiveEvaluationTest`, `NegativeEvaluationTest`, `PositiveSyntaxTest` or `NegativeSyntaxTest`. *)
val c_CompactTest : Iri.t

(** A `ExpandTest` modifies either a `PositiveEvaluationTest`, `NegativeEvaluationTest`, `PositiveSyntaxTest` or `NegativeSyntaxTest`. *)
val c_ExpandTest : Iri.t

(** A `FlattenTest` modifies either a `PositiveEvaluationTest`, `NegativeEvaluationTest`, `PositiveSyntaxTest` or `NegativeSyntaxTest`. *)
val c_FlattenTest : Iri.t

(** A `FrameTest` modifies either a `PositiveEvaluationTest`, `NegativeEvaluationTest`, `PositiveSyntaxTest` or `NegativeSyntaxTest`. *)
val c_FrameTest : Iri.t

(** A `FromRDFTest` modifies either a `PositiveEvaluationTest`, `NegativeEvaluationTest`, `PositiveSyntaxTest` or `NegativeSyntaxTest`. *)
val c_FromRDFTest : Iri.t

(** An `HtmlTest` modifies either a `PositiveEvaluationTest` or `NegativeEvaluationTest` indicating that the source is of type text/html, which requires optional _HTML script extraction_ support. *)
val c_HtmlTest : Iri.t

(** An `HttpTest` modifies either a `PositiveEvaluationTest` or `NegativeEvaluationTest`. *)
val c_HttpTest : Iri.t

(** A Negative Evaluation test is successful when the result of processing the input file specified as `mf:action` (aliased as "input" in test manifest) results in the error identified by the literal value of `mf:result` (aliased as "expectErrorCode" in test manifest). The specifics of invoking test, including the interpretation of options (`:option`) and other input files are specified through another class. See the [README](https://w3c.github.io/json-ld-api/tests/) for more details on running tests. *)
val c_NegativeEvaluationTest : Iri.t

(** A type of test specifically for syntax testing. Syntax tests are not required to have an associated result, only an action. Negative syntax tests are tests of which the result should be a parser error. *)
val c_NegativeSyntaxTest : Iri.t

(** Options passed to the test runner to affect invocation of the appropriate API method. *)
val c_Option : Iri.t

(** A Positive Evaluation test is successful when the result of processing the input file specified as `mf:action` (aliased as "input" in test manifest) exactly matches the output file specified as `mf:result` (aliased as "expect" in test manifest) using the comparison defined in another class. The specifics of invoking test, including the interpretation of options (`:option`) and other input files are specified through another class. *)
val c_PositiveEvaluationTest : Iri.t

(** A type of test specifically for syntax testing. Syntax tests are not required to have an associated result, only an action. *)
val c_PositiveSyntaxTest : Iri.t

(** All JSON-LD tests have an input file referenced using `mf:action` (aliased as "input" in test manifest). Positive and Negative Evaluation Tests also have a result file referenced using `mf:result` (aliased as "expect" and "expectErrorCode" for respectively Positive and Negative Evaluation Tests in test manifest). Other tests may take different inputs and options as defined for each test class. Tests should be run with the processingMode option set to "json-ld-1.1", unless specified explicitly as a test option. *)
val c_Test : Iri.t

(** A `ToRDFTest` modifies either a `PositiveEvaluationTest`, `NegativeEvaluationTest`, `PositiveSyntaxTest` or `NegativeSyntaxTest`. *)
val c_ToRDFTest : Iri.t

(** The base IRI to use when expanding or compacting the document. If set, this overrides the input document's IRI. *)
val base : Iri.t

(** If set to `true`, the JSON-LD processor replaces arrays with just one element with that element during compaction. If set to false, all arrays will remain arrays even if they have just one element. *)
val compactArrays : Iri.t

(** If set to `false`, the JSON-LD processor will not attempt to compact using document-relative IRIs. *)
val compactToRelative : Iri.t

(** The HTTP Content-Type used for the input file, in case it is a non-registered type. *)
val contentType : Iri.t

(** A context that is used for transforming the input document. *)
val context : Iri.t

(** A context that is used to initialize the active context when expanding a document. *)
val expandContext : Iri.t

(** Whether to extract all script elements from HTML, or just the first encountered. *)
val extractAllScripts : Iri.t

(** A frame that is used for transforming the input document. *)
val frame : Iri.t

(** An HTTP Accept header. *)
val httpAccept : Iri.t

(** An HTTP Link header to be added to the result of requesting the input file. *)
val httpLink : Iri.t

(** The HTTP status code that must be returned when the input file is requested. This is typically used along with the `redirectTo` property. *)
val httpStatus : Iri.t

(** Secondary input file *)
val input : Iri.t

(** Indicates that this is or is not a test of a normative requirement. Tests without this option are treated as being normative. *)
val normative : Iri.t

(** Options affecting processing *)
val option : Iri.t

(** If set to "json-ld-1.1", the JSON-LD processor must produce exactly the same results as the algorithms defined in this specification. If set to another value, the JSON-LD processor is allowed to extend or modify the algorithms defined in this specification to enable application-specific optimizations. The definition of such optimizations is beyond the scope of this specification and thus not defined. Consequently, different implementations may implement different optimizations. Developers must not define modes beginning with json-ld as they are reserved for future versions of this specification. *)
val processingMode : Iri.t

(** A particular processor feature that may be supported by some implementations. *)
val processorFeature : Iri.t

(** Unless the produce generalized RDF flag is set to true, RDF triples containing a blank node predicate are excluded from output. *)
val produceGeneralizedRdf : Iri.t

(** The location of a URL for redirection. A request made of the input file must be redirected to the designated URL. *)
val redirectTo : Iri.t

(** Indicates the JSON-LD version to which the test applies, rather than the specific processing mode. Values are "json-ld-1.0", and "json-ld-1.1". If not set, the test is presumed to be valid for all versions of JSON-LD. In cases where results differ between spec versions for the same test, the test will have both a "1.0" and "1.1" version, for example. *)
val specVersion : Iri.t

(** Requires the use of JSON Canonicalization Scheme when generating RDF literals from JSON literal values. *)
val useJCS : Iri.t

(** If the _use native types_ flag is set to `true`, RDF literals with a datatype IRI that equal `xsd:integer` or `xsd:double` are converted to a JSON numbers and RDF literals with a datatype IRI that equals `xsd:boolean` are converted to `true` or `false` based on their lexical form. *)
val useNativeTypes : Iri.t

(** If the _use rdf type_ flag is set to `true`, statements with an `rdf:type` predicate will not use `@type`, but will be transformed as a normal property. *)
val useRdfType : Iri.t


module Open : sig
  (** A `CompactTest` modifies either a `PositiveEvaluationTest`, `NegativeEvaluationTest`, `PositiveSyntaxTest` or `NegativeSyntaxTest`. *)
  val jsonld_test_vocab_c_CompactTest : Iri.t

  (** A `ExpandTest` modifies either a `PositiveEvaluationTest`, `NegativeEvaluationTest`, `PositiveSyntaxTest` or `NegativeSyntaxTest`. *)
  val jsonld_test_vocab_c_ExpandTest : Iri.t

  (** A `FlattenTest` modifies either a `PositiveEvaluationTest`, `NegativeEvaluationTest`, `PositiveSyntaxTest` or `NegativeSyntaxTest`. *)
  val jsonld_test_vocab_c_FlattenTest : Iri.t

  (** A `FrameTest` modifies either a `PositiveEvaluationTest`, `NegativeEvaluationTest`, `PositiveSyntaxTest` or `NegativeSyntaxTest`. *)
  val jsonld_test_vocab_c_FrameTest : Iri.t

  (** A `FromRDFTest` modifies either a `PositiveEvaluationTest`, `NegativeEvaluationTest`, `PositiveSyntaxTest` or `NegativeSyntaxTest`. *)
  val jsonld_test_vocab_c_FromRDFTest : Iri.t

  (** An `HtmlTest` modifies either a `PositiveEvaluationTest` or `NegativeEvaluationTest` indicating that the source is of type text/html, which requires optional _HTML script extraction_ support. *)
  val jsonld_test_vocab_c_HtmlTest : Iri.t

  (** An `HttpTest` modifies either a `PositiveEvaluationTest` or `NegativeEvaluationTest`. *)
  val jsonld_test_vocab_c_HttpTest : Iri.t

  (** A Negative Evaluation test is successful when the result of processing the input file specified as `mf:action` (aliased as "input" in test manifest) results in the error identified by the literal value of `mf:result` (aliased as "expectErrorCode" in test manifest). The specifics of invoking test, including the interpretation of options (`:option`) and other input files are specified through another class. See the [README](https://w3c.github.io/json-ld-api/tests/) for more details on running tests. *)
  val jsonld_test_vocab_c_NegativeEvaluationTest : Iri.t

  (** A type of test specifically for syntax testing. Syntax tests are not required to have an associated result, only an action. Negative syntax tests are tests of which the result should be a parser error. *)
  val jsonld_test_vocab_c_NegativeSyntaxTest : Iri.t

  (** Options passed to the test runner to affect invocation of the appropriate API method. *)
  val jsonld_test_vocab_c_Option : Iri.t

  (** A Positive Evaluation test is successful when the result of processing the input file specified as `mf:action` (aliased as "input" in test manifest) exactly matches the output file specified as `mf:result` (aliased as "expect" in test manifest) using the comparison defined in another class. The specifics of invoking test, including the interpretation of options (`:option`) and other input files are specified through another class. *)
  val jsonld_test_vocab_c_PositiveEvaluationTest : Iri.t

  (** A type of test specifically for syntax testing. Syntax tests are not required to have an associated result, only an action. *)
  val jsonld_test_vocab_c_PositiveSyntaxTest : Iri.t

  (** All JSON-LD tests have an input file referenced using `mf:action` (aliased as "input" in test manifest). Positive and Negative Evaluation Tests also have a result file referenced using `mf:result` (aliased as "expect" and "expectErrorCode" for respectively Positive and Negative Evaluation Tests in test manifest). Other tests may take different inputs and options as defined for each test class. Tests should be run with the processingMode option set to "json-ld-1.1", unless specified explicitly as a test option. *)
  val jsonld_test_vocab_c_Test : Iri.t

  (** A `ToRDFTest` modifies either a `PositiveEvaluationTest`, `NegativeEvaluationTest`, `PositiveSyntaxTest` or `NegativeSyntaxTest`. *)
  val jsonld_test_vocab_c_ToRDFTest : Iri.t

  (** The base IRI to use when expanding or compacting the document. If set, this overrides the input document's IRI. *)
  val jsonld_test_vocab_base : Iri.t

  (** If set to `true`, the JSON-LD processor replaces arrays with just one element with that element during compaction. If set to false, all arrays will remain arrays even if they have just one element. *)
  val jsonld_test_vocab_compactArrays : Iri.t

  (** If set to `false`, the JSON-LD processor will not attempt to compact using document-relative IRIs. *)
  val jsonld_test_vocab_compactToRelative : Iri.t

  (** The HTTP Content-Type used for the input file, in case it is a non-registered type. *)
  val jsonld_test_vocab_contentType : Iri.t

  (** A context that is used for transforming the input document. *)
  val jsonld_test_vocab_context : Iri.t

  (** A context that is used to initialize the active context when expanding a document. *)
  val jsonld_test_vocab_expandContext : Iri.t

  (** Whether to extract all script elements from HTML, or just the first encountered. *)
  val jsonld_test_vocab_extractAllScripts : Iri.t

  (** A frame that is used for transforming the input document. *)
  val jsonld_test_vocab_frame : Iri.t

  (** An HTTP Accept header. *)
  val jsonld_test_vocab_httpAccept : Iri.t

  (** An HTTP Link header to be added to the result of requesting the input file. *)
  val jsonld_test_vocab_httpLink : Iri.t

  (** The HTTP status code that must be returned when the input file is requested. This is typically used along with the `redirectTo` property. *)
  val jsonld_test_vocab_httpStatus : Iri.t

  (** Secondary input file *)
  val jsonld_test_vocab_input : Iri.t

  (** Indicates that this is or is not a test of a normative requirement. Tests without this option are treated as being normative. *)
  val jsonld_test_vocab_normative : Iri.t

  (** Options affecting processing *)
  val jsonld_test_vocab_option : Iri.t

  (** If set to "json-ld-1.1", the JSON-LD processor must produce exactly the same results as the algorithms defined in this specification. If set to another value, the JSON-LD processor is allowed to extend or modify the algorithms defined in this specification to enable application-specific optimizations. The definition of such optimizations is beyond the scope of this specification and thus not defined. Consequently, different implementations may implement different optimizations. Developers must not define modes beginning with json-ld as they are reserved for future versions of this specification. *)
  val jsonld_test_vocab_processingMode : Iri.t

  (** A particular processor feature that may be supported by some implementations. *)
  val jsonld_test_vocab_processorFeature : Iri.t

  (** Unless the produce generalized RDF flag is set to true, RDF triples containing a blank node predicate are excluded from output. *)
  val jsonld_test_vocab_produceGeneralizedRdf : Iri.t

  (** The location of a URL for redirection. A request made of the input file must be redirected to the designated URL. *)
  val jsonld_test_vocab_redirectTo : Iri.t

  (** Indicates the JSON-LD version to which the test applies, rather than the specific processing mode. Values are "json-ld-1.0", and "json-ld-1.1". If not set, the test is presumed to be valid for all versions of JSON-LD. In cases where results differ between spec versions for the same test, the test will have both a "1.0" and "1.1" version, for example. *)
  val jsonld_test_vocab_specVersion : Iri.t

  (** Requires the use of JSON Canonicalization Scheme when generating RDF literals from JSON literal values. *)
  val jsonld_test_vocab_useJCS : Iri.t

  (** If the _use native types_ flag is set to `true`, RDF literals with a datatype IRI that equal `xsd:integer` or `xsd:double` are converted to a JSON numbers and RDF literals with a datatype IRI that equals `xsd:boolean` are converted to `true` or `false` based on their lexical form. *)
  val jsonld_test_vocab_useNativeTypes : Iri.t

  (** If the _use rdf type_ flag is set to `true`, statements with an `rdf:type` predicate will not use `@type`, but will be transformed as a normal property. *)
  val jsonld_test_vocab_useRdfType : Iri.t

end

class from : ?sub: Term.term -> Graph.graph ->
  object
    method base : Term.term list
    method base_opt : Term.term option
    method base_iris : Iri.t list
    method base_opt_iri : Iri.t option
    method compactArrays : Term.term list
    method compactArrays_opt : Term.term option
    method compactArrays_iris : Iri.t list
    method compactArrays_opt_iri : Iri.t option
    method compactToRelative : Term.term list
    method compactToRelative_opt : Term.term option
    method compactToRelative_iris : Iri.t list
    method compactToRelative_opt_iri : Iri.t option
    method contentType : Term.literal list
    method contentType_opt : Term.literal option
    method context : Term.term list
    method context_opt : Term.term option
    method context_iris : Iri.t list
    method context_opt_iri : Iri.t option
    method expandContext : Term.term list
    method expandContext_opt : Term.term option
    method expandContext_iris : Iri.t list
    method expandContext_opt_iri : Iri.t option
    method extractAllScripts : Term.term list
    method extractAllScripts_opt : Term.term option
    method extractAllScripts_iris : Iri.t list
    method extractAllScripts_opt_iri : Iri.t option
    method frame : Term.term list
    method frame_opt : Term.term option
    method frame_iris : Iri.t list
    method frame_opt_iri : Iri.t option
    method httpAccept : Term.literal list
    method httpAccept_opt : Term.literal option
    method httpLink : Term.literal list
    method httpLink_opt : Term.literal option
    method httpStatus : Term.term list
    method httpStatus_opt : Term.term option
    method httpStatus_iris : Iri.t list
    method httpStatus_opt_iri : Iri.t option
    method input : Term.term list
    method input_opt : Term.term option
    method input_iris : Iri.t list
    method input_opt_iri : Iri.t option
    method normative : Term.term list
    method normative_opt : Term.term option
    method normative_iris : Iri.t list
    method normative_opt_iri : Iri.t option
    method option : Term.term list
    method option_opt : Term.term option
    method option_iris : Iri.t list
    method option_opt_iri : Iri.t option
    method processingMode : Term.literal list
    method processingMode_opt : Term.literal option
    method processorFeature : Term.literal list
    method processorFeature_opt : Term.literal option
    method produceGeneralizedRdf : Term.term list
    method produceGeneralizedRdf_opt : Term.term option
    method produceGeneralizedRdf_iris : Iri.t list
    method produceGeneralizedRdf_opt_iri : Iri.t option
    method redirectTo : Term.literal list
    method redirectTo_opt : Term.literal option
    method specVersion : Term.literal list
    method specVersion_opt : Term.literal option
    method useJCS : Term.term list
    method useJCS_opt : Term.term option
    method useJCS_iris : Iri.t list
    method useJCS_opt_iri : Iri.t option
    method useNativeTypes : Term.term list
    method useNativeTypes_opt : Term.term option
    method useNativeTypes_iris : Iri.t list
    method useNativeTypes_opt_iri : Iri.t option
    method useRdfType : Term.term list
    method useRdfType_opt : Term.term option
    method useRdfType_iris : Iri.t list
    method useRdfType_opt_iri : Iri.t option
  end
