(** Read test manifest in jsonld format and run described tests.*)

open Rdf_json_ld

let lwt_reporter () =
  (* beware to set style before defining the lwt_reporter, or style
     will not be taken into account *)
  Fmt_tty.setup_std_outputs ();
  let buf_fmt ~like =
    let b = Buffer.create 512 in
    Fmt.with_buffer ~like b,
    fun () -> let m = Buffer.contents b in Buffer.reset b; m
  in
  let app, app_flush = buf_fmt ~like:Fmt.stdout in
  let dst, dst_flush = buf_fmt ~like:Fmt.stderr in
  let reporter = Logs_fmt.reporter ~app ~dst () in
  let report src level ~over k msgf =
    let k () =
      let write () = match level with
        | Logs.App -> Lwt_io.write Lwt_io.stdout (app_flush ())
        | _ -> Lwt_io.write Lwt_io.stderr (dst_flush ())
      in
      let unblock () = over (); Lwt.return_unit in
      Lwt.finalize write unblock |> Lwt.ignore_result;
      k ()
    in
    reporter.Logs.report src level ~over:(fun () -> ()) k msgf;
  in
  { Logs.report = report }

let install_log_reporter () = Logs.set_reporter (lwt_reporter ())

let base_str = "https://w3c.github.io/json-ld-api/tests/"
let base_iri = Iri.of_string base_str
let iri_rdf_direction = Iri.of_string (base_str ^ "vocab#rdfDirection")


open Rdf

let log_src = Logs.Src.create "test_manifest"
module Log = (val Logs.src_log log_src)

let one_or_fail msg = function
| [] -> failwith msg
| x :: _ -> x

let stop_on_fail = ref false

let get_bool_option g pred sub =
  match Rdf.Graph.literal_objects_of g ~sub ~pred with
  | [] -> None
  | lit :: _ -> Some (Rdf.Term.bool_of_literal lit)

let get_string_option g pred sub =
  match Rdf.Graph.literal_objects_of g ~sub ~pred with
  | [] -> None
  | lit :: _ -> Some lit.Rdf.Term.lit_value

let get_iri_option g pred sub =
  match Rdf.Graph.iri_objects_of g ~sub ~pred with
  | [] -> None
  | iri :: _ -> Some iri

let get_first =
  let rec iter f = function
  | [] -> None
  | h :: q -> match f h with None -> iter f q | x -> x
  in
  fun f l -> iter f l

let get_options g dl props =
  (*Log.info (fun m -> m "get_options %s"
     (String.concat ", " (List.map Rdf.Term.string_of_term props)));
  *)
  (*let props =
    List.fold_left (fun acc sub ->
       List.fold_left (fun acc (_,pred,obj) -> Iri.Map.add pred obj acc)
         acc (g.Rdf.Graph.find ~sub ()))
      Iri.Map.empty subs
  in*)
  let base = Option.map Iri.to_string
    (get_first (get_iri_option g Jsonld_test_vocab.base) props)
  in
  let compact_arrays = get_first (get_bool_option g Jsonld_test_vocab.compactArrays) props in
  let compact_to_relative = get_first
    (get_bool_option g Jsonld_test_vocab.compactToRelative) props
  in
  let expand_context = Option.map (fun i -> `Iri i)
    (get_first (get_iri_option g Jsonld_test_vocab.expandContext) props)
  in
  let extract_all_scripts = get_first
    (get_bool_option g Jsonld_test_vocab.extractAllScripts) props
  in
  (*let frame_expansion = get_bool_option g props Jsonld_test_vocab.frameExpansion in *)
  (* ordered : bool ; *)
  let processing_mode = get_first
    (get_string_option g Jsonld_test_vocab.processingMode) props
  in
  let produce_generalized_rdf =  get_first
    (get_bool_option g Jsonld_test_vocab.produceGeneralizedRdf) props
  in
  let rdf_direction = Option.map T.rdf_direction_of_string
    (get_first (get_string_option g iri_rdf_direction) props) in
  (match rdf_direction with
   | None -> ()
   | Some d -> Log.info (fun m -> m "option rdf_direction: %s"
          (T.string_of_rdf_direction d))
  );
  let use_native_types = get_first
    (get_bool_option g Jsonld_test_vocab.useNativeTypes) props
  in
  (match use_native_types with
   | None -> ()
   | Some b -> 
       Log.info (fun m -> m "option use_native_types: %b" b)
  );
  let use_rdf_type = get_first
    (get_bool_option g Jsonld_test_vocab.useRdfType) props
  in
  T.options ?base ?compact_arrays ?compact_to_relative
    ?expand_context ?extract_all_scripts ?processing_mode
    ?produce_generalized_rdf
    ?use_native_types ?use_rdf_type ?rdf_direction
    dl


let run_test ~action ~positive g ~(options:T.options) test =
  let test_term = Rdf.Term.Iri test in
  let input_iri = one_or_fail "No input"
    (Graph.iri_objects_of g ~sub:test_term ~pred:Test_manifest.action)
  in
  let g_test = Rdf.Graph.open_graph input_iri in
  let base_url =
    match options.T.base with
    | Some str ->
        let iri = Iri.of_string str in
        Log.info (fun m -> m "option base: %a" Iri.pp iri);
        iri
    | _ -> g_test.name()
  in
  if List.exists (Iri.equal Jsonld_test_vocab.c_PositiveSyntaxTest)
    (Graph.types_of g test_term)
  then
    (
     try%lwt
       let%lwt _ = action options ~base_url ~input:input_iri in
       Lwt.return `Passed
     with
     | Iri.Error e ->
         let msg = Printf.sprintf
           "Unexpected failure: %s\nBacktrace:%s"
             (Iri.string_of_error e) (Printexc.get_backtrace())
         in
             Lwt.return (`Failed msg)
     | T.Error e ->
         let msg = Printf.sprintf
           "Unexpected failure: %s\nBacktrace:%s"
             (T.string_of_error e) (Printexc.get_backtrace())
         in
         Lwt.return (`Failed msg)
    )
  else
    let result =
      one_or_fail "No result"
        (g.objects_of ~sub:test_term ~pred:Test_manifest.result)
    in
    if List.exists (Iri.equal Jsonld_test_vocab.c_PositiveEvaluationTest)
      (Graph.types_of g test_term)
    then
      match result with
      | Term.Iri expected ->
          (
         try%lwt
             let%lwt result = action options ~base_url ~input:input_iri in
             positive options ~base_url ~expected ~result
           with
           | Iri.Error e ->
               let msg = Printf.sprintf
                 "Unexpected failure: %s\nBacktrace:%s"
                   (Iri.string_of_error e) (Printexc.get_backtrace())
               in
               Lwt.return (`Failed msg)
           | T.Error e ->
               let msg = Printf.sprintf
               "Unexpected failure: %s\nBacktrace:%s"
                   (T.string_of_error e) (Printexc.get_backtrace())
               in
               Lwt.return (`Failed msg)
          )
      | _ ->
          Lwt.return (`Failed "Expected result is not an iri")
    else
      match result with
      | Term.Literal lit ->
          (try%lwt
             let%lwt res = action options ~base_url ~input:input_iri in
             let msg = Printf.sprintf
               "Test should have failed with error %S" lit.lit_value
             in
             Lwt.return (`Failed msg)
           with
         | T.Error e ->
               let msg = T.code_of_error e in
               if msg <> lit.lit_value then
                 (
                  let msg = Printf.sprintf
                    "Test should have failed with %S but failed with %S\nBacktrace:%s"
                      lit.lit_value msg (Printexc.get_backtrace())
                  in
                  Lwt.return (`Failed msg)
                 )
               else
                 Lwt.return `Passed
          )
      | _ ->
          Lwt.return (`Failed "Expected error is not a literal")

let run_expand_test =
  let action (options:T.options) ~base_url ~input =
    let%lwt input = T.load_remote_json options.document_loader input in
    let%lwt ctx = Json_ld.ctx_of_options options base_url in
    let%lwt res = Json_ld.expansion { options with ordered = true } ctx input base_url in
    let res = J.normalize res in
    Lwt.return res
  in
  let positive (options:T.options) ~base_url ~expected ~result =
    let%lwt expected = T.load_remote_json options.document_loader expected in
    let expected = J.normalize expected in
    let result = J.normalize result in
    if J.compare expected result = 0 then
      Lwt.return `Passed
    else
      (
       let msg = Printf.sprintf
         "Result differs from expected output:\nexpected=%s\nresult=%s"
           (J.to_string expected) (J.to_string result)
       in
       Lwt.return (`Failed msg)
      )
  in
  run_test ~action ~positive

let run_toRDF_test =
  let action (options:T.options) ~base_url ~input =
    let%lwt input = T.load_remote_json options.document_loader input in
    let g = Rdf.Graph.open_graph base_url in
    let%lwt (ds,_) = Json_ld.to_rdf options input g in
    Lwt.return ds
  in
  let positive (options:T.options) ~base_url ~expected ~result =
    let%lwt expected =
      let%lwt str = match%lwt options.document_loader expected with
        | Ok str -> Lwt.return str
        | Error e -> raise e
      in
      let g = Rdf.Graph.open_graph base_url in
      let ds = Rdf.Ds.mem_dataset g in
      Rdf.Nq.from_string ds str ;
      Lwt.return ds
    in
    match Rdf.Ds.isomorphic_diff ~ignore_blanks:true expected result with
    | None -> Lwt.return `Passed
    | Some diff ->
        let result = Rdf.Nq.to_string result in
        let expected = Rdf.Nq.to_string expected in
        let msg = Printf.sprintf
          "Result differs from expected output:\n%s\nexpected=\n%sresult=\n%s"
            (Ds.string_of_diff diff) expected result
        in
        Lwt.return (`Failed msg)
  in
  run_test ~action ~positive

let run_fromRDF_test =
  let action (options:T.options) ~base_url ~input =
    let%lwt input = 
      match%lwt options.document_loader input with
      | Ok str -> Lwt.return str
      | Error e -> raise e
    in
    let g = Rdf.Graph.open_graph base_url in
    let ds = Rdf.Ds.mem_dataset g in
    Rdf.Nq.from_string ds input ;
    let json = Json_ld.from_rdf options ds in
    Lwt.return json
  in
  let positive (options:T.options) ~base_url ~expected ~result =
    let%lwt result =
      let g = Rdf.Graph.open_graph base_url in
      let%lwt (ds,_) = Json_ld.to_rdf options result g in
      Lwt.return ds
    in
    let%lwt expected =
      let%lwt json = T.load_remote_json options.document_loader expected in
      let g = Rdf.Graph.open_graph base_url in
      let%lwt (ds,_) = Json_ld.to_rdf options json g in
      Lwt.return ds
    in
    match Rdf.Ds.isomorphic_diff ~ignore_blanks:true expected result with
    | None -> Lwt.return `Passed
    | Some diff ->
        let result = Rdf.Nq.to_string result in
        let expected = Rdf.Nq.to_string expected in
        let msg = Printf.sprintf
          "Result differs from expected output:\n%s\nexpected=\n%sresult=\n%s"
            (Ds.string_of_diff diff) expected result
        in
        Lwt.return (`Failed msg)
  in
  run_test ~action ~positive

let run_funs = List.fold_left
  (fun acc (iri, f) -> Iri.Map.add iri f acc)
    Iri.Map.empty
    [
      Jsonld_test_vocab.c_ExpandTest, run_expand_test ;
      Jsonld_test_vocab.c_ToRDFTest, run_toRDF_test ;
      Jsonld_test_vocab.c_FromRDFTest, run_fromRDF_test ;
    ]

let print_sub_triples g sub =
  let triples = g.Rdf.Graph.find ~sub () in
  List.iter (fun (_,pred,obj) ->
     Log.warn (fun m -> m "%s -- %s -- %s"
        (Rdf.Term.string_of_term sub)
          (Iri.to_string pred)
          (Rdf.Term.string_of_term obj)
     ))
    triples

let load_remote_cached cache =
  match cache with
  | None -> T.load_remote_curl
  | Some dir ->
     let _ = Sys.command (Printf.sprintf "mkdir -p %s" (Filename.quote dir)) in
     let f iri =
        let sha = Cryptokit.Hash.sha256 () in
        sha#add_string (Iri.to_string iri) ;
        let e = Cryptokit.Hexa.encode () in
        e#put_string sha#result ;
        let file = Filename.concat dir e#get_string in
        match%lwt Lwt_unix.file_exists file with
        | true ->
            let%lwt str = Lwt_io.(with_file ~mode:Input file read) in
            Lwt.return_ok str
        | false ->
            match%lwt T.load_remote_curl iri with
            | Ok str ->
                let%lwt () = Lwt_io.(with_file ~mode:Output file
                   (fun oc -> write oc str))
                in
                Lwt.return_ok str
            | x -> Lwt.return x
     in
     f

let load_remote = ref T.load_remote_curl

let run_test_by_types =
  let rec iter g ~options test = function
  | [] ->
      Log.warn (fun m -> m "%a: no supported type" Iri.pp test);
      Lwt.return `Ignored
  | typ :: q ->
      match Iri.Map.find_opt typ run_funs with
      | None -> iter g ~options test q
      | Some f ->
          Log.debug (fun m -> m "running test %s" (Iri.to_string test));
          match%lwt f g ~options test with
          | `Passed -> Lwt.return `Passed
          | `Failed msg -> if !stop_on_fail then exit 1 else Lwt.return (`Failed msg)
  in
  iter

let run_test g test =
  let test_term = Rdf.Term.Iri test in
  match g.Rdf.Graph.objects_of ~sub:test_term ~pred:Jsonld_test_vocab.option with
  | l when List.exists (fun sub ->
       List.exists
         (fun lit -> lit.Rdf.Term.lit_value = "json-ld-1.0")
         (Rdf.Graph.literal_objects_of g ~sub ~pred:Jsonld_test_vocab.specVersion)
    ) l ->
      Lwt.return (`Ignored_with "json_1_0")
  | l when List.exists (fun sub ->
       List.exists Rdf.Term.bool_of_literal
         (Rdf.Graph.literal_objects_of g ~sub ~pred:Jsonld_test_vocab.produceGeneralizedRdf)
    ) l ->
      Lwt.return (`Ignored_with "produce generalized rdf")
  | l ->
      let options = get_options g !load_remote l in
      (*print_sub_triples g test ;
      List.iter (print_sub_triples g) l;*)
      match Rdf.Graph.types_of g test_term with
      | [] -> Log.err (fun m -> m "no type"); Lwt.return `Ignored
      | types -> run_test_by_types g ~options test types

let get_first_literal g sub pred =
  match Rdf.Graph.literal_objects_of g ~sub ~pred with
  | [] -> None
  | lit :: _ -> Some lit
let get_first_string g sub pred =
  match get_first_literal g sub pred with
  | None -> None
  | Some lit -> Some lit.lit_value
let get_comment g sub =
  get_first_string g (Rdf.Term.Iri sub) Rdf.Rdfs.comment
let get_name g sub =
  get_first_string g (Rdf.Term.Iri sub) Rdf.Test_manifest.name

let run_tests g tests =
  let ok = ref 0 in
  let ko = ref 0 in
  let ignored = ref 0 in
  let f test =
    match%lwt run_test g test with
    | `Ignored_with reason ->
        incr ignored ;
        Log.info (fun m -> m "%a: ignoring %s" Iri.pp test reason);
        Lwt.return_unit
    | `Ignored ->
        incr ignored;
        Log.info (fun m -> m "%a: ignored" Iri.pp test);
        Lwt.return_unit
    | `Passed ->
        incr ok ;
        Log.info (fun m -> m "%a: passed" Iri.pp test);
        Lwt.return_unit
    | `Failed msg ->
        incr ko;
        let comment = match get_comment g test with
          | None -> ""
          | Some str -> Printf.sprintf "Purpose: %s\n" str
        in
        let name = match get_name g test with
          | None -> ""
          | Some str -> Printf.sprintf "Name: %s\n" str
        in
        Log.err (fun m -> m "%a: failed\n%s%s%s" Iri.pp test comment name msg);
        Lwt.return_unit
  in
  let%lwt () = Lwt_list.iter_s f tests in
  Log.app (fun m -> m "%d tests, %d ignored, %d successed, %d failed"
       (!ok + !ko + !ignored) !ignored !ok !ko);
  Lwt.return_unit

let run_manifest file =
  try
    Log.info (fun m -> m "running test manifest %s" file);
    let%lwt str = Lwt_io.(with_file ~mode:Input file read) in
    let json = J.from_string_exn str in
    let iri = Iri.append_path base_iri [file] in
    let g = Rdf.Graph.open_graph iri in
    let options = T.options !load_remote in
    let%lwt (ds,_) = Json_ld.to_rdf options json g in
    Rdf.Ds.merge_to_default ds;
    (*let%lwt () = Lwt_io.(write_line stdout
       (Rdf.Ttl.to_string ds.default))
    in*)
    (* get root term of manifest *)
    let root =
      match ds.default.subjects_of
        ~pred:Rdf.Rdf_.type_ ~obj:Rdf.(Term.Iri Test_manifest.c_Manifest)
      with
        [] -> failwith "No root manifest"
      | t :: _ -> t
    in
    let o = new Rdf.Test_manifest.from ~sub:root ds.default in
    let entries = match o#entries with
      | [] -> []
      | h :: _ ->  Rdf.Graph.only_iris (Rdf.Graph.to_list ds.default h)
    in
    run_tests ds.default entries
    (*Lwt_list.iter_s (fun t -> Lwt_io.(write_line stdout (Rdf.Term.string_of_term t))) entries*)
  with Failure msg -> prerr_endline msg; Lwt.return_unit
  | Iri.Error e ->
      let%lwt () = Lwt_io.(write stderr (Iri.string_of_error e)) in
      Lwt.return_unit
(*| e ->
      let%lwt () = Lwt_io.(write stderr (Printexc.to_string e)) in
      raise e*)

let options = [
    "--cache", Arg.String (fun s -> load_remote := load_remote_cached (Some s)),
    "<dir> use <dir> to cache remotely loaded files";
  ]

let usage = Printf.sprintf "Usage: %s [options] <files>" Sys.argv.(0)
let main () =
  let files = ref [] in
  Arg.parse options
    (fun s -> files := s :: !files)
    (Printf.sprintf "%s\n where options are:" usage);

  Logs.(set_level (Some Debug));
  Logs.(set_level (Some Error));
  Logs.Src.set_level log_src (Some Info);
  install_log_reporter ();
  match List.rev !files with
  | [] -> Lwt_io.(write_line stderr usage)
  | args -> Lwt_list.iter_s run_manifest args

let () = Lwt_main.run (main ())
