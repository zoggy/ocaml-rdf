(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** JSON documents. *)

type loc = int * int (** 1-based line * 0-based column *)

type range = loc * loc

val string_of_range : range -> string

type 'a ranged = {
    loc : range option;
    data : 'a;
}
val ranged : ?loc:range -> 'a -> 'a ranged

type error = ..
type error += | Invalid_json of string

exception Error of error

(** [error e] raises [Error e]. *)
val error : error -> 'a

type key = string ranged
type map = (key * json) list
and json_t = [
    | `Obj of map
    | `List of json list
    | `String of string
    | `Bool of bool
    | `Float of float
    | `Null
 ]
and json = json_t ranged

val json : ?loc:range -> json_t -> json

val string_of_error : range -> Jsonm.error -> string

val from_string :
  ?encoding:[< Jsonm.encoding ] ->
  string -> (json, (range * Jsonm.error)) Stdlib.result

val from_string_exn :
  ?encoding:[< Jsonm.encoding ] ->
  string -> json

(** [to_string json] returns the given [json] in JSON syntax.
  Optional argument [minify] (default [false]) indicates whether
  to minify the output. *)
val to_string : ?minify:bool -> json -> string

(** [pp ppf json] pretty-prints [json] to [ppf].*)
val pp : Stdlib.Format.formatter -> json -> unit

(** Same as {!val:pp} but pretty-prints minified JSON. *)
val ppm : Stdlib.Format.formatter -> json -> unit


val string : ?loc:range -> string -> json
val string_of_opt : ?loc:range -> string option -> json
val list : ?loc:range -> json list -> json
val obj : ?loc:range -> map -> json
val null : json
val bool : ?loc:range -> bool -> json
val float : ?loc:range -> float -> json
val compare : json -> json -> int

val key_value_compare : (key * json) -> (key * json) -> int
val map_compare : map -> map -> int
val sort_map : map -> map
val normalize : json -> json
val equal : json -> json -> bool
val to_array : json -> json
val values : json -> json list

val is_scalar : json -> bool
val is_string_array : json -> bool

val string_of_json_opt : json option -> string

val map_get : map -> string -> json option
val map_add_value : map -> string -> json -> map
val map_remove_value : map -> string -> map
val map_pp : Stdlib.Format.formatter -> map -> unit
val opt_map_pp : Stdlib.Format.formatter -> map option -> unit

val (-->>) : map -> string -> json option
val (-->) : map -> string -> json_t option
val (-?>) : json -> string -> json option
