(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** JSON-LD API.

Only minimum functions to communicate with JSON-LD are implemented,
i.e. context processing, expansion, deserialization and serialization.

Use of JSON-LD should be avoided when possible.
*)

(** Initialize an expansion context from the given base IRI. *)
val init_ctx : Iri.t -> Expand.ctx

(** Same as {!init_ctx} but will use the provided {!T.type-options}
  to initialize a context. This includes downloading and
  processing an optional "expand context". *)
val ctx_of_options : T.options -> Iri.t -> Expand.ctx Lwt.t

(** [expansion options context json base] expands the given
     [json] value in [context], with [base] IRI, honouring [options].
     Optional argument [frame_expansion] is supposed to work as
     specified {{:https://www.w3.org/TR/json-ld11-api/#dom-jsonldoptions-frameexpansion}here}
     (default is false).
*)
val expansion : T.options ->
  ?frame_expansion:bool -> Expand.ctx -> J.json -> Iri.t -> J.json Lwt.t

(** [to_rdf options json graph] builds a context from [options], expands
  the [json] value (using [graph] name as base IRI), then applies
  the {{:https://www.w3.org/TR/json-ld11-api/#node-map-generation}node map generation}
  and {{:https://www.w3.org/TR/json-ld11-api/#deserialize-json-ld-to-rdf-algorithm}JSON-LD to RDF}
  algorithms to build the returned dataset. The provided [graph] is
  the default graph of the returned dataset. The function also returns the
  optional root IRI if there is only one root object and it has an [@id]. *)
val to_rdf :
  T.options -> J.json -> Rdf.Graph.graph -> (Rdf.Ds.dataset * Iri.t option) Lwt.t

(** [from_rdf options dataset] serializes the given [dataset] to
  a JSON-LD value, honouring [options].
  Beware that the {{:https://www.w3.org/TR/json-ld11-api/#serialize-rdf-as-json-ld-algorithm}
  specified RDF to JSON-LD algorithm} is not implemented, but a simpler version returning
  some flat (but correct) JSON-LD. *)
val from_rdf : T.options -> Rdf.Ds.dataset -> J.json

(** [from_rdf_root options graph root] serializes the given [graph] to
  a JSON-LD value, with a single object starting from [root] term, honouring [options].
  Beware that the {{:https://www.w3.org/TR/json-ld11-api/#serialize-rdf-as-json-ld-algorithm}
  specified RDF to JSON-LD algorithm} is not implemented, but a simpler version returning
  some flat (but correct) JSON-LD. Lists are represented using json list, though. *)
val from_rdf_root : T.options -> Rdf.Graph.graph -> Rdf.Term.term -> J.json
