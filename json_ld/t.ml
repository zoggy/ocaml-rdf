(*********************************************************************************)
(*                OCaml-RDF                                                      *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Base types and utilities. *)

open Rdf
module SMap = Map.Make(String)
module SSet = Set.Make(String)

module J = J
open J

let is_default_object = function
| { data = `Obj fields } -> J.map_get fields "@default" <> None
| _ -> false


type error = J.error = ..
let error = J.error

exception Error = J.Error

(* https://www.w3.org/TR/json-ld11/#dfn-node-object *)
let is_node_object json =
  match json.data with
  | `Obj fields ->
      (* we do not test node being top-most map ... *)
      (J.map_get fields "@value" = None &&
       J.map_get fields "@list" = None &&
         J.map_get fields "@set" = None)
  | _ -> false

let iri_of_string str = Iri.of_string ~normalize:false str
let iri_resolve ~base iri =
  let res = Iri.resolve ~normalize:true ~base iri in
  Log.debug (fun m -> m "iri_resolve ~base=%a iri=%a => %a"
     Iri.pp base Iri.pp iri Iri.pp res);
  res

let str_is_iri str =
  try Iri.scheme (iri_of_string str) <> ""
  with _ -> false

let is_iri = function
| { data = `String str } -> str_is_iri str
| _ -> false

let i18n_ns = "https://www.w3.org/ns/i18n#"

type error +=
  | Bad_iri of string * string (* iri as string, message *)
  | Colliding_keywords of string
  | Conflicting_indexes of J.json
  | Cyclic_iri_mapping of bool SMap.t
  | Expanded_graph_bad_result of J.json
  | Expanded_list_bad_result of J.json
  | Invalid_base_direction of J.json
  | Invalid_base_iri of J.json
  | Invalid_blank_node_id of J.json
  | Invalid_container_mapping of J.json
  | Invalid_context_entry of string
  | Invalid_context_nullification of J.range option
  | Invalid_default_language of J.json
  | Invalid_expanded_json of J.json
  | Invalid_id_value of J.json
  | Invalid_import of J.json
  | Invalid_included_value of J.json
  | Invalid_index_value of J.json
  | Invalid_iri_mapping of J.json
  | Invalid_json_literal of string
  | Invalid_keyword_alias of string * string
  | Invalid_language_map_value of J.json
  | Invalid_language_mapping of J.json
  | Invalid_language_tagged_string of J.json
  | Invalid_language_tagged_value of J.json
  | Invalid_local_context of J.range option
  | Invalid_nest_value of J.json
  | Invalid_prefix_value of J.json
  | Invalid_propagate_value of J.json
  | Invalid_protected_value of J.json
  | Invalid_remote_context of Iri.t * string
  | Invalid_reverse_property
  | Invalid_reverse_property_map
  | Invalid_reverse_property_value of J.json
  | Invalid_reverse_value of J.json
  | Invalid_scoped_context
  | Invalid_set_or_list_object of J.json
  | Invalid_subject of Term.term
  | Invalid_term_definition of string
  | Invalid_term_definition_value of J.json
  | Invalid_type of Term.term
  | Invalid_type_mapping of J.json
  | Invalid_type_mapping_value of J.json
  | Invalid_type_value of J.json
  | Invalid_typed_value of J.json
  | Invalid_value_object_value of J.json
  | Invalid_value_object of J.json
  | Invalid_version of J.json
  | Invalid_vocab_mapping of string
  | Iri_get of Iri.t * string (* iri, message *)
  | Keyword_redefinition of string
  | Loading_remote_context_failed of Iri.t * string
  | Processing_mode_conflict
  | Protected_term_redefinition of string

let string_of_error_json msg json =
  Printf.sprintf "%s%s: %s"
    (match json.loc with
     | None -> ""
     | Some r -> Printf.sprintf "%s: " (string_of_range r))
    msg
    (J.to_string json)

let string_of_error = function
| Bad_iri (iri, s) -> Printf.sprintf "Invalid IRI %s: %s" iri s
| Colliding_keywords kw -> Printf.sprintf "Colliding keyword %S" kw
| Conflicting_indexes json ->
    string_of_error_json "Conflicting indexes" json
| Cyclic_iri_mapping def ->
    Printf.sprintf "Cyclic IRI mapping with terms %s"
      (String.concat ", " (SMap.fold
        (fun t v acc -> if false then t :: acc else acc) def []))
| Expanded_graph_bad_result json ->
    string_of_error_json "Expanded @graph result has wrong form" json
| Expanded_list_bad_result json ->
    string_of_error_json "Expanded @list result has wrong form" json
| Iri_get (iri, s) ->
    Printf.sprintf "Could not retrieve %s: %s" (Iri.to_string iri) s
| Invalid_expanded_json s  ->
    Printf.sprintf "Invalid expanded json: %s" (J.to_string s)
| Invalid_json s  ->
    Printf.sprintf "Invalid json: %s" s
| Invalid_json_literal s  ->
    Printf.sprintf "Invalid json literal: %s" s
| Invalid_subject t ->
    Printf.sprintf "Invalid triple subject: %s" (Term.string_of_term t)
| Invalid_type t ->
    Printf.sprintf "Invalid type: %s" (Term.string_of_term t)
| Invalid_container_mapping json ->
    string_of_error_json "Invalid container mapping" json
| Invalid_context_nullification range ->
    Printf.sprintf "%sInvalid context nullification"
      (match range with
       | None -> ""
       | Some r -> Printf.sprintf "%s: " (string_of_range r))
| Invalid_local_context range ->
    Printf.sprintf "%sInvalid local context"
      (match range with
       | None -> ""
       | Some r -> Printf.sprintf "%s: " (string_of_range r))
| Loading_remote_context_failed (iri, msg) ->
    Printf.sprintf "Loading remote context %s failed: %s"
      (Iri.to_string iri) msg
| Invalid_remote_context (iri, msg) ->
    Printf.sprintf "Invalid remote context %s: %s" (Iri.to_string iri) msg
| Invalid_context_entry e ->
    Printf.sprintf "Invalid context entry %S" e
| Invalid_base_direction json ->
    string_of_error_json "Invalid base @direction" json
| Invalid_base_iri json ->
    string_of_error_json "Invalid @base iri" json
| Invalid_blank_node_id json ->
    string_of_error_json "Invalid blank node id" json
| Invalid_default_language json ->
    string_of_error_json "Invalid default language" json
| Invalid_id_value json ->
    string_of_error_json "Invalid @id value" json
| Invalid_import json ->
    string_of_error_json "Invalid @import" json
| Invalid_included_value json ->
    string_of_error_json "Invalid @included value" json
| Invalid_index_value json ->
    string_of_error_json "Invalid @index value" json
| Invalid_iri_mapping json ->
    string_of_error_json "Invalid IRI mapping" json
| Invalid_keyword_alias (term, kw) ->
    Printf.sprintf "Invalid keyword alias %s for %s" term kw
| Invalid_language_map_value json ->
    string_of_error_json "Invalid language map value" json
| Invalid_language_mapping json ->
    string_of_error_json "Invalid language mapping" json
| Invalid_language_tagged_string json ->
    string_of_error_json "Invalid language tagged string" json
| Invalid_language_tagged_value json ->
    string_of_error_json "Invalid language tagged value" json
| Invalid_nest_value json ->
    string_of_error_json "Invalid @nest value" json
| Invalid_prefix_value json ->
    string_of_error_json "Invalid @prefix value" json
| Invalid_propagate_value json ->
    string_of_error_json "Invalid @propagate value" json
| Invalid_protected_value json ->
    string_of_error_json "Invalid @protected value" json
| Invalid_reverse_property -> "Invalid reverse property"
| Invalid_reverse_property_value json ->
    string_of_error_json "Invalid @reverse property value" json
| Invalid_reverse_property_map -> "Invalid reverse property map"
| Invalid_reverse_value json ->
    string_of_error_json "Invalid @reverse value" json
| Invalid_set_or_list_object json ->
    string_of_error_json "Invalid set or list object" json
| Invalid_scoped_context -> "Invalid scoped context"
| Invalid_term_definition term -> Printf.sprintf "Invalid term definition %S" term
| Invalid_term_definition_value json ->
    string_of_error_json "Invalid term definition value" json
| Invalid_type_mapping json ->
    string_of_error_json "Invalid type mapping" json
| Invalid_type_mapping_value json ->
    string_of_error_json "Invalid type mapping value" json
| Invalid_type_value json ->
    string_of_error_json "Invalid @type value" json
| Invalid_typed_value json ->
    string_of_error_json "Invalid typed value" json
| Invalid_value_object_value json ->
    string_of_error_json "Invalid value object value" json
| Invalid_value_object json ->
    string_of_error_json "Invalid value object" json
| Invalid_version json ->
    string_of_error_json "Invalid @version" json
| Invalid_vocab_mapping str ->
    Printf.sprintf "Invalid vocab mapping %S" str
| Keyword_redefinition kw -> Printf.sprintf "Keyword redefinition %S" kw
| Processing_mode_conflict -> "Processing mode conflict"
| Protected_term_redefinition term -> Printf.sprintf "Protected term redefinition %S" term
| _ -> "Unknown error"


let code_of_error = function
| Colliding_keywords _ -> "colliding keywords"
| Conflicting_indexes _ -> "conflicting indexes"
| Cyclic_iri_mapping _ -> "cyclic IRI mapping"
| Invalid_base_direction _ -> "invalid base direction"
| Invalid_base_iri _ -> "invalid base IRI"
| Invalid_container_mapping _ -> "invalid container mapping"
| Invalid_context_entry _ -> "invalid context entry"
| Invalid_context_nullification _ -> "invalid context nullification"
| Invalid_default_language _ -> "invalid default language"
| Invalid_id_value _ -> "invalid @id value"
| Invalid_import _ -> "invalid @import value"
| Invalid_included_value _ -> "invalid @included value"
| Invalid_index_value _ -> "invalid @index value"
| Invalid_iri_mapping _ -> "invalid IRI mapping"
| Invalid_json_literal _ -> "invalid JSON literal"
| Invalid_keyword_alias _ -> "invalid keyword alias"
| Invalid_language_map_value _ -> "invalid language map value"
| Invalid_language_mapping _ -> "invalid language mapping"
| Invalid_language_tagged_string _ -> "invalid language-tagged string"
| Invalid_language_tagged_value _ -> "invalid language-tagged value"
| Invalid_local_context _ -> "invalid local context"
| Invalid_nest_value _ -> "invalid @nest value"
| Invalid_prefix_value _ -> "invalid @prefix value"
| Invalid_propagate_value _ -> "invalid @propagate value"
| Invalid_protected_value _ -> "invalid @protected value"
| Invalid_remote_context _ -> "invalid remote context"
| Invalid_reverse_property -> "invalid reverse property"
| Invalid_reverse_property_map -> "invalid reverse property map"
| Invalid_reverse_property_value _ -> "invalid reverse property value"
| Invalid_reverse_value _ -> "invalid @reverse value"
| Invalid_scoped_context -> "invalid scoped context"
| Invalid_set_or_list_object _ -> "invalid set or list object"
| Invalid_subject _ -> "invalid subject"
| Invalid_term_definition _ -> "invalid term definition"
| Invalid_term_definition_value _ -> "invalid term definition"
| Invalid_type _ -> "invalid type value"
| Invalid_type_mapping _ -> "invalid type mapping"
| Invalid_type_mapping_value _ -> "invalid type mapping"
| Invalid_type_value _ -> "invalid type value"
| Invalid_typed_value _ -> "invalid typed value"
| Invalid_value_object_value _ -> "invalid value object value"
| Invalid_value_object _ -> "invalid value object"
| Invalid_version _ -> "invalid @version value"
| Invalid_vocab_mapping _ -> "invalid vocab mapping"
| Keyword_redefinition _ -> "keyword redefinition"
| Loading_remote_context_failed _ -> "loading remote context failed"
| Processing_mode_conflict -> "processing mode conflict"
| Protected_term_redefinition _ -> "protected term redefinition"
(* unofficial error codes *)
| e -> string_of_error e

let () = Printexc.register_printer
  (function Error e -> Some (string_of_error e) | _ -> None)

let warn_invalid_language_tag str =
  Log.warn (fun m -> m "Invalid language tag %S" str)


type rdf_direction = I18n_datatype | Compound_literal
let rdf_direction_of_string = function
| "compound-literal" -> Compound_literal
| "i18n-datatype" -> I18n_datatype
| str ->
    Log.warn (fun m -> m "Invalid rdf_direction %S, using i18n-datatype" str);
    I18n_datatype
let string_of_rdf_direction = function
| Compound_literal -> "compound-literal"
| I18n_datatype -> "i18n-datatype"


type direction = [`Ltr | `Rtl]
let string_of_direction = function
| `Ltr -> "ltr"
| `Rtl -> "rtl"


type options = {
    base: string option ;
    compact_arrays : bool ;
    compact_to_relative : bool ;
    document_loader : Iri.t -> (string, exn) result Lwt.t ;
    expand_context : [`Json of J.json | `Iri of Iri.t] option ;
    extract_all_scripts : bool ;
    frame_expansion : bool ;
    ordered : bool ;
    processing_mode : string ;
    produce_generalized_rdf : bool ;
    rdf_direction : rdf_direction option ;
    use_native_types : bool ;
    use_rdf_type : bool ;
}

let options ?base ?(compact_arrays=true) ?(compact_to_relative=true)
  ?expand_context ?(extract_all_scripts=false)
    ?(frame_expansion=false) ?(ordered=false)
    ?(processing_mode="json-ld-1.1") ?(produce_generalized_rdf=true)
    ?rdf_direction ?(use_native_types=false) ?(use_rdf_type=false)
    document_loader =
    { base ; compact_arrays ; compact_to_relative ; document_loader ;
      expand_context ; extract_all_scripts ;
      frame_expansion ; ordered ; processing_mode ;
      produce_generalized_rdf ; rdf_direction ;
      use_native_types ; use_rdf_type ;
    }


type type_mapping = [ `Iri of string | `Id | `Json | `None | `Vocab ]
type iri_mapping = [ `Iri of string | `Keyword of string | `Bnode of string | `Null ]

let string_of_iri_mapping = function
| `Iri iri -> Format.sprintf "Iri %s" iri
| `Keyword s -> Format.sprintf "Kw %s" s
| `Bnode s -> Format.sprintf "Bnode %s" s
| `Null -> "null"

let pp_iri_mapping ppf im = Format.fprintf ppf "%s" (string_of_iri_mapping im)

let string_of_type_mapping = function
| `Iri iri -> Format.sprintf "Iri %s" iri
| `Id -> "@id"
| `Json -> "@json"
| `None -> "@none"
| `Vocab -> "@vocab"

let string_of_type_mapping_opt = function
| None -> "None"
| Some t -> string_of_type_mapping t

let keywords = SSet.of_list [
    "@base" ; "@container" ; "@context" ; "@direction" ;
    "@graph"; "@id"; "@import"; "@included" ; "@index" ;
    "@json" ; "@language" ; "@list" ; "@nest" ; "@none" ;
    "@prefix" ; "@propagate" ; "@protected" ; "@reverse" ;
    "@set" ; "@type"; "@value"; "@version" ; "@vocab" ;
  ]

let keyword_re = Re.(compile (seq [ bol ; rep1 (char '@') ; rep1 alpha ; eol]))
let str_has_keyword_form str =
  match Re.exec_opt keyword_re str with
  | Some _ -> true
  | None -> false

let has_keyword_form = function
| { data = `String str } -> str_has_keyword_form str
| _ -> false

let str_is_kw str = SSet.mem str keywords

let is_kw = function
| { data = `String str } -> str_is_kw str
| _ -> false

let is_kw_ kw : J.json -> bool = function { data = `String str } -> str = kw | _ -> false
let is_kw_id = is_kw_ "@id"
let is_kw_json = is_kw_ "@json"
let is_kw_none = is_kw_ "@none"
let is_kw_value = is_kw_ "@value"
let is_kw_vocab = is_kw_ "@vocab"


let is_compact_iri str =
  let len = String.length str in
  match String.index_opt str ':' with
  | None -> false
  | Some p ->
      not (len > p + 2 && str.[p+1] = '/' && str.[p+2] = '/')

(* FIXME: use Ttl lexer to check for blank node syntax *)
let str_is_blank_node_id str = Rdf.Utf8.utf8_is_prefix str "_:"

let is_blank_node_id json =
  match json.data with
  | `String str -> str_is_blank_node_id str
  | _ -> false

let is_value_object json =
  match json.data with
  | `Obj l -> J.map_get l "@value" <> None
  | _ -> false

(* https://www.w3.org/TR/json-ld11/#graph-objects *)
let is_graph_object json =
  match json.data with
  | `Obj map ->
      J.map_get map "@graph" <> None &&
        (List.for_all
         (function
          | ({data = ("@graph"|"@index"|"@id"|"@context")},_) -> true
          | _ -> false) map
        )
  | _ -> false

(* https://www.w3.org/TR/json-ld11/#dfn-list-object *)
let is_list_object json =
  match json.data with
  | `Obj map ->
      J.map_get map "@list" <> None &&
        (List.for_all
         (function
          | ({data = ("@list"|"@index")},_) -> true
          | _ -> false) map
        )
  | _ -> false

let smap_to_json map =
  let fields = SMap.fold
    (fun k v acc -> (ranged k, v) :: acc) map []
  in
  J.obj fields

let remove_blank_id_prefix str = String.sub str 2 (String.length str - 2)

let load_remote_json load iri =
  match%lwt load iri with
  | Result.Error e ->
      let msg = Printexc.to_string e in
      error (Loading_remote_context_failed (iri, msg))
  | Ok str ->
      match J.from_string str with
      | Error (range, e) ->
          error (Invalid_remote_context (iri, (J.string_of_error range e)))
      | Ok x -> Lwt.return x

let jsonld_content_types = ["application/ld+json"; "application/json"]
let load_remote_curl iri =
  let accept = Printf.sprintf "Accept: %s" (String.concat ", " jsonld_content_types) in
  let com = Printf.sprintf "curl -L -s -H %S %s" accept (Iri.to_string iri) in
  Log.debug (fun m -> m "running %s" com);
  let fread p_in =
     let%lwt str = Lwt_io.read p_in#stdout in
    match%lwt p_in#close with
    | Unix.WEXITED 0 -> Lwt.return_ok str
    | _ -> Lwt.return_error (Failure (Printf.sprintf "Command %S failed" com))
  in
  Lwt_process.(with_process_in (shell com) fread)